﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ug.Models;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleAppTestRoomJsonData
{
    class Program
    {
        //static HubConnection hubConnection = new HubConnection("http://localhost:61124/");

        


        static void Main(string[] args)
        {

            try
            {
                var connection = new HubConnection("http://localhost:61124/");
                IHubProxy hub = connection.CreateHubProxy("RoomHub");
                connection.Start().ContinueWith(
                    task =>
                    {
                        if (task.IsFaulted)
                        {
                            Console.WriteLine("{0}", task.Exception.GetBaseException());
                        }
                        else
                        {

                          //  ApplicationDbContext db = new ApplicationDbContext();
                            

                            hub.Invoke("Login", "Hello world");
                        }
                    });

                hub.On("Login", (string mess) =>
                {
                    Console.WriteLine(mess);
                }

                        //// Context is a reference to SynchronizationContext.Current
                        //Context.Post(delegate
                        //{
                        //    textBox.Text += "Notified!\n";
                        //}, null)
                    );
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            Console.ReadLine();

            //IHubProxy stockTickerHubProxy = hubConnection.CreateHubProxy("RoomHub");
            ////stockTickerHubProxy.On<Stock>("UpdateStockPrice", stock => Console.WriteLine("Stock update for {0} new price {1}", stock.Symbol, stock.Price));

            //hubConnection.Start();

            //stockTickerHubProxy.Invoke("Welcome", "1");
            //Console.WriteLine("hello world");
            //Console.ReadLine();
        }



        static void Main78454(string[] args)
        {
            var task = MakeRequest();
            task.Wait();

            var response = task.Result;
            var body = response.Content.ReadAsStringAsync().Result;

            Console.WriteLine(body);
        }

        private static async Task<HttpResponseMessage> MakeRequest()
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml");
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Encoding", "gzip, deflate");
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0");
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

            return await httpClient.PostAsync("https://appr.tc/join/"+Guid.NewGuid().ToString(), null);
        }
    }
}