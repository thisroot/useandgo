﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using ug.Models;

[assembly: OwinStartupAttribute(typeof(ug.Startup))]
namespace ug
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {


            var config = new HubConfiguration();
            config.EnableJSONP = true;
           
            app.MapSignalR(config);
            
            //app.Map("/signalr", map =>
            //{
            //    map.UseCors(CorsOptions.AllowAll);
            //    new HubConfiguration() { EnableJSONP = true };
            //    map.RunSignalR();
            //});

            ConfigureAuth(app);
            ConfigureOAuth(app);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
        }

        private void ConfigureOAuth(IAppBuilder appBuilder)
        {
            appBuilder.CreatePerOwinContext(ApplicationDbContext.Create);
            appBuilder.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            OAuthAuthorizationServerOptions oAuthAuthorizationServerOptions = new OAuthAuthorizationServerOptions()
            {
                TokenEndpointPath = new Microsoft.Owin.PathString("/oauth/login"), // token alacağımız path'i belirtiyoruz
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(30),
                AllowInsecureHttp = true,
                Provider = new SimpleAuthorizationServerProvider(),
            };
            appBuilder.UseOAuthAuthorizationServer(oAuthAuthorizationServerOptions);
            appBuilder.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}
