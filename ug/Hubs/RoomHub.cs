﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using MoreLinq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ug.Models;
using ug.Models.Communication;
using ug.Models.Events;
using ug.Models.Simple;
using ug.Models.StreamManager;

namespace ug.Hubs
{

    public class RoomHub : Hub
    {
        public static readonly Dictionary<string, Models.ApplicationUser> OnlineUsers = new Dictionary<string, Models.ApplicationUser>();
        private ApplicationDbContext db = new ApplicationDbContext();
        private static ApplicationDbContext dbs = new ApplicationDbContext();
        private static IHubCallerConnectionContext<dynamic> _clients;


        public static void SendAllUsersMessages()
        {
            foreach (var item in OnlineUsers)
            {
                _clients.Client(item.Key).SetreamEvent("User ConnectedToStreamServer");
            }
        }


        public static void StreamCallBack(ApplicationUser user, StreamEvent sev, EEvent ev)
        {
            if (sev.EventStream == TypeStreamEvent.Publish)
            {
                List<ApplicationUser> allUsers = new List<ApplicationUser>();
                //получить всех подписчиков на событие
                allUsers.AddRange(ev.Room.UserRooms.Select(x => x.User).ToList());
                //получить всех подписчиков на специализацию
                allUsers.AddRange(ev.Specialisation.Subscribtions.Select(a => a.OnSubscribeUser).ToList());
                //получить всех друзей пользователя
                allUsers.AddRange(dbs.UserFriends.Where(s => s.UserId == user.Id).Select(d => d.FriendUser).ToList());
                //получить всех подписчиков пользователя
                allUsers.AddRange(dbs.UserInvitations.Where(f => f.UserIdFriend == user.Id).Select(g => g.User).ToList());
                //удалим дубликаты
                allUsers.DistinctBy(ws => ws.Id);

                //отправим всем онлайн пользователям сообщение
                var ous = OnlineUsers.Where(qw => allUsers.Select(df => df.Id).Contains(qw.Value.Id)).Select(er => er.Key).ToList();
                ous.ForEach(fe =>
                {
                    _clients.Client(fe).StreamEvent(user.Id, ev);
                });
                //так же отправим всем пуш уведомление
                //userOffline.CallNotification(ev, tm, userCaller.Id, roomId, userInfo, userCaller.PhoneNumber);
                allUsers.ForEach(fea =>
                {
                    fea.CallNotification(
                        PushMessage.TypePushEvent.Stream,
                        PushMessage.TypeMedia.Video,
                        new SignalingMessage()
                        {
                            RoomId = ev.RoomId,
                            RoomType = "event",
                            From = user.Id,
                            To = fea.Id,
                            Type = "event",
                            Body = sev
                        }, user, ev.Name);
                });
            }
        }


        public void Login(string Token)
        {
            if (String.IsNullOrEmpty(Token))
            {
                Clients.Client(Context.ConnectionId).ErrorLogin("Error Token");
                return;
            }

            var UserToken = db.UsersTokens.Where(x => x.Token == Token).FirstOrDefault();
            if (UserToken == null)
            {
                Clients.Client(Context.ConnectionId).ErrorLogin("Error Token");
                return;
            }

            if (!OnlineUsers.Where(u => u.Key == Context.ConnectionId).Any())
            {
                OnlineUsers.Add(Context.ConnectionId, UserToken.User);
            }


            Clients.Client(Context.ConnectionId).SuccessfulLogin(DateTime.Now);
            //отправить ICE сервера
            Clients.Client(Context.ConnectionId).ICE(new ICE());

            //отправить всем друзьям, что юзер онлайн
            //получить список всех зареганных друзей юзера
            var friends = UserFriend.GetFriends(UserToken.User.Id);
            var ids = friends.Select(x => x.Id).ToList();
            // получить из них всех кто онлайн
            var online = OnlineUsers.Where(x => ids.Contains(x.Value.Id)).ToList();
            online.ForEach(x => {
                Clients.Client(x.Key).ChangeFriendState(new { Status = "Online", Id = UserToken.User.Id });
            });

            // отправить юзеру всех друзей онлайн
            var onlineUsers = online.Select(x => x.Value.Id).ToList();
            var newIds = ids.Where(x => onlineUsers.Contains(x)).Select(d => { return new { Status = "Online", Id = d }; }).ToList();
            Clients.Client(Context.ConnectionId).onlineFriends(newIds);
        }
        public void GetOnlineFriends()
        {
            var u = OnlineUsers[Context.ConnectionId];
            var ids = u.GetFriends().Select(x => x.Id);
            // получить из них всех кто онлайн
            var online = OnlineUsers.Where(x => ids.Contains(x.Value.Id)).ToList();
            var onlineUsers = online.Select(x => x.Value.Id).ToList();
            var newUsers = ids.Where(x => onlineUsers.Contains(x)).Select(w=>  { return new { Status = "Online", Id = w }; }).ToList();
            Clients.Client(Context.ConnectionId).onlineFriends(newUsers);
    
        }
        /// <summary>
        /// Отправить запрос в друзья
        /// </summary>
        public void AddUserInvations(string UserID)
        {

            var u = OnlineUsers[Context.ConnectionId];
            //проверяем на наличие инвайта
            if (db.UserInvitations.Where(a => a.UserIdFriend == UserID && a.UserId == u.Id).Any())
            {
                Clients.Client(Context.ConnectionId).SuccessAddUserInvations("Success Add User");
                return;
            }

            db.Logs.Add(new Log()
            {
                Info = "UserID" + UserID,
                Level = Log.Levels.Debug,
                MethodName = "AddUserInvations",
            });
            db.SaveChanges();


            var d = db.UserInvitations.Add(new UserInvitation()
            {
                UserIdFriend = UserID,
                UserId = u.Id,
            });

            db.SaveChanges();

            foreach (var item in OnlineUsers)
            {
                if (item.Value.Id == UserID)
                {
                    Clients.Client(item.Key).AddUserInvations(d.Id);
                }
            }

            Clients.Client(Context.ConnectionId).SuccessAddUserInvations("Success Add User");
        }
        /// <summary>
        /// Принять запрос в друзья
        /// </summary>
        /// <param name="id">class UserInvitations</param>
        public void AcceptInvations(int id)
        {
            var u = OnlineUsers[Context.ConnectionId];
            if (db.UserInvitations.Where(x => x.Id == id && x.UserIdFriend == u.Id).Any())
            {

                var r = db.UserInvitations.Where(x => x.Id == id && x.UserIdFriend == u.Id).FirstOrDefault();
                if (r != null)
                {
                    UserFriend.Add(r);
                    r.IsDeleted = true;
                    db.SaveChanges();
                    Clients.Client(Context.ConnectionId).AcceptInvations(true);
                }
                else
                {
                    Clients.Client(Context.ConnectionId).ErrorMessage("Cant find invitation");
                }
            }
        }
        /// <summary>
        /// Отклонить запрос в друзья
        /// </summary>
        /// <param name="id">class UserInvitations</param>
        public void RejectInvations(int id)
        {
            var u = OnlineUsers[Context.ConnectionId];
            if (db.UserInvitations.Where(x => x.Id == id && x.UserIdFriend == u.Id).Any())
            {
                var r = db.UserInvitations.Where(x => x.Id == id && x.UserIdFriend == u.Id).FirstOrDefault();
                if (r != null)
                {
                    r.IsDeleted = true;
                    //db.UserInvitations.Attach(r);
                    //db.Entry(r).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    Clients.Client(Context.ConnectionId).RejectInvations(true);
                }
                else
                {
                    Clients.Client(Context.ConnectionId).ErrorMessage("Cant find invitation");
                }
            }
        }
        /// <summary>
        /// Отправка пуша пользователю, раздел связан со звонками, обычные текстовые сообщения здесь не передаются
        /// </summary>
        /// <param name="userid">Пользователь, которому передаем пуш</param>
        /// <param name="roomId">Комната где происходит событие</param>
        private void CallNotification(PushMessage.TypePushEvent ev, PushMessage.TypeMedia tm, string userid, int roomId)
        {
            var UserIdCall = OnlineUsers[Context.ConnectionId].Id.ToString();
            var userCaller = db.Users.FirstOrDefault(q => q.Id == UserIdCall);
            var userOffline = db.Users.FirstOrDefault(q => q.Id == userid);
            string userInfo = string.Format("{0} {1}", userCaller.FirstName, userCaller.SecondName);
            if (userInfo == null || userInfo.Trim().Length == 0)
            {
                userInfo = userCaller.UserName;
            }
            if (userOffline != null)
                userOffline.CallNotification(ev, tm, userCaller.Id, roomId, userInfo, userCaller.PhoneNumber);
        }

        /// <summary>
        /// Получить список все приглашений
        /// </summary>
        public void GetUserInvations()
        {
#warning Задокументировать этот метод в вики
#warning Допилить этот метод см в Апликатион юзер
            var u = OnlineUsers[Context.ConnectionId];

            if (db.UserInvitations.Where(x => x.UserIdFriend == u.Id).Any())
            {
                var r = db.UserInvitations.Where(x => x.UserIdFriend == u.Id).ToList();
                Clients.Client(Context.ConnectionId).UserInvations(r);
            }
        }
        /// <summary>
        /// Удалить из друзей
        /// </summary>
        /// <param name="Id">class UserFriends</param>
        public void RemoveFriend(string UserFriendId)
        {
            var u = OnlineUsers[Context.ConnectionId];

            if (db.UserFriends.Where(h => h.UserId == u.Id && h.UserIdFriend == UserFriendId).Any())
            {
                var uf = db.UserFriends.Where(h => ((h.UserId == u.Id && h.UserIdFriend == UserFriendId) ||
                                            h.UserId == UserFriendId && h.UserIdFriend == u.Id)).ToList();
 
                    uf.ForEach(z =>
                    {
                        z.IsDeleted = true;
                        if(z.UserId == u.Id)
                        {
                            db.UserInvitations.Add(new UserInvitation
                            {
                                UserId = u.Id,
                                UserIdFriend = z.UserIdFriend
                            });

                            var uc = db.Contacts.Where(x => x.Phone == z.FriendUser.PhoneNumber).FirstOrDefault();
                            if (uc != null) uc.IsDeleted = true;
                        }
                    });

                db.SaveChanges();
                Clients.Client(Context.ConnectionId).UserDeleted(true);
            }
        }

        /// <summary>
        /// Создать исходящий вызов
        /// </summary>
        /// <param name="UserID">Ид пользователя которому звоним</param>
        public void OutgoingCall(string userid, int roomId, PushMessage.TypeMedia tm)
        {
            var user = OnlineUsers[Context.ConnectionId];

            //регистрация нового звонка если нет текущих незавершенных звонков
            var lastCall = db.Calls.Where(x => (x.CallerId == user.Id ||
                                                x.AcceptorId == user.Id))
                                   .OrderByDescending(a => a.DateAdd)
                                   .FirstOrDefault();
            
            if (lastCall == null || (lastCall.Status != Call.CallStatus.calling ||
                                     lastCall.Status != Call.CallStatus.onpause))
            {

                var room = db.Rooms.FirstOrDefault(s => s.Id == roomId);
                if(room == null)
                {
                    Clients.Client(Context.ConnectionId).ErrorMessage(new { Message = "Room is not exist", Err = "404" });
                }

                //если не админ и если комната евент или специализация         
                if ((room.TypeRoom == Room.RoomType.Event || room.TypeRoom == Room.RoomType.Specialisation)
                    && room.UserRooms.Any(er => er.UserId == user.Id && er.Role != UserRoom.MemberRole.Admin))
                {
                    var cost = room.CostPerMessage;
                    if (user.Balance >= cost && cost != 0)
                    {
                        user.MoneyTransaction(-cost);
                        Clients.Client(Context.ConnectionId).BalanceInfo(new
                        {
                            Status = BalanceEvent.Ok,
                            Message = "Success"
                        });
                    }
                    else
                    {
                        Clients.Client(Context.ConnectionId).BalanceInfo(new
                        {
                            Status = BalanceEvent.NotEnougth,
                            Message = "Not enougth money"
                        });
                        return;
                    }
                }

                if (OnlineUsers.Where(x => x.Value.Id == userid).Any())
                {
                    var users = OnlineUsers.Where(x => x.Value.Id == userid).FirstOrDefault();
                    var UserIdCall = OnlineUsers[Context.ConnectionId].Id.ToString();
                    Clients.Client(users.Key).NewCall(UserIdCall, roomId, tm);
                    CallNotification(PushMessage.TypePushEvent.Call, tm, userid, roomId);
                }
                else
                {
                    //если пользователя нет онлайн ему pus VOIP сообщение
                    CallNotification(PushMessage.TypePushEvent.Call, tm, userid, roomId);
                }

                db.Calls.Add(new Call
                {
                    ConnectionId = Context.ConnectionId,
                    CallerId = user.Id,
                    AcceptorId = userid,
                    RoomId = roomId,
                    Status = Call.CallStatus.calling
                });
            }
            db.SaveChanges();
        }
        
        public void AcceptСall(string userid, int roomId, PushMessage.TypeMedia tm)
        {

            var acceptor = OnlineUsers[Context.ConnectionId].Id.ToString();
            Clients.Client(Context.ConnectionId).AcceptСall(userid, roomId, tm);
            var caller = OnlineUsers.Where(x => x.Value.Id == userid).FirstOrDefault();

            //проверка на отсутствие текущих незаконченных звонков между пользователями
            var lastCall = db.Calls.Where(x => x.CallerId == userid &&
                                             x.AcceptorId == acceptor &&
                                             x.RoomId == roomId).OrderByDescending(a => a.DateAdd)
                                             .FirstOrDefault();

            if (lastCall != null || (lastCall.Status == Call.CallStatus.calling))
            {

                var k = OnlineUsers.Where(x => x.Value.Id == userid).LastOrDefault().Key;

            if (!String.IsNullOrEmpty(k))
            {
                Clients.Client(k).AcceptСall(acceptor, roomId, tm);
                CallNotification(PushMessage.TypePushEvent.AcceptCall, tm, userid, roomId);
            }
            else
            {
                var UserIdCall = OnlineUsers[Context.ConnectionId].Id.ToString();
                var userCaller = db.Users.FirstOrDefault(q => q.Id == UserIdCall);
                var userOffline = db.Users.FirstOrDefault(q => q.Id == userid);
                if (userOffline != null)
                    userOffline.CallNotification(PushMessage.TypePushEvent.AcceptCall, tm, userCaller.Id, roomId, userCaller.UserName, userCaller.PhoneNumber); 
                Clients.Client(Context.ConnectionId).ErrorMessage(new { Message = "User offline", Err = "404" });
            }

           
                db.Calls.Add(new Call
                {
                    CallerId = userid,
                    AcceptorId = acceptor,
                    RoomId = roomId,
                    Status = Call.CallStatus.started,
                    ConnectionId = caller.Key
                });
            }

            db.SaveChanges();
        }

        /// <summary>
        /// Мне звонят и клиент срасывает
        /// </summary>
        /// <param name="UserID"></param>
        public void RejectCall(string userid, int roomId, PushMessage.TypeMedia tm)
        {

            var UserIDCall = OnlineUsers[Context.ConnectionId].Id.ToString();
            var k = OnlineUsers.Where(x => x.Value.Id == userid).LastOrDefault().Key;

            //реджект может сделать как звонящий так и аццептор
            //получить информацию о текущем состоянии звонка
            var lastCall = db.Calls.Where(x => (x.CallerId == UserIDCall &&
                                                x.AcceptorId == userid) ||
                                               (x.CallerId == userid &&
                                                x.AcceptorId == UserIDCall))
                                                .OrderByDescending(a => a.DateAdd)
                                                .FirstOrDefault();
            //если статус звонка на этапе звонения!%#
            if (lastCall != null && lastCall.Status == Call.CallStatus.calling)
            {


                string caller, acceptor, connection;
                bool initiator = true;
                if (lastCall.CallerId == UserIDCall)
                {
                    caller = UserIDCall;
                    acceptor = userid;
                    connection = Context.ConnectionId;
                    db.UsersRooms.FirstOrDefault(s => s.UserId == acceptor && s.RoomId == roomId).UnreadRejected++;
                    db.SaveChanges();
                }
                else
                {
                    caller = lastCall.CallerId;
                    acceptor = UserIDCall;
                    connection = lastCall.ConnectionId;
                    initiator = false;
                }

                if (!String.IsNullOrEmpty(k))
                {
                    Clients.Client(k).RejectCall(UserIDCall);
                    CallNotification(PushMessage.TypePushEvent.Reject, tm, userid, roomId);
                }
                else
                {
                    CallNotification(PushMessage.TypePushEvent.Reject, tm, userid, roomId);
                }

                db.Calls.Add(new Call
                {
                    ConnectionId = connection,
                    CallerId = caller,
                    AcceptorId = acceptor,
                    RoomId = roomId,
                    Status = Call.CallStatus.rejected
                });

                db.Messages.Add(new Message
                {
                    RoomId = roomId,
                    UserId = caller,
                    Type = Message.TypeMessage.RejectCall,
                    Body = JsonConvert.SerializeObject(new { AcceptorId = acceptor, Initiator = initiator })
                });

                db.Logs.Add( new Log()
            {
                 Info = "RejectInitiator "+ userid,
                 Level = Log.Levels.Debug,
                 MethodName = "RejectInitiator"
            });
                db.SaveChanges();
            }
        }


        public void ResetRejectedUnread(int roomId)
        {
            var UserId = OnlineUsers[Context.ConnectionId].Id.ToString();
            db.UsersRooms.FirstOrDefault(s => s.UserId == UserId && s.RoomId == roomId).UnreadRejected = 0;
            db.UsersRooms.FirstOrDefault(s => s.UserId == UserId && s.RoomId == roomId).ReadAllMessages();

            var u = OnlineUsers.Where(s=>s.Value.UserRooms.Select(d=>d.RoomId).Contains(roomId));
            u.ForEach(r =>
            {

                Clients.Client(r.Key).ChangeMessageStatus(new { RoomId = roomId, Status = "Read"});
                
            });

            db.SaveChanges();
        }

        public void UserAction(int roomId, string action)
        {
            //var UserId = OnlineUsers[Context.ConnectionId].Id.ToString();
            //var online = OnlineUsers.Where(x => ids.Contains(x.Value.Id)).ToList();

        }

        public void HangUp(string userid, int roomId)
        {

            var UserIDCall = OnlineUsers[Context.ConnectionId].Id.ToString();
            Clients.Client(Context.ConnectionId).HangUp(userid, roomId);
            var k = OnlineUsers.Where(x => x.Value.Id == userid).LastOrDefault().Key;
            if (!String.IsNullOrEmpty(k))
            {
               
                // проверка на последний незавершенный звонок
                var lastCall = db.Calls.Where(x => (x.CallerId == userid &&
                                              x.AcceptorId == UserIDCall) || 
                                             (x.CallerId == UserIDCall &&
                                              x.AcceptorId == userid) &&
                                              x.RoomId == roomId).OrderByDescending(a => a.DateAdd)
                                            .FirstOrDefault();
                if (lastCall != null && (lastCall.Status == Call.CallStatus.started ||
                                    lastCall.Status == Call.CallStatus.onpause))
                {

                    Clients.Client(k).HangUp(UserIDCall, roomId);

                    string caller, acceptor, connection;
                    if (lastCall.CallerId == UserIDCall)
                    {
                        caller = UserIDCall;
                        acceptor = userid;
                        connection = Context.ConnectionId;
                    }
                    else
                    {
                        caller = lastCall.CallerId;
                        acceptor = UserIDCall;
                        connection = lastCall.ConnectionId;
                    }

                    db.Calls.Add(new Call
                    {
                        CallerId = caller,
                        AcceptorId = acceptor,
                        RoomId = roomId,
                        ConnectionId = connection,
                        Status = Call.CallStatus.ended
                    });

                    var time = DateTime.Now - lastCall.DateAdd;

                    db.Messages.Add(new Message
                    {
                        RoomId = roomId,
                        UserId = caller,
                        Body = JsonConvert.SerializeObject(new { Time = time, UserId = lastCall.AcceptorId }),
                        Type = Message.TypeMessage.HangUp
                    });
                    db.SaveChanges();
                }
            }
        }


        /// <summary>
        /// SDP сообщения и Candidates для осуществления RTC сеанса
        /// </summary>
        /// <param name="message"></param>
        public void RTCMessage(SignalingMessage message)
        {
            var to = OnlineUsers.Where(x => x.Value.Id == message.To).LastOrDefault().Key;

            if (to != null)
            {
                var user = OnlineUsers[Context.ConnectionId];
                message.From = user.Id;
                Clients.Client(to).RTCMessage(message);
            }
        }

        public void RoomMessage(SignalingMessage message)
        {

            var err = new { UserId = message.To, RoomId = message.RoomId, InternalId = message.InternalId };

            var user = OnlineUsers[Context.ConnectionId];
            message.From = user.Id;

            //простая проверка на наличие данных по комнате
            if (String.IsNullOrEmpty(message.RoomId.ToString()) ||
                String.IsNullOrEmpty(message.To) ||
                String.IsNullOrEmpty(message.Type.ToString())
              )
            {
                Clients.Client(Context.ConnectionId).RoomMessageError(err);
                return;
            }

            var to = OnlineUsers.Where(x => x.Value.Id == message.To).FirstOrDefault().Key;
            var room = db.Rooms.Where(x => x.Id == message.RoomId).FirstOrDefault();

            if (room != null)
            {

                //если пользователя нет в комнате выкидываем ошибку
                if (!room.UserRooms.Any(i => i.UserId == user.Id))
                {
                    Clients.Client(Context.ConnectionId).RoomMessageError(err);
                    return;
                }

                var mtype = PushMessage.TypeMedia.Text;

                var m = new Message
                {
                    UserId = user.Id,
                    RoomId = room.Id
                };

                switch (message.Type)
                {
                    case ("Text"):
                        m.Body = message.Body.Text;
                        break;
                    case ("Location"):
                        m.Latitude = message.Body.Latitude;
                        m.Longitude = message.Body.Longitude;
                        m.Zoom = message.Body.Zoom;
                        m.Type = Message.TypeMessage.Location;
                        if (!String.IsNullOrEmpty((string)message.Body.FileName))
                        {
                            string img = message.Body.FileName;
                            var imgFile = db.Files.Where(f => f.FileName == img).FirstOrDefault();
                            if (imgFile != null)
                            {
                                m.File = imgFile;
                                mtype = PushMessage.TypeMedia.Content;
                            }
                        }

                        break;
#warning протестить case or condition
                    case ("File"):
                    case ("Image"):
                        string fileName = message.Body.FileName;
                        var file = db.Files.Where(f => f.FileName == fileName).FirstOrDefault();
                        if (file != null)
                        {
                            m.File = file;
                            m.Type = Message.TypeMessage.File;
                            mtype = PushMessage.TypeMedia.Content;
                        }
                        else
                        {
                            //  если файла нет выкидываем ошибку и завершаем процсс передачи сообщения
                            Clients.Client(Context.ConnectionId).ErrorMessage(err);
                            return;
                        }
                        break;
                }


                //если не админ и если комната евент или специализация         
                if ((room.TypeRoom == Room.RoomType.Event || room.TypeRoom == Room.RoomType.Specialisation)
                    && room.UserRooms.Any(er => er.UserId == user.Id && er.Role != UserRoom.MemberRole.Admin))
                {
                    var cost = room.CostPerMessage;
                    if (cost != 0)
                    {
                        if (user.Balance >= cost)
                        {
                            user.MoneyTransfer(cost, message.To);

                            Clients.Client(Context.ConnectionId).BalanceInfo(new
                            {
                                Status = BalanceEvent.Ok,
                                Message = "Success",
                                RoomId = room.Id,
                                InternalId = message.InternalId
                            });
                        }
                        else
                        {
                            Clients.Client(Context.ConnectionId).BalanceInfo(new
                            {
                                Status = BalanceEvent.NotEnougth,
                                Message = "Not enougth money",
                                RoomId = room.Id,
                                InternalId = message.InternalId
                            });
                            return;
                        }
                    }
                }

                //если клиент онлайн отправляем ему сигнальное сообщение
                if (to != null)
                {
                    Clients.Client(to).RoomMessage(message);
                }

                var nm = db.Messages.Add(m);
                db.SaveChanges();

                //если не онлайн отправляем ему push
                //Отправка пуш уведомлений пользователям в комнате
                foreach (var item in room.UserRooms)
                {
                    if (item.UserId != user.Id)
                    {
                        string text = message.Body.Text;
                        item.User.CallNotification(PushMessage.TypePushEvent.Message, mtype, message, user, text);
                    }
                }

                Clients.Client(Context.ConnectionId).RoomMessageSuccess(new { UserId = message.To, RoomId = room.Id, MessageId = nm.Id, InternalId = message.InternalId });               
                db.SaveChanges();
                return;
            }
            Clients.Client(Context.ConnectionId).RoomMessageError(err);
        }
        /// <summary>
        /// Метод если клиент отключился
        /// </summary>
        /// <param name="stopCalled"></param>
        /// <returns></returns>
        public override Task OnDisconnected(bool stopCalled)
        {
            if (OnlineUsers.ContainsKey(Context.ConnectionId))
            {

                //отправить всем друзьям, что юзер офлайн
                //получить список всех зареганных друзей юзера
                var u = OnlineUsers[Context.ConnectionId];
                var ids = u.GetFriends().Select(x => x.Id);
                // получить из них всех кто онлайн
                var online = OnlineUsers.Where(x => ids.Contains(x.Value.Id)).ToList();
               

#warning надо протестировать функционал при обрыве активной связи
                // проверка на все незавершенные звоноки пользователя
                var lastCall = db.Calls.Where(x => (x.CallerId == u.Id ||
                                            x.AcceptorId == u.Id)).OrderByDescending(a => a.DateAdd).FirstOrDefault();

               
                if (lastCall.Status == Call.CallStatus.started || lastCall.Status == Call.CallStatus.onpause)
                {
                    db.Calls.Add(new Call
                    {
                        CallerId = lastCall.CallerId,
                        AcceptorId = lastCall.AcceptorId,
                        RoomId = lastCall.RoomId,
                        Status = Call.CallStatus.ended,
                        ConnectionId = lastCall.ConnectionId
                            
                    });

                    online.ForEach(x => {
                        Clients.Client(x.Key).ChangeFriendState(new { Status = "Offline", Id = u.Id });
                        if (x.Value.Id == lastCall.CallerId)
                        {
                            Clients.Client(x.Key).HangUp(lastCall.AcceptorId, lastCall.RoomId);
                        } else if (x.Value.Id == lastCall.AcceptorId)
                        {
                            Clients.Client(x.Key).HangUp(lastCall.CallerId, lastCall.RoomId);
                        }
                    });

                    var time = DateTime.Now.AddSeconds(-30) - lastCall.DateAdd;

                    db.Messages.Add(new Message
                    {
                        RoomId = lastCall.RoomId,
                        UserId = lastCall.CallerId,
                        Body = JsonConvert.SerializeObject(new { Time = time, UserId = lastCall.AcceptorId }),
                        Type = Message.TypeMessage.HangUp
                    });
                }
                else if(lastCall.Status == Call.CallStatus.calling)
                {
                    db.Calls.Add(new Call
                    {
                        CallerId = lastCall.CallerId,
                        AcceptorId = lastCall.AcceptorId,
                        RoomId = lastCall.RoomId,
                        Status = Call.CallStatus.rejected,
                        ConnectionId = lastCall.ConnectionId
                    });

                    online.ForEach(x => {
                        Clients.Client(x.Key).ChangeFriendState(new { Status = "Offline", Id = u.Id });
                        if (x.Value.Id == lastCall.CallerId)
                        {
                            Clients.Client(x.Key).RejectCall(lastCall.AcceptorId);
                        }
                        else if (x.Value.Id == lastCall.AcceptorId)
                        {
                            Clients.Client(x.Key).RejectCall(lastCall.CallerId);
                        }
                    });

                    db.Messages.Add(new Message
                    {
                        RoomId = lastCall.RoomId,
                        UserId = lastCall.CallerId,
                        Type = Message.TypeMessage.RejectCall,
                        Body = lastCall.AcceptorId.ToString()
                    });
                } else
                {
                    online.ForEach(x => {
                        Clients.Client(x.Key).ChangeFriendState(new { Status = "Offline", Id = u.Id });
                    });
                }

                db.SaveChanges();
                OnlineUsers.Remove(Context.ConnectionId);
            }
            _clients = Clients;
            return base.OnDisconnected(stopCalled);
        }
        public override Task OnConnected()
        {
            _clients = Clients;
            //var version = Context.QueryString["contosochatversion"];
            //if (version != "1.0")
            //{
            //    Clients.Caller.notifyWrongVersion();
            //}
            return base.OnConnected();
        }
    }
}