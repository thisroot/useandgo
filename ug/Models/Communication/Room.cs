﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace ug.Models
{
    //https://github.com/ISBX/apprtc-node-server/blob/master/lib/rooms.js

    public class Room
    {

        public static ApplicationDbContext db = new ApplicationDbContext();
        public Room()
        {
            this.Messages = new HashSet<Message>();
            this.UserRooms = new HashSet<UserRoom>();
        }

        public enum RoomType
        {
            Friends = 1,
            Event = 2,
            Specialisation = 3
        }
 
        public int Id { get; set; }
        public string RoomName { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<UserRoom> UserRooms { get; set; }
        public bool IsDeleted { get; set; } = false;
        public RoomType TypeRoom { get; set; } = RoomType.Friends;
        private decimal _CostPerMessage { get; set; }
        [NotMapped]
        public decimal CostPerMessage
        {
            get
            {
                if (TypeRoom == RoomType.Event)
                {
                    try { return db.EEvents.FirstOrDefault(s => s.RoomId == Id).CostPerMessage; }
                    catch { return 0; }
                }
                else if (TypeRoom == RoomType.Specialisation)
                {
                    try { return db.UsersSubscribtions.FirstOrDefault(s => s.RoomId == Id).UserSpecialisation.CostPerMessage; }
                    catch { return 0; }
                }
                else { return 0; }
            }
        }

        public DateTime DateAdd { get; set; } = DateTime.Now;
        public DateTime DateUpdate { get; set; } = DateTime.Now;
        public List<ApplicationUser> Join(string phone)
        {
            var user = db.Users.FirstOrDefault(u => u.UserName == phone);
            // если данного пользователя еще нет в комнате, добавим его
            if (!db.UsersRooms.Where(u => u.UserId == user.Id).Any())
            {
                db.UsersRooms.Add(new UserRoom { User = user, Room = this });
                db.SaveChanges();
            }
            return  db.UsersRooms.Where(r=>r.RoomId == this.Id).Select(u=> u.User).ToList();
        }
        public List<ApplicationUser> RemoveUser(string phone)
        {
            var user = db.Users.FirstOrDefault(u => u.UserName == phone);
            // если данный пользователь есть в комнате, удаляем его
            var deluser = db.UsersRooms.Where(u => u.UserId == user.Id).LastOrDefault();
            if (deluser != null)
            {
                db.UsersRooms.Remove(deluser);
                db.SaveChanges();
            }
            return db.UsersRooms.Where(r => r.RoomId == this.Id).Select(u => u.User).ToList();
        }

    }
}