﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ug.Models
{
    /// <summary>
    /// Друзья пользователя
    /// </summary>
    public class UserRoom
    {

        public enum  MemberRole
        {
            Free = 0,
            Admin = 1,
            Moderator = 2,
            Vip = 3,
        }

        public enum MediaPermission
        {
            AllMedia = 0,
            Streamer = 1,
            Viewer = 2
        }

        public int Id { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        [JsonIgnore]
        public virtual  ApplicationUser User { get; set; }
        public int RoomId { get; set; }
        [ForeignKey("RoomId")]
        [JsonIgnore]
        public virtual Room Room { get; set; }
        public MemberRole Role { get; set; } = MemberRole.Free;
        public MediaPermission MPermission { get; set; } = MediaPermission.AllMedia;
        public bool ChatWriter { get; set; } = true;
        public bool FileTransfer { get; set; } = true;
        public int UnreadRejected { get; set; } = 0;
        private int _UnreadMessage;
        [NotMapped]
        //[Computed]
        public int UnreadMessages {
            get {
                return this.Room.Messages
                    .Where(a => a.Status == Message.StatusMessage.New && a.UserId != this.UserId && a.Type == Message.TypeMessage.Text)
                    .Count();
            }

            set
            {
                _UnreadMessage = value;
            }
        }

        public void ReadAllMessages()
        {
            var mess = this.Room.Messages
                    .Where(a => a.Status == Message.StatusMessage.New);
            if (mess == null) return;
            foreach (var item in mess)
            {
                item.Status = Message.StatusMessage.Read;
            }
        }

        public bool IsDeleted { get; set; } = false;
        public bool IsBlocked { get; set; } = false;
        /// <summary>
        /// Дата добавления в друзья
        /// </summary>
        
        public DateTime DateAdd { get; set; } = DateTime.Now;
        
        public DateTime DateUpdate { get; set; } = DateTime.Now;
    }
}