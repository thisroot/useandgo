﻿using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace ug.Models.Communication
{
    public class Call
    {
        public enum CallStatus
        {
            calling = 1,
            started = 2,
            onpause = 3,
            ended = 4,
            rejected = 5,
        };
        public int Id { get; set; }
        public string ConnectionId { get; set; }
        public string CallerId { get; set; }
        [ForeignKey("CallerId")]
        public virtual ApplicationUser Caller { get; set; }
        public string AcceptorId { get; set; }
        [ForeignKey("AcceptorId")]
        public virtual ApplicationUser Acceptor { get; set; }
        public int RoomId { get; set; }
        [ForeignKey("RoomId")]
        public virtual Room Room { get; set; }
        public CallStatus? Status { get; set; }
        public bool IsDeleted { get; set; }
        
        public DateTime DateAdd { get; set; } = DateTime.Now;
        
        public DateTime DateUpdate { get; set; } = DateTime.Now;
    }
}