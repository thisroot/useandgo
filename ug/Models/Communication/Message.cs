﻿using System;
using System.Data.Entity;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using Newtonsoft.Json;

namespace ug.Models
{
   
    public class Message
    {

        public enum TypeMessage
        {
            Text = 1,
            File = 2,
            Location = 3,
            StartCall = 4,
            RejectCall = 5,
            HangUp = 6
        }

        public enum StatusMessage
        {
            New = 1,
            Read = 2,
        }

        public int Id { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        public int RoomId { get; set; }
        [ForeignKey("RoomId")]
        public virtual Room Room { get; set; }
        public TypeMessage Type { get; set; } = TypeMessage.Text;
        public StatusMessage Status { get; set; } = StatusMessage.New;
        public bool IsDeleted { get; set; } = false;
        public int? FileId {get;set;}
        [ForeignKey("FileId")]
        public virtual AppFile File { get; set; }
        [StringLength(4065)]
        public string Body { get; set; }
        public float? Latitude { get; set; }
        public float? Longitude { get; set; }
        public float? Zoom { get; set; }
        
        public DateTime DateAdd { get; set; } = DateTime.Now;
        
        public DateTime DateUpdate { get; set; } = DateTime.Now;
    }
}