﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using ug.Models.Specialisations;
using ug.Models.Simple;
using ug.Models;
using ug.Models.Events;
using ug.Models.StreamManager;
using EntityFramework.DynamicFilters;
using ug.Models.Communication;
using ug.Models.Money;

namespace ug.Models
{

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        /// <summary>
        /// Логи
        /// </summary>
        public DbSet<Log> Logs { get; set; }
        /// <summary>
        /// Коды подтверждения
        /// </summary>
        public DbSet<UserConfirmationCodes> UsersCodes { get; set; }
        /// <summary>
        /// Отправленые смс
        /// </summary>
        public DbSet<SendSms> SendSms { get; set; }
        /// <summary>
        /// Токены выданные пользователям
        /// </summary>
        public DbSet<UserToken> UsersTokens { get; set; }
        /// <summary>
        /// Друзья пользователя
        /// </summary>
        public DbSet<UserFriend> UserFriends { get; set; }
        /// <summary>
        /// Друзья пользователя
        /// </summary>
        public DbSet<UserInvitation> UserInvitations { get; set; }
        /// <summary>
        /// Загружаемые файлы
        /// </summary>
        public DbSet<AppFile> Files { get; set; }

        /// <summary>
        /// Комнаты созданные для комуникаций
        /// не дописаный метод
        /// </summary>
        public DbSet<Room> Rooms { get; set; }

        public DbSet<Contact> Contacts { get; set; }

        public DbSet<UserRoom> UsersRooms { get; set; }

        public DbSet<Specialisation> Specialisations { get; set; }

        public DbSet<SpecialisationCategory> SpecialisationsCategories { get; set; }

        public DbSet<UserSpecialisations> UsersSpecialisations { get; set; }

        public DbSet<Message> Messages { get; set; }
        public DbSet<Call> Calls { get; set; }

        public DbSet<AndroidChanel> AndroidChanels { get; set; }
        public DbSet<iOSChanel> iOSChanels { get; set; }

        public DbSet<UserSubscribtions> UsersSubscribtions { get; set; }

        //Event models
        public DbSet<EWebinar> EWebinars { get; set; }
        public DbSet<EBattle> EBattles { get; set; }
        public DbSet<EChallenge> EChallenges { get; set; }
        public DbSet<EStandUp> EStandUps { get; set; }
        public DbSet<EStream> EStreams { get; set; }
        ////вьюха
        public DbSet<EEvent> EEvents { get; set; }
      
        public DbSet<StreamEvent> StreamEvents { get; set; }
        //Money
        public DbSet<UserTransaction> UsersTransactions { get; set; }
      
        public override IDbSet<ApplicationUser> Users { get; set; }

        public ApplicationDbContext()  
            : base("DefaultConnection", throwIfV1Schema: false)
        {
          
        }

        public ApplicationDbContext(string DefaultConnection)
           : base(DefaultConnection, throwIfV1Schema: false)
        {

        }


        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Filter("IsDeleted", (UserRoom d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (ApplicationUser d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (UserToken d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (UserFriend d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (UserSubscribtions d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (UserInvitation d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (AppFile d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (Message d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (Call d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (Room d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (Contact d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (EBattle d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (EChallenge d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (EEvent d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (EStandUp d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (EWebinar d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (EStream d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (SpecialisationCategory d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (Specialisation d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (UserSpecialisations d) => d.IsDeleted, false);
            modelBuilder.Filter("IsDeleted", (UserTransaction d) => d.IsDeleted, false);
        }
    }


    public static class DbSetExtensions
    {
        
#warning перенести в расширения
        public static T AddIfNotExists<T>(this DbSet<T> dbSet, T entity, Expression<Func<T, bool>> predicate = null) where T : class, new()
        {
            var exists = predicate != null ? dbSet.Any(predicate) : dbSet.Any();
            return !exists ? dbSet.Add(entity) : null;
            
        }
    }


}