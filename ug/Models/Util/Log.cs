﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ug.Models
{
    public class Log
    {
        public enum Levels
        {
            Fatal = 1,
            Error = 2,
            Warn = 3,
            Info = 4,
            Debug = 5,
            Trace = 6
        }
        public int id { get; set; }
        public Levels Level { get; set; }
        public string MethodName { get; set; }
        public string Info { get; set; }
        
        public DateTime DateAdd { get; set; } = DateTime.Now;
        
        public DateTime DateUpdate { get; set; } = DateTime.Now;
    }
}