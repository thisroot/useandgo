﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace ug.Models
{
    /// <summary>
    /// СМС сообщение
    /// </summary>
    public class SendSms
    {
        public int id { get; set; }
        /// <summary>
        /// Телефон на который отправляем
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Какой метод инициализоварол отправку
        /// </summary>
        public string MethodInit { get; set; }
        /// <summary>
        /// Дата отправки
        /// </summary>
        
        public DateTime DateAdd { get; set; } = DateTime.Now;
        /// <summary>
        /// Доставленно или не доставлено
        /// </summary>
        public bool State { get; set; } = false;
        /// <summary>
        /// Отправить смс
        /// </summary>
        internal void Send()
        {
#warning закомментить
            //return;
            ApplicationDbContext db = new ApplicationDbContext();
#if DEBUG
            db.Logs.Add(new Log()
            {
                DateAdd = DateTime.Now,
                DateUpdate = DateTime.Now,
                Info = Phone + "SEND SMS : " + Text + " answer server gate sms -:",
                Level = Log.Levels.Debug,
                MethodName = "SendSms->Send"
            });
            return;
#endif
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://json.gate.iqsms.ru/send/");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"login\":\"z1481139324578\",\"password\":\"277967\",\"messages\":[{\"clientId\":\"1\",\"phone\":\"" + Phone + "\",\"text\":\"" + Text + "\"}]}";
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                db.Logs.Add(new Log()
                {
                    DateAdd = DateTime.Now,
                    DateUpdate = DateTime.Now,
                    Info = Phone + "SEND SMS : " + Text + " answer server gate sms -:" + result,
                    Level = Log.Levels.Debug,
                    MethodName = "SendSms->Send"
                });
                State = true;
                db.SaveChanges();
            }
        }
    }
}