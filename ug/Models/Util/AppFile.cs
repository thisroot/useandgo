﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;


namespace ug.Models
{
    /// <summary>
    /// Загруженые файлы
    /// </summary>
    public class AppFile
    {
        public enum TypeFile
        {
            Image = 1,
            Archive = 2,
            Document = 3,
            Other = 4,
        }

        public int Id { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        public string FileName { get; set; }
        public string Extension { get; set; }
        public long FileSize { get; set; }
        public string ContentType { get; set; }
        
        public DateTime DateAdd { get; set; } = DateTime.Now;
       
        public DateTime DateDelete { get; set; } = DateTime.Now;
        /// <summary>
        /// Тип загруженного файла
        /// </summary>
        public TypeFile TypeUploadFile { get; set; }
        public bool IsDeleted { get; set; } = false;
        public bool IsPrivate { get; set; } = false;
    }
}