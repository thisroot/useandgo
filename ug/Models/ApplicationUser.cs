﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using ug.Models.Specialisations;
using ug.Models.Simple;
using ug.Models;
using Newtonsoft.Json;
using ug.Models.Money;

namespace ug.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {

        private ApplicationDbContext db = new ApplicationDbContext();

        public ApplicationUser() { }
        public ApplicationUser(SimpleUserCard ucard)
        {
            this.Id = ucard.Id;
            this.FirstName = ucard.FirstName;
            this.SecondName = ucard.SecondName;
            this.Patronymic = ucard.Patronymic;
            this.AvatarId = ucard.AvatarId;
            this.DateBirth = ucard.DateBirth;
        }

        [Display(Name = "ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Nummer { get; set; }
        [Display(Name = "Фамилия")]
        public string SecondName { get; set; }
        [Display(Name = "Имя")]
        public string FirstName { get; set; }
        [Display(Name = "Отчество")]
        public string Patronymic { get; set; }
        [Display(Name = "Дата рождения")]
        [Required]
        public DateTime DateBirth { get; set; } = DateTime.Now;
        [Display(Name = "Дата регистрации")]
        public DateTime DateAdd { get; set; } = DateTime.Now;     
        public DateTime DateUpdate { get; set; } = DateTime.Now;
        
        public DateTime LastLogin { get; set; } = DateTime.Now;
        public virtual List<UserRoom> UserRooms { get; set; }
        public string AvatarId { get; set; }
        public bool IsDeleted { get; set; } = false;
        public bool IsValidate { get; set; } = false;
        public string GetId()
        {
            return this.Id;
        }

#warning Расширить написать тут приглашения которые у пользователя висят

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        /// <summary>
        /// Отправити уведомление
        /// </summary>
        /// <param name="Mess">Сообщение</param>
        /// <returns></returns>
        public bool SendNotification(string Mess, int roomId)
        {
            
            if (db.AndroidChanels.Where(x => x.UserId == Id && !x.IsDelete).Any())
            {
                var n = db.AndroidChanels.Where(x => x.UserId == Id && !x.IsDelete).FirstOrDefault();
                n.Send(Mess, roomId);
                db.Logs.Add(new Log()
                {
                    Info = "Попытка отправки уведомления на android на номер " + PhoneNumber + " текст сообщения:" + Mess,
                    Level = Log.Levels.Debug,
                    MethodName = "SendNotification->AndroidChanels"
                });
                db.SaveChanges();
            }
            if (db.iOSChanels.Where(x => x.UserId == Id && !x.IsDelete).Any())
            {
                var n = db.iOSChanels.Where(x => x.UserId == Id && !x.IsDelete && !x.IsVoIP).FirstOrDefault();
                n.Send(Mess, false);
                db.Logs.Add(new Log()
                {
                    Info = "Попытка отправки уведомления на iOS на номер " + PhoneNumber + " текст сообщения:" + Mess,
                    Level = Log.Levels.Debug,
                    MethodName = "SendNotification->iOSChanels"
                });
                db.SaveChanges();
            }
            return true;
        }

        public bool CallNotification(PushMessage.TypePushEvent ev, PushMessage.TypeMedia tm, SignalingMessage  message, ApplicationUser sender, string text)
        {
            if (db.AndroidChanels.Where(x => x.UserId == Id && !x.IsDelete).Any())
            {
                foreach (var channel in db.AndroidChanels.Where(x => x.UserId == Id && !x.IsDelete))
                {
                    channel.Send(new PushMessage()
                    {
                        MediaType = tm,
                        PushEvent = ev,
                        UserId = message.From,
                        RoomId = message.RoomId,
                        UserName = sender.FirstName +" "+ sender.SecondName,
                        UserPhone = sender.PhoneNumber,
                        Message = Newtonsoft.Json.JsonConvert.SerializeObject(new { message.Body }),
                        Title = text
                    });
                }
                // var n = db.AndroidChanels.Where(x => x.UserId == Id && !x.isDelete).FirstOrDefault();

                db.Logs.Add(new Log()
                {
                    Info = "Попытка отправки уведомления на android на номер " + PhoneNumber + " CallNotification",
                    Level = Log.Levels.Debug,
                    MethodName = "SendNotification->AndroidChanels"
                });
                db.SaveChanges();
            }

            if (db.iOSChanels.Where(x => x.UserId == Id && !x.IsDelete).Any())
            {

                foreach (var channel in db.iOSChanels.Where(x => x.UserId == Id && !x.IsDelete))
                {
                    if ((ev == PushMessage.TypePushEvent.Call ||
                         ev == PushMessage.TypePushEvent.Reject) && channel.IsVoIP)
                    {
#warning пока шлем строку, но надо перейти к структуре, указывая лишь тип пуша 
                        channel.Send(new PushMessage()
                        {
                            MediaType = tm,
                            PushEvent = ev,
                            UserId = message.From,
                            RoomId = message.RoomId,
                            UserName = sender.FirstName +" "+ sender.SecondName,
                            UserPhone = sender.PhoneNumber,
                            Message = Newtonsoft.Json.JsonConvert.SerializeObject( new { message.Body }),
                            Title = text
                        }, true);
                    }
                    else if ((ev != PushMessage.TypePushEvent.Call ||
                             ev != PushMessage.TypePushEvent.Reject) && !channel.IsVoIP)
                    {
                        channel.Send(new PushMessage()
                        {
                            MediaType = tm,
                            PushEvent = ev,
                            UserId = message.From,
                            RoomId = message.RoomId,
                            UserName = sender.FirstName + " "+ sender.SecondName,
                            UserPhone = sender.PhoneNumber,
                            Message = Newtonsoft.Json.JsonConvert.SerializeObject( new { message.Body }),
                            Title = text
                        }, false);
                    }
                }

                db.Logs.Add(new Log()
                {
                    Info = "Попытка отправки уведомления на iOs на номер " + PhoneNumber + " CallNotification",
                    Level = Log.Levels.Debug,
                    MethodName = "SendNotification->iOSChanels"
                });

                db.SaveChanges();
            }
            return true;
        }

        public bool CallNotification(PushMessage.TypePushEvent ev,PushMessage.TypeMedia tm, string UserId, int Room, string UserName, string UserPhone)
        {
            if (db.AndroidChanels.Where(x => x.UserId == Id && !x.IsDelete).Any())
            {
                foreach (var channel in db.AndroidChanels.Where(x => x.UserId == Id && !x.IsDelete))
                {
                    channel.Send(new PushMessage
                    {
                        MediaType = tm,
                        PushEvent = ev,
                        UserId = UserId,
                        RoomId = Room,
                        UserName = UserName,
                        UserPhone = UserPhone,
                    });
                }
                // var n = db.AndroidChanels.Where(x => x.UserId == Id && !x.isDelete).FirstOrDefault();

                db.Logs.Add(new Log()
                {
                    Info = "Попытка отправки уведомления на android на номер " + PhoneNumber + " CallNotification",
                    Level = Log.Levels.Debug,
                    MethodName = "SendNotification->AndroidChanels"
                });
                db.SaveChanges();
            }

            if (db.iOSChanels.Where(x => x.UserId == Id && !x.IsDelete).Any())
            {
                
                foreach (var channel in db.iOSChanels.Where(x => x.UserId == Id && !x.IsDelete))
                {
                    if ((ev == PushMessage.TypePushEvent.Call || 
                         ev == PushMessage.TypePushEvent.Reject ) && channel.IsVoIP == true)
                    {

                        channel.Send(new PushMessage()
                        {
                            MediaType = tm,
                            PushEvent = ev,
                            UserId = UserId,
                            RoomId = Room,
                            UserName = FirstName +" "+ SecondName,
                        }, true);
                    } else if(ev == PushMessage.TypePushEvent.Message && channel.IsVoIP == false)
                    {

                        channel.Send(new PushMessage()
                        {
                            MediaType = tm,
                            PushEvent = ev,
                            UserId = UserId,
                            RoomId = Room,
                            UserName = FirstName +" "+ SecondName
                        }, false);
                    }
                }

                    db.Logs.Add(new Log()
                {
                    Info = "Попытка отправки уведомления на iOs на номер " + PhoneNumber + " CallNotification",
                    Level = Log.Levels.Debug,
                    MethodName = "SendNotification->iOSChanels"
                });

                db.SaveChanges();
            }
            return true;
        }

        public List<UserFriend> AddFriend(ApplicationUser friend)
        {

            var UF = new List<UserFriend>();

            //Создадим комнату
            var room = db.Rooms.Add(new Room { IsDeleted = true, TypeRoom = Room.RoomType.Friends });

            //Получим существующие связи при их наличии
            var oldfriends = db.UserFriends.Where(e => ((e.UserId == this.Id && e.UserIdFriend == friend.Id) ||
                                                        (e.UserId == friend.Id && e.UserIdFriend == this.Id)) &&
                                                        e.IsDeleted == false);

            //Если нет связи
            if (oldfriends.Where(e => (e.UserId == this.Id && e.UserIdFriend == friend.Id && e.IsDeleted == false)).Any() == false)
            {
                // Добавим в друзья
                var uf = db.UserFriends.Add(new UserFriend { UserId = this.Id, UserIdFriend = friend.Id });
                //Добавим в комнату
                room.IsDeleted = false;
                db.UsersRooms.Add(new UserRoom { UserId = this.Id, Room = room });
                UF.Add(uf);
            }

            if (oldfriends.Where(b => (b.UserId == friend.Id && b.UserIdFriend == this.Id && b.IsDeleted == false)).Any() == false)
            {
                // Добавим в друзья
                var uf = db.UserFriends.Add(new UserFriend { UserId = friend.Id, UserIdFriend = this.Id });
                //Добавим в комнату
                room.IsDeleted = false;
                db.UsersRooms.Add(new UserRoom { UserId = friend.Id, Room = room });
                UF.Add(uf);
            }

            db.SaveChanges();
            return UF;
        }


        public List<ApplicationUser> GetFriends()
        {
            // получаем список друзей пользователя
            return (from uf in db.UserFriends
                    join u1 in db.Users on uf.UserId equals u1.Id
                    join u2 in db.Users on uf.UserIdFriend equals u2.Id
                    where u1.Id == this.Id && uf.IsDeleted == false
                    select u2
                          ).ToList();
        }

        private decimal _Balance;

        [NotMapped]
        public decimal Balance
        {
            get {
                //return db.UsersTransactions.Where(w => w.UserId == Id).Any() ? db.UsersTransactions.Where(w => w.UserId == Id).Sum(e => e.Value) : 0;
                try
                {
                    return db.UsersTransactions.Where(w => w.UserId == Id).Sum(e => e.Value);
                }
                catch
                {
                    return 0;
                }
            } 
            set
            {
                _Balance = value;
            }
         
        }

        //операция со знаком
        public decimal MoneyTransaction(decimal m)
        {
            var added =  db.UsersTransactions.Add(new UserTransaction()
            {
                UserId = Id,
                Value = m
            });

            db.SaveChanges();

            return added.Value;
        }
        //перевод от одного пользователя, другому
        public List<UserTransaction> MoneyTransfer(decimal m, string userId)
        {
            var added = db.UsersTransactions.AddRange(
                new List<UserTransaction>()
                {
                    {
                        new UserTransaction()
                        {
                            UserId = Id,
                            Value = -m
                        }
                    },
                    {
                        new UserTransaction()
                        {
                            UserId = userId,
                            Value = m
                        }
                    }
                });

            db.SaveChanges();

            return added.ToList();
        }
        //пополнение на сумму
        public decimal MoneyWriteOn(decimal m)
        {
            var added = db.UsersTransactions.Add(new UserTransaction()
            {
                UserId = Id,
                Value = m
            });

            db.SaveChanges();

            return added.Value;
        }
        //списание суммы
        public decimal MoneyWriteOff(decimal m)
        {
            var added = db.UsersTransactions.Add(new UserTransaction()
            {
                UserId = Id,
                Value = -m
            });

            db.SaveChanges();

            return added.Value;
        }
    }
}