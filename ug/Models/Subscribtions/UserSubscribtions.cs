﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using ug.Models.Specialisations;

namespace ug.Models
{
    /// <summary>
    /// Подписки пользователей
    /// на специализации
    /// </summary>
    public class UserSubscribtions
    {
        public enum Satus
        {
            Open = 1, //открытая подписка
            Freeze = 2, // доступ в стадии согласования
            Blocked = 3 // доступ закрыт
        }
        public int Id { get; set; }
        public int SpecialisationId { get; set; }
        [ForeignKey("SpecialisationId")]
        [JsonIgnore]
        public virtual UserSpecialisations UserSpecialisation { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        public string OnSubscribeId { get; set; }
        [ForeignKey("OnSubscribeId")]
        public virtual ApplicationUser OnSubscribeUser { get; set; }
        public Satus Status { get; set; } = Satus.Open;
        public bool IsDeleted { get; set; } = false;
        public DateTime DateAdd { get; set; } = DateTime.Now;
        public DateTime DataUpdate { get; set; } = DateTime.Now;
    }
}