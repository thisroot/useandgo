﻿using MoreLinq;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using ug.Controllers;
using ug.Models.Simple;

namespace ug.Models
{
    /// <summary>
    /// Контакты пользователя
    /// </summary>
    public class Contact
    {

        static private ApplicationDbContext db = new ApplicationDbContext();

        public int Id { get; set; }
        /// <summary>
        /// Гуид сгенерированый на клиенте
        /// </summary>
        public string LocalId { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Company { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        public string FriendId { get; set; }
        [ForeignKey("FriendId")]
        public virtual ApplicationUser Friend { get; set; }
        public string Phone { get; set; }
        
        public DateTime DateAdd { get; set; } = DateTime.Now;
        
        public DateTime DateUpdate { get; set; } = DateTime.Now;
        public bool IsDeleted { get; set; } = false;


#warning Отрефакторить перенести на odata по индексу будет фильтр
//        public static List<ContactRequest> getFriends(string userId)
//        {
//            //var user = db.Users.FirstOrDefault(u => u.UserName == phone);
//            // получаем контакты друзей пользователя
//            var query = (from ph in db.Phones
//                        join c in db.Contacts on ph.ContactId equals c.id
//                        join pr in db.UserFriends on ph.ContactUserId equals pr.UserIdFriend
//                        where c.UserId == userId && c.IsDeleted == false
//                         select c).DistinctBy(z=>z.UserId).ToList();

//            //получаем комнаты для связи с друзьями пользователя
//            var rooms = (from uf in db.UsersRooms
//                        where ((from ur in db.UsersRooms
//                                join u1 in db.UsersRooms on ur.RoomId equals u1.RoomId
//                                where ur.UserId == userId
//                                select ur.RoomId).ToList().Distinct().Contains(uf.RoomId)
//                               && uf.UserId != userId)
//                        select uf).ToList();

//            //создаем ответ
//            var newContacts = new List<ContactRequest>();

//            foreach(var item in query)
//            {
//                var friendIDs = item.Phones.Select(x => x.ContactUserId).ToList();

//                var contResp = new ContactRequest(){
//                    Company = item.Company,
//                    FirstName = item.FirstName,
//                    LocalId = item.LocalId,
//                    SecondName = item.SecondName
//                };

//                var listPhones = new List<PhonesRequest>();
//                foreach (var item2 in friendIDs)
//                {
                    
//                    var room = rooms.Where(a => a.UserId == item2).LastOrDefault();
//                    if(room != null)
//                    {

//                        var newphone = new PhonesRequest()
//                        {
//                            RoomId = room.RoomId,
//                            Phone = room.User.PhoneNumber,
//                            ContactUserId = item2
//                        };

//                        listPhones.Add(newphone);
//                    }
//                }
//                contResp.Phones = listPhones;
//                newContacts.Add(contResp);
//            }

//            return newContacts;
//        }


//        public static Contact Remove(ContactRequest contact, string phone)
//        {
//            var user = db.Users.FirstOrDefault(u => u.UserName == phone);
//            if (user != null)
//            {
//                var newcontact = new Contact
//                {
//                    UserId = user.Id,
//                    LocalId = contact.LocalId,
//                    FirstName = contact.FirstName,
//                    SecondName = contact.SecondName,
//                    Company = contact.Company,
//                };

//                foreach (var item in contact.Phones.Select(x=>x.Phone))
//                {
//                    var tel = new Phone
//                    {
//                        Tel = item,
//                        ContactId = newcontact.id
//                    };

//                    newcontact.Phones.Add(tel);
//                }

//                db.Contacts.Remove(newcontact);
//                db.SaveChanges();
//                return newcontact;
//            }

//            return null;

//        }


//        public static void Add(List<ContactRequest> contacts, string userId, string mode = "add")
//        {
          
//            if (userId != null)
//            {
//                // если mode с перезаписью
//                if (mode == "rw")
//                {

//                    //помечаем контакты как удаленные
//                    db.Contacts.Where(u => u.UserId == userId).ForEach(u =>
//                    {
//                        u.IsDeleted = true;
//                    });
//                    db.SaveChanges();
//                } else
//                {
//#warning метод не работает
                   
//                    //удаляем дубликаты по номеру телефона
//                    contacts = ExcludeRegisteredPhones(contacts,userId);
//                    //db.Set<Contact>().AddIfNotExists()
//                }

//                foreach (var item in contacts)
//                {
//                    // если не передан localId пропускаем
//                    if (!string.IsNullOrEmpty(item.LocalId))
//                    {
//                        var contact = new Contact()
//                        {
//                            FirstName = item.FirstName,
//                            SecondName = item.SecondName,
//                            Company = item.Company,
//                            UserId = userId,
//                            LocalId = item.LocalId,
//                        };

//                        foreach (var phone in item.Phones)
//                        {
//                            db.Phones.Add(new Phone() { Tel = phone.Phone, Contact = contact });
//                        }

//                        db.Contacts.Add(contact);
//                    }
//                }

//                db.SaveChanges();
//            }   
//        }


//        public static List<ContactRequest>ExcludeRegisteredPhones(List<ContactRequest> contacts, string UserId)
//        {
//            var userPhones = db.Contacts.Where(x => x.UserId == UserId && x.IsDeleted == false)
//                                        .SelectMany(z => z.Phones.Select(e=>e.Tel)).ToList();

//            var newContacts = new List<ContactRequest>();

//            foreach (var item in contacts)
//            {
//                if (!userPhones.Any(e => item.Phones.Select(x => x.Phone).Contains(e)))
//                {
//                    newContacts.Add(item);
//                }
//            }
//            return newContacts;
//        }

       
//        public static ContactRequest ExcludeNotRegisteredPhones(ContactRequest contact, List<ApplicationUser> users)
//        {
//            var Phones = new List<PhonesRequest>(contact.Phones);
//            contact.Phones.Clear();
//            foreach (var reguser in users)
//            {
//                if (Phones.Select(x=>x.Phone).Contains(reguser.PhoneNumber))
//                {
//                    contact.Phones.Add(new PhonesRequest { Phone  = reguser.PhoneNumber, ContactUserId = reguser.Id });
//                }
//            }
//            return contact;
//        }

//        public static ContactRequest FindFromRegisteredUsers(ContactRequest contact)
//        {
//            var phones = contact.Phones.Select(x=>x.Phone);
//            // ищем среди зареганных юзеров
//            var result = db.Users.Where(u => phones.Contains(u.PhoneNumber)).ToList();
//            // исключаем из контактов номера, на которых нет зарегестрированных аккаунтов
//            contact = ExcludeNotRegisteredPhones(contact, result);
//            // исключаем все контакты, которые не содержат зареганных номеров
//            return contact.Phones.Select(x=>x.Phone).Count() != 0 ? contact : null;
//        }

//        public static List<ContactRequest> FindFromRegisteredUsers(List<ContactRequest> contacts)
//        {
//            // выделяем телефоны из списка классов
//            var phones = contacts.SelectMany(x => x.Phones.Select(z=>z.Phone)).ToList().Distinct().ToList();

//            // ищем среди зареганных юзеров
//            //var result = db.Users.Where(u => phones.Contains(u.PhoneNumber)).Select(p => p.PhoneNumber.ToString()).ToList(); //.OfType<string>()
//            // фильтруем входящий список возвращает пользоваателей со всеми телефонами
//            //var newcontacts = contacts.Where(u => u.Phones.Any(p => result.Contains(p))).ToList();

//            // вернуть список пользователей и исключить все телефоны на которых они не зареганы

//            // ищем среди зареганных юзеров
//            var result = db.Users.Where(u => phones.Contains(u.PhoneNumber)).ToList();

//            // исключаем из контактов номера, на которых нет зарегестрированных аккаунтов
//            for (int i = 0; i < contacts.Count; ++i)
//            {
//                contacts[i] = ExcludeNotRegisteredPhones(contacts[i], result);
//            }

//            // исключаем все контакты, которые не содержат зареганных номеров
//            return contacts.Where(u => u.Phones.Count() != 0).ToList();
//        }

    }
}