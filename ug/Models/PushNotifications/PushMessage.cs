﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ug.Models.Simple
{
    /// <summary>
    /// Без добавления в базу данных
    /// </summary>
    public class PushMessage
    {
        public enum TypePushEvent
        {
            Call = 1,
            Reject = 2,
            AcceptCall = 3,
            Message = 4,
            Stream = 5
        }

        public enum TypeMedia
        {
            Video = 1,
            Audio = 2,
            Text = 3,
            Content = 4
        }

        public string Title { get; set; }
        public string FileId { get; set; }
        public string Message { get; set; }
        public TypePushEvent PushEvent { get; set; }
        public TypeMedia MediaType { get; set; } = TypeMedia.Video;
        public string UserId { get; set; }
        public int RoomId { get; set; }
        public string UserName { get; set; }
        public string UserPhone { get; set; }
    }
}