﻿using Newtonsoft.Json.Linq;
using PushSharp.Apple;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using PushSharp;
using ug.Models.Simple;
using System.Web.Configuration;
using System.Configuration;
using Newtonsoft.Json;

namespace ug.Models
{

    /// <summary>
    /// Уведомления для IOS
    /// </summary>
    public class iOSChanel
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public int Id { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        public DateTime DateAdd { get; set; }
        ///
        //Клиенский канал куда отправлять
        ///
        public string Ids_chanel { get; set; }
        public bool IsDelete { get; set; }
        public bool IsVoIP { get; set; } = false;

        public bool Send(string message, bool isVoIP)
        {
            return Send(new PushMessage { Message = message }, isVoIP);
        }
        /// <summary>
        /// Отправить уведомление
        /// </summary>
        /// <returns></returns>
        public bool Send(PushMessage message, bool IsVoIP)
        {
            string path;
            ApnsConfiguration config;

            var compilation = (CompilationSection)ConfigurationManager
                      .GetSection("system.web/compilation");

            if (compilation.Debug)
            {
                if (IsVoIP)
                {
                    path = HttpContext.Current.Server.MapPath("~/ugv.p12");
                    config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, path, "house1", false);
                }
                else
                {
                    path = HttpContext.Current.Server.MapPath("~/ug.p12");
                    config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Sandbox, path, "house1");
                }
            }
            else
            {
                if (IsVoIP)
                {
                    path = HttpContext.Current.Server.MapPath("~/ugv.p12");
                    config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, path, "house1", false);
                }
                else
                {
                    path = HttpContext.Current.Server.MapPath("~/ug.p12");
                    config = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production, path, "house1");
                }
            }

            var apnsBroker = new ApnsServiceBroker(config);
          
            apnsBroker.OnNotificationSucceeded += (notification) =>
            {
                Console.WriteLine();
                
                db.Logs.Add(new Log()
                {
                    Info = "Apple Notification Sent!",
                    Level = Log.Levels.Info,
                    MethodName = "ApnsBroker_OnNotificationSuccessed"
                });
                db.SaveChanges();
            };

            

            int unreadRejected = 0;
            int unreadDialogs = 0;
            int unreadMessages = 0;
           
            dynamic title = message.Message != null ? JsonConvert.DeserializeObject(message.Message) : null;
            dynamic body =  message.Message != null ? JsonConvert.DeserializeObject(message.Message) : null;
            body = body != null ? body.Body.Text : "";

            if (message.PushEvent == PushMessage.TypePushEvent.Reject || message.PushEvent == PushMessage.TypePushEvent.Message)
            {
                //сброшенные звонки
                unreadRejected = db.UsersRooms.Where(s => s.UserId == UserId).Sum(s=>s.UnreadRejected);

                //количество непрочяитанных сообщений
                //unreadMessages = db.UsersRooms.Where(s => s.UserId == message.UserId)
                //    .Sum(s => s.Room.Messages
                //    .Where(d => 
                //    d.Status == Message.StatusMessage.New & d.UserId != UserId).Count()
                //);

                unreadMessages = db.UsersRooms.Where(s => s.UserId == UserId)    
                    .Sum(s => s.Room.Messages
                    .Where(d =>
                    d.Status == Message.StatusMessage.New && d.UserId != UserId  && d.Type == Message.TypeMessage.Text).Count()
                );

                //var countD = db.UsersRooms.Where(s => s.UserId == UserId).ToList();
                //unreadDialogs = countD.Where(s => s.UnreadMessages > 0).Any() ? countD.Where(s => s.UnreadMessages > 0).Count() : 0;     
            }

            //else if(message.PushEvent == PushMessage.TypePushEvent.Message)
            //{
            //    var countD = db.UsersRooms.Where(s => s.UserId == UserId).ToList();
            //    unreadDialogs = countD.Where(s => s.UnreadMessages > 0).Any() ? countD.Where(s => s.UnreadMessages > 0).Count() : 0;
            //}

            apnsBroker.Start();
            apnsBroker.QueueNotification(new ApnsNotification
            {
                DeviceToken = Ids_chanel,
                //Payload = JObject.Parse("{\"aps\":{\"alert\":\"" + message + "\",\"badge\":\"1\"}}")
                
                Payload = JObject.Parse(JsonConvert.SerializeObject(new {
                    aps = new {
                        alert = new {
                            title = message.UserName,
                            body = body
                        },
                        badge = unreadRejected + unreadDialogs + unreadMessages,
                        sound = "default",
                        message = message
                    } }))
            });
            apnsBroker.OnNotificationFailed += ApnsBroker_OnNotificationFailed;
            return true;
        }

        private void ApnsBroker_OnNotificationFailed(ApnsNotification notification, AggregateException exception)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            db.Logs.Add(new Log()
            {
                Info = "Ошибка Попытка отправки уведомления на iOS:" + exception.Message+" "+ exception.InnerException+" q "+ exception.StackTrace,
                Level = Log.Levels.Error,
                MethodName = "ApnsBroker_OnNotificationFailed"
            });
            db.SaveChanges();
        }
    }
}