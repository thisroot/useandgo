﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using PushSharp.Google;
using ug.Models.Simple;

namespace ug.Models
{
    public class AndroidChanel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        
        public DateTime DateAdd { get; set; }
        ///
        //Клиенский канал куда отправлять
        ///
        public string Ids_chanel { get; set; }
        public bool IsDelete { get; set; }
        /// <summary>
        /// Отправить уведомление
        /// </summary>
        /// <returns></returns>
        public bool Send(string Message, int RoomId)
        {
            var config = new GcmConfiguration("3dcafe.ru", FastConf.server_key_android, null);
            return Send(new PushMessage() { Message = Message, MediaType = PushMessage.TypeMedia.Text,RoomId = RoomId});
        }

        public bool Send(PushMessage message)
        {
            var config = new GcmConfiguration("3dcafe.ru", FastConf.server_key_android, null);
            config.GcmUrl = "https://fcm.googleapis.com/fcm/send";
            var gcmBroker = new GcmServiceBroker(config);
            gcmBroker.Start();

            gcmBroker.OnNotificationSucceeded += (notification) =>
            {
                Console.WriteLine();
                ApplicationDbContext db = new ApplicationDbContext();
                db.Logs.Add(new Log()
                {
                    Info = "Android Notification Sent!",
                    Level = Log.Levels.Info,
                    MethodName = "ApnsBroker_OnNotificationSuccessed"
                });
                db.SaveChanges();
            };

            gcmBroker.OnNotificationFailed += GcmBroker_OnNotificationFailed;


            gcmBroker.QueueNotification(new GcmNotification
            {
                RegistrationIds = new List<string> { Ids_chanel },
                Data = JObject.FromObject(message)
            });
            return true;
        }

        private void GcmBroker_OnNotificationFailed(GcmNotification notification, AggregateException exception)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            db.Logs.Add(new Log()
            {
                Info = "Ошибка Попытка отправки уведомления на Android:" + exception.Message + " " + exception.InnerException + " q " + exception.StackTrace,
                Level = Log.Levels.Error,
                MethodName = "ApnsBroker_OnNotificationFailed"
            });
            db.SaveChanges();
        }
    }
}