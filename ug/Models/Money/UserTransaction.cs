﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ug.Models.Money
{
    /// <summary>
    /// Пользовательские денежные транзакции
    /// </summary>
    public class UserTransaction
    {
        public int Id { get; set; }
        public decimal Value { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime DateAdd { get; set; } = DateTime.Now;
    }
}