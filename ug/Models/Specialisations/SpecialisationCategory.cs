﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ug.Models.Specialisations
{
    public class SpecialisationCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public bool IsSystem { get; set; }

        public bool IsDeleted { get; set; }

        public virtual string Description { get; set; }
       
        public virtual List<Specialisation> Specialisations { get; set; }
        
        public DateTime DateAdd { get; set; } = DateTime.Now;
        
        public DateTime DateUpdate { get; set; } = DateTime.Now;
    }
}