﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using ug.Models.Specialisations;

namespace ug.Models
{

    public class Specialisation
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual SpecialisationCategory Category { get; set; }

        public bool IsSystem { get; set; } = false;

        public bool IsDeleted { get; set; } = false;
        
        public DateTime DateAdd { get; set; } = DateTime.Now;
        
        public DateTime DateUpdate { get; set; } = DateTime.Now;
    }
}