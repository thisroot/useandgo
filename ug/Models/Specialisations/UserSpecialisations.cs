﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ug.Models.Specialisations
{
    public class UserSpecialisations
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        public int SpecialisationId { get; set; }
        [ForeignKey("SpecialisationId")]
        public virtual Specialisation Specialisation { get; set; }
        public virtual List<UserSubscribtions>  Subscribtions { get; set; }
        public string Profession { get; set; }
        public string Description { get; set; }
        public decimal CostPerMinuteCall { get; set; } = 0;
        public decimal CostPerMessage { get; set; } = 0;
        public bool IsDeleted { get; set; } = false;
        public bool IsValidate { get; set; } = false;
        public DateTime Experience { get; set; } = DateTime.Now;

        public DateTime DateAdd { get; set; } = DateTime.Now;
       
        public DateTime DateUpdate { get; set; } = DateTime.Now;
    }
}