﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ug.Models.Simple;

namespace ug.Models.Events
{
    public interface IEEvents
    {
        void ChangeStatus(StatusEvent se);
        UserRoom Subscribe(ApplicationUser user, EventSubscribeRequest sr);
    }
}
