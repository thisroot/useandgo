﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using ug.Models.Specialisations;

namespace ug.Models.Events
{

    public enum StatusEvent
    {
        NotStarted = 0,
        Started = 1,
        Finished = 2,
        Blocked = 3,
        Pending = 4
    }

    [Table("Events")]
    public class EEvent
    {
        [Key]
        [Column(TypeName = "bigint")]
        public Int64 IdEvent { get; set; }
        public string Event { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? FileId { get; set; }
        [ForeignKey("FileId")]
        public virtual AppFile File { get; set; }
        public int RoomId { get; set; }
        [ForeignKey("RoomId")]
        public virtual Room Room { get; set; }
        public StatusEvent EventStatus { get; set; } = StatusEvent.NotStarted;
        [Required]
        /// <summary>
        /// Организатор
        /// </summary>
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        [JsonIgnore]
        public virtual ApplicationUser User { get; set; }
        public int? SpecialisationId { get; set; }
        [ForeignKey("SpecialisationId")]
        public virtual UserSpecialisations Specialisation { get; set; }
        /// <summary>
        /// Стоимость / ограничения
        /// </summary>
        public decimal CostPerEvent { get; set; } = 0;
        public decimal CostPerEventVIP { get; set; } = 0;
        public decimal CostPerMessage { get; set; } = 0;
        public decimal CostPerMinute { get; set; } = 0;
        public int? LimitTickets { get; set; }
        public decimal CostPerMinuteVIP { get; set; } = 0;
        public int? LimitVIPTickets { get; set; }
        public bool OnlyVIP { get; set; } = false;
        /// <summary>
        /// Время организации события
        /// </summary>
        
        public DateTime DateStart { get; set; } = DateTime.Now.AddHours(1);
        
        public DateTime? DateEnd { get; set; }
        public bool IsDeleted { get; set; } = false;
        
        public DateTime DateAdd { get; set; } = DateTime.Now;
        
        public DateTime DateUpdate { get; set; } = DateTime.Now;

        public Object GetObject(ApplicationDbContext db)
        {
            switch (this.Event)
            {
                case ("webinar"):
                    return db.EWebinars.Where(s => s.Id == this.Id).FirstOrDefault();
                case ("standup"):
                    return db.EStandUps.Where(s => s.Id == this.Id).FirstOrDefault();
                case ("battle"):
                    return db.EBattles.Where(s => s.Id == this.Id).FirstOrDefault();
                case ("challenge"):
                    return db.EChallenges.Where(s => s.Id == this.Id).FirstOrDefault();
                case ("stream"):
                    return db.EStreams.Where(s => s.Id == this.Id).FirstOrDefault();
                default:
                    return this;
            }
        }
    }
}