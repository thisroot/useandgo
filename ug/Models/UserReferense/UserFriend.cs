﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace ug.Models
{

    public static class ExtensionMethods
    {
        public static IEnumerable<TA> Except<TA, TB, TK>(
            this IEnumerable<TA> a,
            IEnumerable<TB> b,
            Func<TA, TK> selectKeyA,
            Func<TB, TK> selectKeyB,
            IEqualityComparer<TK> comparer = null)
        {
            return a.Where(aItem => !b.Select(bItem => selectKeyB(bItem)).Contains(selectKeyA(aItem), comparer));
        }
    }

    /// <summary>
    /// Друзья пользователя
    /// </summary>
    public class UserFriend
    {

        static private ApplicationDbContext db = new ApplicationDbContext();


        public int Id { get; set; }
        public bool IsDeleted { get; set; } = false;
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        /// <summary>
        /// Друзья пользователя
        /// </summary>
        public string UserIdFriend { get; set; }
        [ForeignKey("UserIdFriend")]
        public virtual ApplicationUser FriendUser { get; set; }
        /// <summary>
        /// Дата добавления в друзья
        /// </summary>
        public DateTime DateAdd { get; set; } = DateTime.Now;
        public DateTime DateUpdate { get; set; } = DateTime.Now;


       
        public static List<ApplicationUser> GetFriends(string id)
        {
            // получаем список друзей пользователя
            var query = (from uf in db.UserFriends
                         join u1 in db.Users on uf.UserId equals u1.Id
                         join u2 in db.Users on uf.UserIdFriend equals u2.Id
                         where u1.Id == id && uf.IsDeleted == false
                         select u2
                         ).ToList();

            return query;
        }

        
        public static void Add(UserInvitation ui)
        {
            var UserId = ui.UserId;
            string FriendId = ui.UserIdFriend;

            if (UserId == FriendId) return;

            var room = new Room() { IsDeleted = true };
            room = db.Rooms.Add(room);

            if (db.UserFriends.Where(e=>(e.UserId == UserId && e.UserIdFriend == FriendId  && e.IsDeleted == false)).Any() == false)
            {
                // Добавим в друзья
                db.UserFriends.Add(new UserFriend { UserId = UserId, UserIdFriend = FriendId});
                //Добавим в комнату
                room.IsDeleted = false;
                db.UsersRooms.Add(new UserRoom { UserId = UserId, Room = room });

                //обновим id пользователя в контактах
#warning Стоит ли хранить Id друга в контактах
                //db.Contacts.Where(b => b.Phone == db.Users.Where(x => x.Id == FriendId).Select(s => s.PhoneNumber).FirstOrDefault()).ToList().ForEach(c =>
                //{
                //    c.FriendId = FriendId;
                //});
            }

            if (db.UserFriends.Where(e => (e.UserId == FriendId && e.UserIdFriend == UserId && e.IsDeleted == false)).Any() == false)
            {
                // Добавим в друзья
                db.UserFriends.Add(new UserFriend { UserId = FriendId, UserIdFriend = UserId });
                room.IsDeleted = false;
                db.UsersRooms.Add(new UserRoom { UserId = FriendId, Room = room });
#warning Стоит ли хранить Id друга в контактах
                //db.Contacts.Where(b => b.Phone == db.Users.Where(x => x.Id == UserId).Select(s => s.PhoneNumber).FirstOrDefault()).ToList().ForEach(c =>
                //{
                //    c.FriendId = UserId;
                //});
            }

            db.SaveChanges();
        }
        
        /// <summary>
        /// УДАЛЯЕТ И ИЗ СВОИХ ДРУЗЕЙ И СЕБЯ ИЗ ДРУГЕЙ ДРУГА
        /// </summary>
        public void Remove()
        {

            var friends = db.UserFriends.Where(u => (
                                                        (u.UserIdFriend == this.UserId && u.UserId == this.UserIdFriend) || 
                                                        (u.UserIdFriend == this.UserIdFriend && u.UserId == this.UserId)
                                                    )  
                                                    
                                                    && u.IsDeleted == false);
            if (friends != null)
            {

                var user = friends.FirstOrDefault();

                //SELECT
                //  r2.RoomId
                // ,r2.id
                //FROM dbo.UserRooms r2
                //WHERE r2.RoomId IN(SELECT
                //    r1.RoomId
                //  FROM dbo.UserRooms r1
                //  WHERE r1.UserId = '1a02c186-a062-4444-92ef-815e38641f6b')
                //AND r2.UserId = '3751615e-4a1c-42f0-b4da-d605fae5d70d'
                //AND r2.IsDeleted = 'false'

                string userIdfriend = user.UserIdFriend;
                string userId = user.UserId;

                // удалить из комнат
                var rooms = db.Rooms.Where(z => z.UserRooms.Select(v => v.UserId).Contains(userId) &&
                                               z.UserRooms.Select(v => v.UserId).Contains(userIdfriend)
                );

                foreach(var room in rooms)
                {
                    room.IsDeleted = true;
                   
                    foreach(var urooms in room.UserRooms)
                    {
                        urooms.IsDeleted = true;
                    }
                }

            
                foreach (var friend in friends)
                {
                    friend.IsDeleted = true;
                }

                //добавить в инвайты
                db.UserInvitations.Add(new UserInvitation
                {
                    UserId = this.UserIdFriend,
                    UserIdFriend = this.UserId
                });

                db.SaveChanges();
            }
        }
    }
}