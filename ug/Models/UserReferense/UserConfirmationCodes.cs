﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ug.Models
{
    public class UserConfirmationCodes
    {
        public int id { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        public string Code { get; set; }
        public DateTime DateAdd { get; set; }
    }
}