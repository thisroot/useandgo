﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ug.Models
{
    /// <summary>
    /// Приглашения от польователей
    /// </summary>
    public class UserInvitation
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; } = false;
        /// <summary>
        /// Кто приглашает
        /// </summary>
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        /// <summary>
        /// Кого приглашает
        /// </summary>
        public string UserIdFriend { get; set; }
        [ForeignKey("UserIdFriend")]
        public virtual ApplicationUser FriendUser { get; set; }
        /// <summary>
        /// Дата добавления в друзья
        /// </summary>
        
        public DateTime DateAdd { get; set; } = DateTime.Now;
       
        public DateTime DateUpdate { get; set; } = DateTime.Now;
    }
}