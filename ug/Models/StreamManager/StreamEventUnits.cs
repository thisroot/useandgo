﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ug.Models.StreamManager
{
    public class StreamEventRequest
    {
        public string action { get; set; }
        public int client_id { get; set; }
        public string ip { get; set; }
        public string vhost { get; set; }
        public string app { get; set; }
        public string stream { get; set; }
        public string tcUrl { get; set; }
        public string pageUrl { get; set; }
        public int? send_bytes { get; set; }
        public int? recv_bytes { get; set; }
        public string cwd { get; set; }
        public string file { get; set; }
        public string duration { get; set; }
        public string url { get; set; }
        public string m3u8 { get; set; }
        public string m3u8_url { get; set; }
        public string seq_no { get; set; }
    }

    public enum TypeStreamEvent
    {
        Connect = 1,
        Close = 2,
        Publish = 3,
        UnPublish = 4,
        Play = 5,
        Stop = 6,
        Dvr = 7
    }

    public enum TypeStreamer
    {
        Undefined = 0,
        Publisher = 1,
        Viewer = 2
    }
}