﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Text;

namespace ug.Models.StreamManager
{
    public class StreamEventStreamer
    {
        public int Id { get; set; }
        public TypeStreamEvent EventStream { get; set; }
        public int ClientId { get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        public int? EventId { get; set; }
        public string Ip { get; set; }
        public string Vhost { get; set; }
        public string App { get; set; }
        public string Stream { get; set; }
        public string TcUrl { get; set; }
        public string PageUrl { get; set; }
        public int? SendBytes { get; set; }
        public int? RecvBytes { get; set; }
        public string Cwd { get; set; }
        public string File { get; set; }
        public DateTime DateAdd { get; set; } = DateTime.Now;
        public Object GetObject(ApplicationDbContext db)
        {
            var evId = int.Parse(this.Stream);
            switch (this.App)
            {
                case ("webinar"):
                    return db.EWebinars.Where(s => s.Id == evId).FirstOrDefault();
                case ("standup"):
                    return db.EStandUps.Where(s => s.Id == evId).FirstOrDefault();
                case ("battle"):
                    return db.EBattles.Where(s => s.Id == evId).FirstOrDefault();
                case ("challenge"):
                    return db.EChallenges.Where(s => s.Id == evId).FirstOrDefault();
                case ("stream"):
                    return db.EStreams.Where(s => s.Id == evId).FirstOrDefault();
                default:
                    return this;
            }
        }
    }
}