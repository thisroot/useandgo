﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ug.Models.Simple
{
    public class FriendRequest
    {
        public FriendRequest()
        {

        }

        public FriendRequest(ApplicationUser uf, int roomId)
        {
            this.Id = uf.Id;
            this.FirstName = uf.FirstName;
            this.SecondName = uf.SecondName;
            this.Patronymic = uf.Patronymic;
            this.PhoneNumber = uf.PhoneNumber;
            this.RoomId = roomId;
            this.AvatarId = uf.AvatarId;
            this.BirthDate = uf.DateBirth;
            this.LastLogin = uf.LastLogin;
        }

        public string Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Patronymic { get; set; }
        public string PhoneNumber { get; set; }
        public int RoomId { get; set; }
        public string AvatarId { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime  LastLogin { get; set; }
    }
}