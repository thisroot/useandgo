﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ug.Models.Simple
{
    /// <summary>
    /// Обьект модели ICE серверов
    /// </summary>
    public class Turn
    {

        public Turn()
        {
            this.Urls = new List<string>(new string[] { "turn:5.101.78.155:3478" });
        }
        public List<string> Urls { get; set; }
        public string Username { get; set; } = "admin";
        public string Credential { get; set; } = "admin";
    }
}