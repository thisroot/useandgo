﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ug.Models.Simple
{
    public class EventSubscribeRequest
    {

        public enum TypeEvent
        {
            Webinar = 0,
            Battle = 1,
            Challange = 2,
            StandUp = 3,
            Stream = 4
        }
        
        public int EventId { get; set; }
        public TypeEvent EventType { get; set; }
        public bool IsVIP { get; set; } = false;
    }
}