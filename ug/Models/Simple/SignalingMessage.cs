﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ug.Models.Simple
{
    public class SignalingMessage
    {
        public string To { get; set; }
        public string From { get; set; }
        public string Type { get; set; }
        public int RoomId { get; set; }
        public string RoomType { get; set; }
        public dynamic Body { get; set; }
        public string InternalId { get; set; }
    };
}