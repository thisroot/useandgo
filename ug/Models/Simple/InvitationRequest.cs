﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ug.Models.Simple
{
    public class InvitationRequest
    {

        public InvitationRequest() { }

        public InvitationRequest(int Action, string UserId)
        {
            switch (Action)
            {
                case (1):
                    this.InvitationAction = InvAction.Add;
                    break;
                case (2):
                    this.InvitationAction = InvAction.Reject;
                    break;
                case (3):
                    this.InvitationAction = InvAction.Accept;
                    break;
            }

            this.UserId = UserId;

        }

        public enum InvAction
        {
            Add = 1, //добавить
            Reject = 2, // отклонить
            Accept = 3 // принять
        }

        public int Id { get; set; }
        public string UserId { get; set; }
        public InvAction InvitationAction { get; set; }
    }
}