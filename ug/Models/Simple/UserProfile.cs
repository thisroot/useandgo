﻿using MoreLinq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using ug.Models.Specialisations;

namespace ug.Models.Simple
{
#warning требует переработки, надо завести таблицы со счетчиками и ограничить вывод данных, эти данные только для построения виджетов.
    public class UserProfile
    {

        static private ApplicationDbContext db = new ApplicationDbContext();

        public UserProfile()
        {

        }
        /// <summary>
        /// self - флаг указывающий вернуть модель себя или модель другого пользователя
        /// </summary>
        /// <param name="us"></param>
        /// <param name="self"></param>
        public UserProfile(ApplicationUser user, bool self)
        {

            this.Id = user.Id;
            this.AvatarId = user.AvatarId;
            this.FirstName = user.FirstName;
            this.SecondName = user.SecondName;
            this.Patronymic = user.Patronymic;
            this.DateBirth = user.DateBirth;
            this.LastLogin = user.LastLogin;
            this.Rooms = new List<SimpleUserRoom>();
            this.Specialisations = new List<SimpleUserSpecialisations>();
            this.Subscribtions = new List<SimpleUserSubscribtions>();
            this.Subscribers = new List<SimpleUserSubscribtions>();
            this.Friends = new List<SimpleUserCard>();
            this.OutgoingInvitations = new List<SimpleUserCard>();
            this.IncomingInvitations = new List<SimpleUserCard>();

            this.Self = self;

            if (this.Self)
            {
                var rooms = (from ur in db.UsersRooms
                             join us in db.Users on ur.UserId equals us.Id
                             join r in db.Rooms on ur.RoomId equals r.Id
                             join ur2 in db.UsersRooms on r.Id equals ur2.RoomId
                             join us2 in db.Users on ur2.UserId equals us2.Id
                             where us.Id == user.Id && r.IsDeleted == false &&
                             ur.IsDeleted == false && ur2.IsDeleted == false
                             select new
                             {
                                 RoomId = r.Id,
                                 User = us2
                             }).ToList();

                var distRoom = rooms.DistinctBy(u => u.RoomId);

                if (distRoom != null)
                {

                    foreach (var room in distRoom)
                    {
                        var users = rooms.Where(r => r.RoomId == room.RoomId).Select(f => f.User).ToList();
                        if (users != null)
                        {
                            var lUsers = new List<SimpleUserCard>();
                            foreach (var us in users)
                            {
                                //исключаем себя из выдачи комнат
                                if (us.Id != user.Id || us != null)
                                {
                                    lUsers.Add(new SimpleUserCard(us, this.Self));
                                }
                            }

                            this.Rooms.Add(new SimpleUserRoom()
                            {
                                Id = room.RoomId,
                                Users = lUsers
                            });
                        }
                    }
                }

                this.Balance = user.Balance;
            }

            //получить специализации
            var specialisations = db.UsersSpecialisations.Where(u => u.UserId == user.Id && u.IsDeleted == false).ToList();
            if (specialisations != null)
            {
                foreach (var spec in specialisations)
                {
                    this.Specialisations.Add(new SimpleUserSpecialisations(spec, this.Self));
                }
            }

            //получить мои подписки
            var subscribtions = db.UsersSubscribtions.Where(u => u.UserId == user.Id && u.IsDeleted == false).ToList();
            if (subscribtions != null)
            {
                foreach (var subscr in subscribtions)
                {
                    this.Subscribtions.Add(new SimpleUserSubscribtions(subscr, this.Self, true));
                }
            }


            var friends = (from dbuser in db.Users
                           where dbuser.Id == user.Id
                           join uf in db.UserFriends on dbuser.Id equals uf.UserId
                           where uf.IsDeleted == false
                           join dbfruser in db.Users on uf.UserIdFriend equals dbfruser.Id
                           select dbfruser
                           ).ToList();

            if (friends != null)
            {
                foreach (var fr in friends)
                {
                    this.Friends.Add(new SimpleUserCard(fr, this.Self));
                }
            }

            var crooms = this.Rooms;
            var cspec = this.Specialisations;
            var csubscr = this.Subscribtions;
            
            if (csubscr != null)
            {
                // установим отдельно список подписчиков
                this.Subscribers = this.Specialisations.SelectMany(u => u.UserSubscribtions).ToList(); ;
            }
            var cfrends = this.Friends;
            if (self)
            {
                //исходящие приглашения в друзья
                var oinvitations = (from dbuser in db.Users
                                    where dbuser.Id == user.Id
                                    join uf in db.UserInvitations on dbuser.Id equals uf.UserId
                                    join dbfruser in db.Users on uf.UserIdFriend equals dbfruser.Id
                                    where uf.IsDeleted == false
                                    select dbfruser
                                   );
                if (oinvitations != null)
                {
                    foreach (var fr in oinvitations)
                    {
                        this.OutgoingInvitations.Add(new SimpleUserCard(fr, this.Self));
                    }
                }

                //входищей приглашения в друзья
                var iinvitations = (from dbuser in db.Users
                                    where dbuser.Id == user.Id
                                    join uf in db.UserInvitations on dbuser.Id equals uf.UserIdFriend
                                    join dbfruser in db.Users on uf.UserId equals dbfruser.Id
                                    where uf.IsDeleted == false
                                    select dbfruser
                                 );
                if (iinvitations != null)
                {
                    foreach (var fr in iinvitations)
                    {
                        this.IncomingInvitations.Add(new SimpleUserCard(fr, this.Self));
                    }
                }
            }

            var coinv = this.OutgoingInvitations;
            var ciinv = this.IncomingInvitations;



            if (self)
            {
                this.CountRooms = crooms != null ? crooms.Count() : 0;
                this.CountOutgoingInvitations = coinv != null ? coinv.Count() : 0;
                this.CountIncomingInvitations = ciinv != null ? ciinv.Count() : 0;
            }
            //счетчики


            this.CountSpecialisations = cspec != null ? cspec.Count() : 0;
            this.CountSubscribtions = csubscr != null ? csubscr.Count() : 0;
            this.CountSubscribers = this.Subscribers != null ? this.Subscribers.Count() : 0;
            this.CountFriends = cfrends != null ? cfrends.Count() : 0;

        }

        //Указывает на ApplicationUser.Id без связи
        public string Id { get; set; }
        public string AvatarId { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Patronymic { get; set; }
        
        public DateTime? DateBirth { get; set; }
        
        public DateTime? LastLogin { get; set; }
        public virtual List<SimpleUserRoom> Rooms { get; set; }
        public virtual List<SimpleUserSpecialisations> Specialisations { get; set; }
        //на что подписан юзер
        public virtual List<SimpleUserSubscribtions> Subscribtions { get; set; }
        //на какие подписки подписаны у него
        public virtual List<SimpleUserSubscribtions> Subscribers { get; set; }
        public virtual List<SimpleUserCard> Friends { get; set; }
        public virtual List<SimpleUserCard> OutgoingInvitations { get; set; }
        public virtual List<SimpleUserCard> IncomingInvitations { get; set; }

        public int CountRooms { get; set; }
        public int CountSpecialisations { get; set; }

        public int CountSubscribtions { get; set; }

        public int CountSubscribers { get; set; }

        public int CountFriends { get; set; }

        public int CountOutgoingInvitations { get; set; }
        public int CountIncomingInvitations { get; set; }
        public bool Self { get; set; } = false;
        public decimal Balance { get; set; } = 0;
    }

    public class SimpleUserCard
    {
        public SimpleUserCard() { }
        public SimpleUserCard(ApplicationUser us, bool mode)
        {

            this.Id = us.Id;
            this.FirstName = us.FirstName;
            this.SecondName = us.SecondName;
            this.AvatarId = us.AvatarId;
            this.DateBirth = us.DateBirth;
            this.Patronymic = us.Patronymic;
            if (mode)
            {
                this.PhoneNumber = us.PhoneNumber;
            }
        }

        public SimpleUserCard(UserFriend uf)
        {
            this.Id = uf.UserIdFriend;
        }

        [Required]
        public string Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string SecondName { get; set; }
        [Required]
        public string Patronymic { get; set; }
        [Required]
        public string AvatarId { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public DateTime DateBirth { get; set; }
        public int? CountSubscribers { get; set; }
        public int? CountSubscribtions { get; set; }
    }


    public class SimpleUserRoom
    {
        public SimpleUserRoom() { }
        public SimpleUserRoom(UserRoom ur)
        {
            if (ur.IsDeleted == false)
            {
                this.Id = ur.Id;
                this.DateAdd = ur.DateAdd;
                this.DateUpdate = ur.DateUpdate;
            }
        }
        public int Id { get; set; }
        public List<SimpleUserCard> Users { get; set; }
        
        public DateTime DateAdd { get; set; } = DateTime.Now;
        
        public DateTime DateUpdate { get; set; } = DateTime.Now;
    }

    public class SimpleUserSpecialisations
    {
        public SimpleUserSpecialisations() { }

        public SimpleUserSpecialisations(UserSpecialisations us, bool mode)
        {
            if (us.IsDeleted == false)
            {

                this.UserSubscribtions = new List<SimpleUserSubscribtions>();

                this.Id = us.Id;
                this.DateAdd = us.DateAdd;
                this.DateUpdate = us.DateUpdate;
                this.SpecialisationId = us.SpecialisationId;
                this.Profession = us.Profession;
                this.Description = us.Description;
                this.CostPerMinuteCall = us.CostPerMinuteCall;
                this.CostPerMessage = us.CostPerMessage;
                this.Experience = us.Experience;

                //перебрать всех подписчиков
                if (us.Subscribtions != null)
                {
                    foreach (var subscr in us.Subscribtions)
                    {
                        this.UserSubscribtions.Add(new SimpleUserSubscribtions(subscr, mode, false));
                    }
                }
            }
        }
        public int Id { get; set; }
        
        public DateTime DateAdd { get; set; } = DateTime.Now;
        public DateTime DateUpdate { get; set; } = DateTime.Now;
        public int SpecialisationId { get; set; }
        public string Profession { get; set; }
        public string Description { get; set; }
        public decimal CostPerMinuteCall { get; set; } = 0;
        public decimal CostPerMessage { get; set; } = 0;
        public DateTime Experience { get; set; } = DateTime.Now;
        public List<SimpleUserSubscribtions> UserSubscribtions { get; set; }
    }

    public class SimpleUserSubscribtions
    {
        public SimpleUserSubscribtions() { }
        public SimpleUserSubscribtions(UserSubscribtions us, bool mode, bool showSubscriber)
        {
            this.Id = us.Id;
            this.DateAdd = us.DateAdd;
            this.DateUpdate = us.DataUpdate;
            this.SpecialisationId = us.SpecialisationId;
            this.Profession = us.UserSpecialisation.Profession;
            this.Desciption = us.UserSpecialisation.Description;
            this.CostPerMinuteCall = us.UserSpecialisation.CostPerMinuteCall;
            this.CostPerMessage = us.UserSpecialisation.CostPerMessage;
            this.Experience =  us.UserSpecialisation.Experience;


            if (showSubscriber)
            {
                this.User = new SimpleUserCard(us.OnSubscribeUser, mode);
            }
            else
            {
                this.User = new SimpleUserCard(us.User, mode);
            }


        }

        public int Id { get; set; }
        public int SpecialisationId { get; set; }
       
        public DateTime DateAdd { get; set; } = DateTime.Now;
        
        public DateTime DateUpdate { get; set; } = DateTime.Now;
        public string Profession { get; set; }
        public string Desciption { get; set; }
        public decimal CostPerMinuteCall { get; set; }
        public decimal CostPerMessage { get; set; }
        public DateTime Experience { get; set; }
        public SimpleUserCard User { get; set; }
    }

    public class SimpleUserFriend
    {

        public SimpleUserFriend() { }

        string Id { get; set; }
        
        public DateTime DateAdd { get; set; } = DateTime.Now;
        
        public DateTime DateUpdate { get; set; } = DateTime.Now;
        public List<SimpleUserCard> Friends { get; set; }
    }

    public class SimpleUserInvitation
    {
        string Id { get; set; }
        
        public DateTime DateAdd { get; set; } = DateTime.Now;
        
        public DateTime DateUpdate { get; set; } = DateTime.Now;
        public List<SimpleUserCard> Invations { get; set; }
    }
}