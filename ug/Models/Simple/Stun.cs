﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ug.Models.Simple
{
    /// <summary>
    /// Обьект модели ICE серверов
    /// </summary>
    public class Stun
    {

        public Stun()
        {

            this.Urls = new List<string>(new string[] { "stun:stun.l.google.com:19302" });
        }
        public List<string> Urls { get; set; }
        public string Username { get; set; }
        public string Credential { get; set; }
    }
}