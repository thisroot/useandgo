﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ug.Models.Simple
{
    /// <summary>
    /// Модель сообщения с ICE серверами
    /// </summary>
    public class ICE
    {
        public Turn Turn { get; set; } = new Turn();
        public Stun Stun { get; set; } = new Stun();
    }
}