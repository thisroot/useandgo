namespace ug.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeStreamName : DbMigration
    {
        public override void Up()
        {
            Sql("DROP VIEW Events");
            Sql("CREATE VIEW Events AS WITH UnionedData " +
             "AS(SELECT 'battle' AS Event, * FROM EBattles eb " +
             "UNION ALL SELECT 'challange' AS Event, * FROM EChallenges ec " +
             "UNION ALL SELECT 'standup' AS Event, * FROM EStandUps eu " +
             "UNION ALL SELECT 'webinar' AS Event, * FROM EWebinars ew " +
             "UNION ALL SELECT 'stream' AS Event, * FROM EStreams es)" +
             "SELECT ROW_NUMBER() OVER(ORDER BY DateAdd) " +
             "AS IdEvent, * FROM UnionedData");
        }
        
        public override void Down()
        {
            Sql("DROP VIEW Events");
            Sql("CREATE VIEW Events AS WITH UnionedData " +
             "AS(SELECT 'battle' AS Event, * FROM EBattles eb " +
             "UNION ALL SELECT 'challange' AS Event, * FROM EChallenges ec " +
             "UNION ALL SELECT 'standup' AS Event, * FROM EStandUps eu " +
             "UNION ALL SELECT 'webinar' AS Event, * FROM EWebinars ew " +
             "UNION ALL SELECT 'streams' AS Event, * FROM EStreams es)" +
             "SELECT ROW_NUMBER() OVER(ORDER BY DateAdd) " +
             "AS IdEvent, * FROM UnionedData");
        }
    }
}
