namespace ug.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class firstCommit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AndroidChanels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        DateAdd = c.DateTime(nullable: false),
                        Ids_chanel = c.String(),
                        IsDelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Nummer = c.Int(nullable: false, identity: true),
                        SecondName = c.String(),
                        FirstName = c.String(),
                        Patronymic = c.String(),
                        DateBirth = c.DateTime(nullable: false),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        LastLogin = c.DateTime(nullable: false),
                        AvatarId = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        IsValidate = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ApplicationUser_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.UserRooms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        RoomId = c.Int(nullable: false),
                        Role = c.Int(nullable: false),
                        MPermission = c.Int(nullable: false),
                        ChatWriter = c.Boolean(nullable: false),
                        FileTransfer = c.Boolean(nullable: false),
                        UnreadRejected = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsBlocked = c.Boolean(nullable: false),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserRoom_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Rooms", t => t.RoomId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.RoomId);
            
            CreateTable(
                "dbo.Rooms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        TypeRoom = c.Int(nullable: false),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Room_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        RoomId = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        FileId = c.Int(),
                        Body = c.String(),
                        Latitude = c.Single(),
                        Longitude = c.Single(),
                        Zoom = c.Single(),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Message_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppFiles", t => t.FileId)
                .ForeignKey("dbo.Rooms", t => t.RoomId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.RoomId)
                .Index(t => t.FileId);
            
            CreateTable(
                "dbo.AppFiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        FileName = c.String(),
                        Extension = c.String(),
                        FileSize = c.Long(nullable: false),
                        ContentType = c.String(),
                        DateAdd = c.DateTime(nullable: false),
                        DateDelete = c.DateTime(nullable: false),
                        TypeUploadFile = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IsPrivate = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_AppFile_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Calls",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ConnectionId = c.String(),
                        CallerId = c.String(maxLength: 128),
                        AcceptorId = c.String(maxLength: 128),
                        RoomId = c.Int(nullable: false),
                        Status = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Call_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.AcceptorId)
                .ForeignKey("dbo.AspNetUsers", t => t.CallerId)
                .ForeignKey("dbo.Rooms", t => t.RoomId, cascadeDelete: true)
                .Index(t => t.CallerId)
                .Index(t => t.AcceptorId)
                .Index(t => t.RoomId);
            
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocalId = c.String(),
                        FirstName = c.String(),
                        SecondName = c.String(),
                        Company = c.String(),
                        UserId = c.String(maxLength: 128),
                        FriendId = c.String(maxLength: 128),
                        Phone = c.String(),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Contact_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.FriendId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.FriendId);
            
            CreateTable(
                "dbo.EBattles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        FileId = c.Int(),
                        RoomId = c.Int(nullable: false),
                        EventStatus = c.Int(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                        SpecialisationId = c.Int(),
                        CostPerEvent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostPerEventVIP = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostPerMinute = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LimitTickets = c.Int(),
                        CostPerMinuteVIP = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LimitVIPTickets = c.Int(),
                        OnlyVIP = c.Boolean(nullable: false),
                        DateStart = c.DateTime(nullable: false),
                        DateEnd = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EBattle_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppFiles", t => t.FileId)
                .ForeignKey("dbo.Rooms", t => t.RoomId, cascadeDelete: true)
                .ForeignKey("dbo.UserSpecialisations", t => t.SpecialisationId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.FileId)
                .Index(t => t.RoomId)
                .Index(t => t.UserId)
                .Index(t => t.SpecialisationId);
            
            CreateTable(
                "dbo.UserSpecialisations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        SpecialisationId = c.Int(nullable: false),
                        Profession = c.String(),
                        Description = c.String(),
                        CostPerMinuteCall = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostPerMessage = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDeleted = c.Boolean(nullable: false),
                        IsValidate = c.Boolean(nullable: false),
                        Experience = c.DateTime(nullable: false),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserSpecialisations_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Specialisations", t => t.SpecialisationId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.SpecialisationId);
            
            CreateTable(
                "dbo.Specialisations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        CategoryId = c.Int(nullable: false),
                        IsSystem = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Specialisation_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SpecialisationCategories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.SpecialisationCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsSystem = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        Description = c.String(),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SpecialisationCategory_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserSubscribtions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SpecialisationId = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128),
                        OnSubscribeId = c.String(maxLength: 128),
                        RoomId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DateAdd = c.DateTime(nullable: false),
                        DataUpdate = c.DateTime(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserSubscribtions_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.OnSubscribeId)
                .ForeignKey("dbo.Rooms", t => t.RoomId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .ForeignKey("dbo.UserSpecialisations", t => t.SpecialisationId, cascadeDelete: true)
                .Index(t => t.SpecialisationId)
                .Index(t => t.UserId)
                .Index(t => t.OnSubscribeId)
                .Index(t => t.RoomId);
            
            CreateTable(
                "dbo.EChallenges",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        FileId = c.Int(),
                        RoomId = c.Int(nullable: false),
                        EventStatus = c.Int(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                        SpecialisationId = c.Int(),
                        CostPerEvent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostPerEventVIP = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostPerMinute = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LimitTickets = c.Int(),
                        CostPerMinuteVIP = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LimitVIPTickets = c.Int(),
                        OnlyVIP = c.Boolean(nullable: false),
                        DateStart = c.DateTime(nullable: false),
                        DateEnd = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EChallenge_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppFiles", t => t.FileId)
                .ForeignKey("dbo.Rooms", t => t.RoomId, cascadeDelete: true)
                .ForeignKey("dbo.UserSpecialisations", t => t.SpecialisationId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.FileId)
                .Index(t => t.RoomId)
                .Index(t => t.UserId)
                .Index(t => t.SpecialisationId);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        IdEvent = c.Long(nullable: false, identity: true),
                        Event = c.String(),
                        Id = c.Int(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        FileId = c.Int(),
                        RoomId = c.Int(nullable: false),
                        EventStatus = c.Int(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                        SpecialisationId = c.Int(),
                        CostPerEvent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostPerEventVIP = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostPerMinute = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LimitTickets = c.Int(),
                        CostPerMinuteVIP = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LimitVIPTickets = c.Int(),
                        OnlyVIP = c.Boolean(nullable: false),
                        DateStart = c.DateTime(nullable: false),
                        DateEnd = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EEvent_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.IdEvent)
                .ForeignKey("dbo.AppFiles", t => t.FileId)
                .ForeignKey("dbo.Rooms", t => t.RoomId, cascadeDelete: true)
                .ForeignKey("dbo.UserSpecialisations", t => t.SpecialisationId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.FileId)
                .Index(t => t.RoomId)
                .Index(t => t.UserId)
                .Index(t => t.SpecialisationId);
            
            CreateTable(
                "dbo.EStandUps",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        FileId = c.Int(),
                        RoomId = c.Int(nullable: false),
                        EventStatus = c.Int(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                        SpecialisationId = c.Int(),
                        CostPerEvent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostPerEventVIP = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostPerMinute = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LimitTickets = c.Int(),
                        CostPerMinuteVIP = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LimitVIPTickets = c.Int(),
                        OnlyVIP = c.Boolean(nullable: false),
                        DateStart = c.DateTime(nullable: false),
                        DateEnd = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EStandUp_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppFiles", t => t.FileId)
                .ForeignKey("dbo.Rooms", t => t.RoomId, cascadeDelete: true)
                .ForeignKey("dbo.UserSpecialisations", t => t.SpecialisationId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.FileId)
                .Index(t => t.RoomId)
                .Index(t => t.UserId)
                .Index(t => t.SpecialisationId);
            
            CreateTable(
                "dbo.EStreams",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        FileId = c.Int(),
                        RoomId = c.Int(nullable: false),
                        EventStatus = c.Int(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                        SpecialisationId = c.Int(),
                        CostPerEvent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostPerEventVIP = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostPerMinute = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LimitTickets = c.Int(),
                        CostPerMinuteVIP = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LimitVIPTickets = c.Int(),
                        OnlyVIP = c.Boolean(nullable: false),
                        DateStart = c.DateTime(nullable: false),
                        DateEnd = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EStream_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppFiles", t => t.FileId)
                .ForeignKey("dbo.Rooms", t => t.RoomId, cascadeDelete: true)
                .ForeignKey("dbo.UserSpecialisations", t => t.SpecialisationId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.FileId)
                .Index(t => t.RoomId)
                .Index(t => t.UserId)
                .Index(t => t.SpecialisationId);
            
            CreateTable(
                "dbo.EWebinars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        FileId = c.Int(),
                        RoomId = c.Int(nullable: false),
                        EventStatus = c.Int(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                        SpecialisationId = c.Int(),
                        CostPerEvent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostPerEventVIP = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostPerMinute = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LimitTickets = c.Int(),
                        CostPerMinuteVIP = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LimitVIPTickets = c.Int(),
                        OnlyVIP = c.Boolean(nullable: false),
                        DateStart = c.DateTime(nullable: false),
                        DateEnd = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EWebinar_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppFiles", t => t.FileId)
                .ForeignKey("dbo.Rooms", t => t.RoomId, cascadeDelete: true)
                .ForeignKey("dbo.UserSpecialisations", t => t.SpecialisationId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.FileId)
                .Index(t => t.RoomId)
                .Index(t => t.UserId)
                .Index(t => t.SpecialisationId);
            
            CreateTable(
                "dbo.iOSChanels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        DateAdd = c.DateTime(nullable: false),
                        Ids_chanel = c.String(),
                        IsDelete = c.Boolean(nullable: false),
                        IsVoIP = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Logs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Level = c.Int(nullable: false),
                        MethodName = c.String(),
                        Info = c.String(),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.SendSms",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Phone = c.String(),
                        Text = c.String(),
                        MethodInit = c.String(),
                        DateAdd = c.DateTime(nullable: false),
                        State = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.StreamEvents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EventStream = c.Int(nullable: false),
                        ClientId = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128),
                        EventId = c.Int(),
                        Ip = c.String(),
                        Vhost = c.String(),
                        App = c.String(),
                        Stream = c.String(),
                        TcUrl = c.String(),
                        PageUrl = c.String(),
                        SendBytes = c.Int(),
                        RecvBytes = c.Int(),
                        Cwd = c.String(),
                        File = c.String(),
                        DateAdd = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserFriends",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        UserId = c.String(maxLength: 128),
                        UserIdFriend = c.String(maxLength: 128),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserFriend_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserIdFriend)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.UserIdFriend);
            
            CreateTable(
                "dbo.UserInvitations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        UserId = c.String(maxLength: 128),
                        UserIdFriend = c.String(maxLength: 128),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserInvitation_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserIdFriend)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.UserIdFriend);
            
            CreateTable(
                "dbo.UserConfirmationCodes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        Code = c.String(),
                        DateAdd = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserTokens",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        Token = c.String(),
                        DateAdd = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserToken_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserTokens", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserConfirmationCodes", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserInvitations", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserInvitations", "UserIdFriend", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserFriends", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserFriends", "UserIdFriend", "dbo.AspNetUsers");
            DropForeignKey("dbo.StreamEvents", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.iOSChanels", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.EWebinars", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.EWebinars", "SpecialisationId", "dbo.UserSpecialisations");
            DropForeignKey("dbo.EWebinars", "RoomId", "dbo.Rooms");
            DropForeignKey("dbo.EWebinars", "FileId", "dbo.AppFiles");
            DropForeignKey("dbo.EStreams", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.EStreams", "SpecialisationId", "dbo.UserSpecialisations");
            DropForeignKey("dbo.EStreams", "RoomId", "dbo.Rooms");
            DropForeignKey("dbo.EStreams", "FileId", "dbo.AppFiles");
            DropForeignKey("dbo.EStandUps", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.EStandUps", "SpecialisationId", "dbo.UserSpecialisations");
            DropForeignKey("dbo.EStandUps", "RoomId", "dbo.Rooms");
            DropForeignKey("dbo.EStandUps", "FileId", "dbo.AppFiles");
            DropForeignKey("dbo.Events", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Events", "SpecialisationId", "dbo.UserSpecialisations");
            DropForeignKey("dbo.Events", "RoomId", "dbo.Rooms");
            DropForeignKey("dbo.Events", "FileId", "dbo.AppFiles");
            DropForeignKey("dbo.EChallenges", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.EChallenges", "SpecialisationId", "dbo.UserSpecialisations");
            DropForeignKey("dbo.EChallenges", "RoomId", "dbo.Rooms");
            DropForeignKey("dbo.EChallenges", "FileId", "dbo.AppFiles");
            DropForeignKey("dbo.EBattles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.EBattles", "SpecialisationId", "dbo.UserSpecialisations");
            DropForeignKey("dbo.UserSpecialisations", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserSubscribtions", "SpecialisationId", "dbo.UserSpecialisations");
            DropForeignKey("dbo.UserSubscribtions", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserSubscribtions", "RoomId", "dbo.Rooms");
            DropForeignKey("dbo.UserSubscribtions", "OnSubscribeId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserSpecialisations", "SpecialisationId", "dbo.Specialisations");
            DropForeignKey("dbo.Specialisations", "CategoryId", "dbo.SpecialisationCategories");
            DropForeignKey("dbo.EBattles", "RoomId", "dbo.Rooms");
            DropForeignKey("dbo.EBattles", "FileId", "dbo.AppFiles");
            DropForeignKey("dbo.Contacts", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Contacts", "FriendId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Calls", "RoomId", "dbo.Rooms");
            DropForeignKey("dbo.Calls", "CallerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Calls", "AcceptorId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AndroidChanels", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserRooms", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserRooms", "RoomId", "dbo.Rooms");
            DropForeignKey("dbo.Messages", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Messages", "RoomId", "dbo.Rooms");
            DropForeignKey("dbo.Messages", "FileId", "dbo.AppFiles");
            DropForeignKey("dbo.AppFiles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.UserTokens", new[] { "UserId" });
            DropIndex("dbo.UserConfirmationCodes", new[] { "UserId" });
            DropIndex("dbo.UserInvitations", new[] { "UserIdFriend" });
            DropIndex("dbo.UserInvitations", new[] { "UserId" });
            DropIndex("dbo.UserFriends", new[] { "UserIdFriend" });
            DropIndex("dbo.UserFriends", new[] { "UserId" });
            DropIndex("dbo.StreamEvents", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.iOSChanels", new[] { "UserId" });
            DropIndex("dbo.EWebinars", new[] { "SpecialisationId" });
            DropIndex("dbo.EWebinars", new[] { "UserId" });
            DropIndex("dbo.EWebinars", new[] { "RoomId" });
            DropIndex("dbo.EWebinars", new[] { "FileId" });
            DropIndex("dbo.EStreams", new[] { "SpecialisationId" });
            DropIndex("dbo.EStreams", new[] { "UserId" });
            DropIndex("dbo.EStreams", new[] { "RoomId" });
            DropIndex("dbo.EStreams", new[] { "FileId" });
            DropIndex("dbo.EStandUps", new[] { "SpecialisationId" });
            DropIndex("dbo.EStandUps", new[] { "UserId" });
            DropIndex("dbo.EStandUps", new[] { "RoomId" });
            DropIndex("dbo.EStandUps", new[] { "FileId" });
            DropIndex("dbo.Events", new[] { "SpecialisationId" });
            DropIndex("dbo.Events", new[] { "UserId" });
            DropIndex("dbo.Events", new[] { "RoomId" });
            DropIndex("dbo.Events", new[] { "FileId" });
            DropIndex("dbo.EChallenges", new[] { "SpecialisationId" });
            DropIndex("dbo.EChallenges", new[] { "UserId" });
            DropIndex("dbo.EChallenges", new[] { "RoomId" });
            DropIndex("dbo.EChallenges", new[] { "FileId" });
            DropIndex("dbo.UserSubscribtions", new[] { "RoomId" });
            DropIndex("dbo.UserSubscribtions", new[] { "OnSubscribeId" });
            DropIndex("dbo.UserSubscribtions", new[] { "UserId" });
            DropIndex("dbo.UserSubscribtions", new[] { "SpecialisationId" });
            DropIndex("dbo.Specialisations", new[] { "CategoryId" });
            DropIndex("dbo.UserSpecialisations", new[] { "SpecialisationId" });
            DropIndex("dbo.UserSpecialisations", new[] { "UserId" });
            DropIndex("dbo.EBattles", new[] { "SpecialisationId" });
            DropIndex("dbo.EBattles", new[] { "UserId" });
            DropIndex("dbo.EBattles", new[] { "RoomId" });
            DropIndex("dbo.EBattles", new[] { "FileId" });
            DropIndex("dbo.Contacts", new[] { "FriendId" });
            DropIndex("dbo.Contacts", new[] { "UserId" });
            DropIndex("dbo.Calls", new[] { "RoomId" });
            DropIndex("dbo.Calls", new[] { "AcceptorId" });
            DropIndex("dbo.Calls", new[] { "CallerId" });
            DropIndex("dbo.AppFiles", new[] { "UserId" });
            DropIndex("dbo.Messages", new[] { "FileId" });
            DropIndex("dbo.Messages", new[] { "RoomId" });
            DropIndex("dbo.Messages", new[] { "UserId" });
            DropIndex("dbo.UserRooms", new[] { "RoomId" });
            DropIndex("dbo.UserRooms", new[] { "UserId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AndroidChanels", new[] { "UserId" });
            DropTable("dbo.UserTokens",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserToken_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.UserConfirmationCodes");
            DropTable("dbo.UserInvitations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserInvitation_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.UserFriends",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserFriend_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.StreamEvents");
            DropTable("dbo.SendSms");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Logs");
            DropTable("dbo.iOSChanels");
            DropTable("dbo.EWebinars",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EWebinar_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EStreams",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EStream_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EStandUps",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EStandUp_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Events",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EEvent_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EChallenges",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EChallenge_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.UserSubscribtions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserSubscribtions_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.SpecialisationCategories",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SpecialisationCategory_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Specialisations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Specialisation_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.UserSpecialisations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserSpecialisations_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.EBattles",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EBattle_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Contacts",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Contact_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Calls",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Call_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.AppFiles",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_AppFile_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Messages",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Message_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Rooms",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Room_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.UserRooms",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UserRoom_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ApplicationUser_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.AndroidChanels");
        }
    }
}
