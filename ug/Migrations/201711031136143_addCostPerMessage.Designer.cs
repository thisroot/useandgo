// <auto-generated />
namespace ug.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addCostPerMessage : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addCostPerMessage));
        
        string IMigrationMetadata.Id
        {
            get { return "201711031136143_addCostPerMessage"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
