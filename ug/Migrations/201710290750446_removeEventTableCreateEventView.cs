namespace ug.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    
    public partial class removeEventTableCreateEventView : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Events");
            Sql("CREATE VIEW Events AS WITH UnionedData " +
             "AS(SELECT 'battle' AS Event, * FROM EBattles eb " +
             "UNION ALL SELECT 'challange' AS Event, * FROM EChallenges ec " +
             "UNION ALL SELECT 'standup' AS Event, * FROM EStandUps eu " +
             "UNION ALL SELECT 'webinar' AS Event, * FROM EWebinars ew " +
             "UNION ALL SELECT 'streams' AS Event, * FROM EStreams es)" +
             "SELECT ROW_NUMBER() OVER(ORDER BY DateAdd) " +
             "AS IdEvent, * FROM UnionedData");
        }
        
        public override void Down()
        {
            Sql("DROP VIEW Events");
            CreateTable(
                "dbo.Events",
                c => new
                {
                    IdEvent = c.Long(nullable: false, identity: true),
                    Event = c.String(),
                    Id = c.Int(nullable: false),
                    Name = c.String(),
                    Description = c.String(),
                    FileId = c.Int(),
                    RoomId = c.Int(nullable: false),
                    EventStatus = c.Int(nullable: false),
                    UserId = c.String(nullable: false, maxLength: 128),
                    SpecialisationId = c.Int(),
                    CostPerEvent = c.Decimal(nullable: false, precision: 18, scale: 2),
                    CostPerEventVIP = c.Decimal(nullable: false, precision: 18, scale: 2),
                    CostPerMinute = c.Decimal(nullable: false, precision: 18, scale: 2),
                    LimitTickets = c.Int(),
                    CostPerMinuteVIP = c.Decimal(nullable: false, precision: 18, scale: 2),
                    LimitVIPTickets = c.Int(),
                    OnlyVIP = c.Boolean(nullable: false),
                    DateStart = c.DateTime(nullable: false),
                    DateEnd = c.DateTime(),
                    IsDeleted = c.Boolean(nullable: false),
                    DateAdd = c.DateTime(nullable: false),
                    DateUpdate = c.DateTime(nullable: false),
                },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EEvent_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.IdEvent)
                .ForeignKey("dbo.AppFiles", t => t.FileId)
                .ForeignKey("dbo.Rooms", t => t.RoomId, cascadeDelete: true)
                .ForeignKey("dbo.UserSpecialisations", t => t.SpecialisationId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.FileId)
                .Index(t => t.RoomId)
                .Index(t => t.UserId)
                .Index(t => t.SpecialisationId);
        }
    }
}
