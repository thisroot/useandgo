namespace ug.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addStreamEventClientModel : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.StreamEvents", newName: "StreamEventClients");
            CreateTable(
                "dbo.StreamEventStreamers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EventStream = c.Int(nullable: false),
                        ClientId = c.Int(nullable: false),
                        UserId = c.String(maxLength: 128),
                        EventId = c.Int(),
                        Ip = c.String(),
                        Vhost = c.String(),
                        App = c.String(),
                        Stream = c.String(),
                        TcUrl = c.String(),
                        PageUrl = c.String(),
                        SendBytes = c.Int(),
                        RecvBytes = c.Int(),
                        Cwd = c.String(),
                        File = c.String(),
                        DateAdd = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.StreamEventStreamers", new[] { "UserId" });
            DropTable("dbo.StreamEventStreamers");
            RenameTable(name: "dbo.StreamEventClients", newName: "StreamEvents");
        }
    }
}
