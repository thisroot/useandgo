namespace ug.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class expandUserSpecialisationModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserSpecialisations", "CostPerMinuteCall", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.UserSpecialisations", "CostPerMessage", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.UserSpecialisations", "Experience", c => c.DateTime(nullable: false));
            DropColumn("dbo.UserSpecialisations", "CostPerMinute");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserSpecialisations", "CostPerMinute", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.UserSpecialisations", "Experience");
            DropColumn("dbo.UserSpecialisations", "CostPerMessage");
            DropColumn("dbo.UserSpecialisations", "CostPerMinuteCall");
        }
    }
}
