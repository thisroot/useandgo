namespace ug.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCostPerMessage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EBattles", "CostPerMessage", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.EChallenges", "CostPerMessage", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.EStandUps", "CostPerMessage", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.EStreams", "CostPerMessage", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.EWebinars", "CostPerMessage", c => c.Decimal(nullable: false, precision: 18, scale: 2));

            Sql("DROP VIEW Events");
            Sql("CREATE VIEW Events AS WITH UnionedData " +
             "AS(SELECT 'battle' AS Event, * FROM EBattles eb " +
             "UNION ALL SELECT 'challange' AS Event, * FROM EChallenges ec " +
             "UNION ALL SELECT 'standup' AS Event, * FROM EStandUps eu " +
             "UNION ALL SELECT 'webinar' AS Event, * FROM EWebinars ew " +
             "UNION ALL SELECT 'streams' AS Event, * FROM EStreams es)" +
             "SELECT ROW_NUMBER() OVER(ORDER BY DateAdd) " +
             "AS IdEvent, * FROM UnionedData");
        }
        
        public override void Down()
        {
            DropColumn("dbo.EWebinars", "CostPerMessage");
            DropColumn("dbo.EStreams", "CostPerMessage");
            DropColumn("dbo.EStandUps", "CostPerMessage");
            DropColumn("dbo.EChallenges", "CostPerMessage");
            DropColumn("dbo.EBattles", "CostPerMessage");

            Sql("DROP VIEW Events");
            Sql("CREATE VIEW Events AS WITH UnionedData " +
             "AS(SELECT 'battle' AS Event, * FROM EBattles eb " +
             "UNION ALL SELECT 'challange' AS Event, * FROM EChallenges ec " +
             "UNION ALL SELECT 'standup' AS Event, * FROM EStandUps eu " +
             "UNION ALL SELECT 'webinar' AS Event, * FROM EWebinars ew " +
             "UNION ALL SELECT 'streams' AS Event, * FROM EStreams es)" +
             "SELECT ROW_NUMBER() OVER(ORDER BY DateAdd) " +
             "AS IdEvent, * FROM UnionedData");
        }
    }
}
