namespace ug.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRoomName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Rooms", "RoomName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Rooms", "RoomName");
        }
    }
}
