﻿using Microsoft.Data.Edm.Library;
using Microsoft.Data.Edm.Library.Values;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;
using ug.Models;
using ug.Models.Events;
using ug.Models.Money;
using ug.Models.Simple;
using ug.Models.Specialisations;

namespace ug
{
    public class ODataConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes(); //This has to be called before the following OData mapping, so also before WebApi mapping
            config.AddODataQueryFilter();
            //config.SetTimeZoneInfo(TimeZoneInfo.Utc);

            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            //var builder = new ODataModelBuilder();
            builder.Namespace = "Func";

            config.Count().Filter().OrderBy().Expand().Select().MaxTop(100); //new line
            

            //exclude model properties
            builder.EntitySet<ApplicationUser>("Users").EntityType.Ignore(ui => ui.PasswordHash);
            builder.EntitySet<ApplicationUser>("Users").EntityType.Ignore(ui => ui.Patronymic);
            builder.EntitySet<ApplicationUser>("Users").EntityType.Ignore(ui => ui.EmailConfirmed);
            builder.EntitySet<ApplicationUser>("Users").EntityType.Ignore(ui => ui.SecurityStamp);
            builder.EntitySet<ApplicationUser>("Users").EntityType.Ignore(ui => ui.DateAdd);
            builder.EntitySet<ApplicationUser>("Users").EntityType.Ignore(ui => ui.DateUpdate);
            builder.EntitySet<ApplicationUser>("Users").EntityType.Ignore(ui => ui.PhoneNumberConfirmed);
            builder.EntitySet<ApplicationUser>("Users").EntityType.Ignore(ui => ui.TwoFactorEnabled);
            builder.EntitySet<ApplicationUser>("Users").EntityType.Ignore(ui => ui.LockoutEndDateUtc);
            builder.EntitySet<ApplicationUser>("Users").EntityType.Ignore(ui => ui.LockoutEnabled);
            builder.EntitySet<ApplicationUser>("Users").EntityType.Ignore(ui => ui.AccessFailedCount);
            builder.EntitySet<ApplicationUser>("Users").EntityType.Ignore(ui => ui.Logins);
            builder.EntitySet<ApplicationUser>("Users").EntityType.Ignore(ui => ui.PhoneNumber);
            builder.EntitySet<ApplicationUser>("Users").EntityType.Ignore(ui => ui.UserName);
            builder.EntitySet<ApplicationUser>("Users").EntityType.Ignore(ui => ui.Email);
            builder.EntitySet<ApplicationUser>("Users").EntityType.Ignore(ui => ui.IsDeleted);

            builder.EntityType<ApplicationUser>().Page(100, 100);
            var ba = builder.StructuralTypes.First(t => t.ClrType == typeof(ApplicationUser));
            ba.AddProperty(typeof(ApplicationUser).GetProperty("Balance"));

            builder.EntitySet<UserFriend>("UserFriends");
            builder.EntityType<UserFriend>().Page(100, 100);
            // getAllFrinedsWithRooms
            builder.EntityType<UserFriend>().Collection.Action("GetFriends");


            builder.EntitySet<UserInvitation>("UserInvitations");
            builder.EntityType<UserInvitation>().Page(100, 100);
            builder.EntitySet<UserRoom>("UserRooms");
            builder.EntityType<UserRoom>().Page(100, 100);

            var cu = builder.StructuralTypes.First(t => t.ClrType == typeof(UserRoom));
            cu.AddProperty(typeof(UserRoom).GetProperty("UnreadMessages"));


            builder.EntitySet<Room>("Room");
            builder.EntityType<Room>().Page(100, 100);
            var du = builder.StructuralTypes.First(t => t.ClrType == typeof(Room));
            du.AddProperty(typeof(Room).GetProperty("CostPerMessage"));

            builder.EntitySet<Message>("Message");
            builder.EntityType<Message>().Page(100, 100);

            // start контакты
            builder.EntitySet<Contact>("Contacts");//.EntityType.Ignore(ui => ui.id);
            builder.EntityType<Contact>().Page(100, 100);
            //Функционал для экшенов
            //var setContacts = builder.EntityType<Contact>().Collection.Action("Set");
            //setContacts.CollectionParameter<ContactRequest>("Contacts");
            //setContacts.Parameter<string>("Mode");
            builder.EntityType<Contact>().Collection.Action("Friends");
            // end контакты

            //UserSpecialisations
            builder.EntitySet<Specialisation>("Specialisations");
            builder.EntitySet<Specialisation>("Specialisations").EntityType.Ignore(ui => ui.DateAdd);
            builder.EntitySet<Specialisation>("Specialisations").EntityType.Ignore(ui => ui.DateUpdate);
            builder.EntityType<Specialisation>().Page(100, 100);

            builder.EntitySet<Specialisation>("Specialisations").EntityType.Ignore(ui => ui.IsDeleted);
            builder.EntitySet<UserSpecialisations>("UserSpecialisations");
            builder.EntitySet<UserSpecialisations>("UserSpecialisations").EntityType.Ignore(ui => ui.IsDeleted);
            builder.EntityType<UserSpecialisations>().Page(100, 100);
            builder.EntitySet<SpecialisationCategory>("SpecialisationCategory");
            builder.EntitySet<SpecialisationCategory>("SpecialisationCategory").EntityType.Ignore(ui => ui.DateAdd);
            builder.EntitySet<SpecialisationCategory>("SpecialisationCategory").EntityType.Ignore(ui => ui.DateUpdate);
            builder.EntitySet<SpecialisationCategory>("SpecialisationCategory").EntityType.Ignore(ui => ui.IsDeleted);
            builder.EntityType<SpecialisationCategory>().Page(100, 100);


            //AppFile
            builder.EntitySet<AppFile>("AppFile");
            builder.EntityType<AppFile>().Page(100, 100);
            // set file property
            var setFiles = builder.EntityType<Models.AppFile>().Collection.Action("Set");
            setFiles.Parameter<HttpPostedFileBase>("File");

            //Subscribtions
            builder.EntitySet<UserSubscribtions>("UserSubscribtions");
            builder.EntitySet<UserSubscribtions>("UserSubscribtions").EntityType.Ignore(ui => ui.IsDeleted);
            builder.EntityType<UserSubscribtions>().Page(100, 100);

            builder.EntitySet<iOSChanel>("iOSChanels");
            builder.EntitySet<iOSChanel>("iOSChanel").EntityType.Ignore(ui => ui.IsDelete);
            builder.EntitySet<AndroidChanel>("AndroidChanels");
            builder.EntitySet<AndroidChanel>("AndroidChanel").EntityType.Ignore(ui => ui.IsDelete);

            //EdmEnumType statusEvent = new EdmEnumType("EventStatus", "Type");
            //statusEvent.AddMember(new EdmEnumMember(statusEvent, "NotStarted", new EdmIntegerConstant(0)));
            //statusEvent.AddMember(new EdmEnumMember(statusEvent, "Started", new EdmIntegerConstant(1)));
            //statusEvent.AddMember(new EdmEnumMember(statusEvent, "Finished", new EdmIntegerConstant(2)));
            //statusEvent.AddMember(new EdmEnumMember(statusEvent, "Blocked", new EdmIntegerConstant(3)));
            //statusEvent.AddMember(new EdmEnumMember(statusEvent, "Pending", new EdmIntegerConstant(4)));

            //var eventType = builder.AddEnumType(typeof(StatusEvent));
            //eventType.AddMember(StatusEvent.NotStarted);
            //eventType.AddMember(StatusEvent.Started);
            //eventType.AddMember(StatusEvent.Pending);
            //eventType.AddMember(StatusEvent.Finished);
            //eventType.AddMember(StatusEvent.Blocked);

            //Events models
            builder.EntitySet<EWebinar>("EWebinars");
            builder.EntityType<EWebinar>().Page(100, 100);
            builder.EntitySet<EBattle>("EBattles");
            builder.EntityType<EBattle>().Page(100, 100);
            builder.EntitySet<EChallenge>("EChallenges");
            builder.EntityType<EChallenge>().Page(100, 100);
            builder.EntitySet<EStandUp>("EStandUps");
            builder.EntityType<EStandUp>().Page(100, 100);
            builder.EntitySet<EStream>("EStreams");
            builder.EntityType<EStream>().Page(100, 100);
            var eevent = builder.EntitySet<EEvent>("EEvents");
            //eevent.EntityType.EnumProperty()
            builder.EntityType<EEvent>().Page(100, 100);

            //Money
            builder.EntitySet<UserTransaction>("UserTransactions");
            builder.EntityType<UserTransaction>().Page(100, 100);


            config.MapODataServiceRoute("ODataRoute", "odata", model: builder.GetEdmModel());
            config.EnsureInitialized();
        }
    }
}