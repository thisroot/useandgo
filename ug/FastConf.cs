﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using ug.Models;

namespace ug
{
    /// <summary>
    /// Набор параметров для конфигурации
    /// </summary>
    public class FastConf
    {
        /// <summary>
        /// Ключ уведомлений для андройда
        /// </summary>
        public const string server_key_android = "AAAAhltirFQ:APA91bFrdnjfLetcjWa_XIaNManb3gJeScmpynnucT52NOTovWTpVxtLxZnNn7KOn6ojBCM-2iO-RRNDZF6XbnjoaQSMeOlOxbdCzNWpdR7ZPJoXbLpuFF5P9h3EQjmd30NN62GFZ5V4";
        /// <summary>
        /// Регулярка для номера телефона
        /// </summary>
        public const string PhoneRegex = @"^(79+([0-9]){9})$";
        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
    }
}