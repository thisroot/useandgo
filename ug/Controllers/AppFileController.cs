﻿using MoreLinq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Routing;
using ug.Models;
using ug.Models.Simple;

namespace ug.Controllers
{
    public class AppFileController : BaseODataController
    {
        [Authorize]
        [EnableQuery(MaxExpansionDepth = 4)]
        public IQueryable<Models.AppFile> Get()
        {
            return db.Files.Where(u => u.UserId == AppUser.Id && u.IsDeleted == false).AsQueryable();
        }

        [AllowAnonymous]

        public IHttpActionResult Get(int key)
        {
            var file = db.Files.Where(u => u.Id == key && u.IsDeleted == false).FirstOrDefault();
            if (file.IsPrivate != true)
            {
                return Ok(file);
            }

            var user = AppUser.Id;
            if (user != null && user == file.UserId)
            {
                return Ok(file);
            }

            return BadRequest("Permission denied");
        }

        public static readonly string[] VALID_IMAGES = new string[4] { ".png", ".jpg", ".gif", ".jpeg" };


        [Authorize]
        public IHttpActionResult Post()
        {
            string root = HttpContext.Current.Server.MapPath("~/Uploads/");
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                List<AppFile> files = new List<AppFile>();
                foreach (string file in httpRequest.Files)
                {
                    string extension = System.IO.Path.GetExtension(httpRequest.Files[file].FileName);
                    string mimeType = MimeMapping.GetMimeMapping(System.IO.Path.GetExtension(httpRequest.Files[file].FileName));

                    //обработчики 
                    if (VALID_IMAGES.Contains(extension.ToLower()))
                    {

                    }
                    else
                    {
                        return BadRequest("VALID_EXTENSIONS");
                    }

                    var postedFile = httpRequest.Files[file];
                    var NewNameFile = Guid.NewGuid() + extension;
                    var filePath = root + NewNameFile;
                    postedFile.SaveAs(filePath);
                    AppFile f = new AppFile()
                    {
                        ContentType = mimeType,
                        DateAdd = DateTime.Now,
                        Extension = extension,
                        FileName = NewNameFile,
                        FileSize = postedFile.ContentLength,
                        User = AppUser,
                        TypeUploadFile = AppFile.TypeFile.Image
                    };
                    db.Files.Add(f);
                    db.SaveChanges();
                    files.Add(f);
                }
                return Ok(files);
            }
            else
            {
                return BadRequest();
            }
        }

    }

}