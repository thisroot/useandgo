﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using ug.Models;

namespace ug.Controllers
{



    /// <summary>
    /// Список приглашений
    /// </summary>
    [Authorize]
    public class UserInvitationsController : BaseODataController
    {
        // GET: Users
        /// <summary>
        /// Получить всех приглашений в друзья
        /// </summary>
        /// <returns></returns>
        [EnableQuery]
        public IQueryable<UserInvitation> Get()
        {
            return db.UserInvitations.Where(x=>x.UserIdFriend == AppUser.Id && x.IsDeleted == false).AsQueryable();
        }

    }
}