﻿using MoreLinq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Routing;
using ug.Models;
using ug.Models.Simple;

namespace ug.Controllers
{
    /// <summary>
    /// Список пользователей вернуть
    /// </summary>
    /// 
    [Authorize]
    public class UserFriendsController : BaseODataController
    {
        // GET: odata/UserFriends
        /// <summary>
        /// Получить всех друзей по пользователю
        /// </summary>
        /// <returns></returns>
        [EnableQuery]
        public IQueryable<UserFriend> Get()
        {
            return db.UserFriends.Where(x=>x.UserId == AppUser.Id && x.IsDeleted == false).AsQueryable();
        }

        /// <summary>
        /// Получить друзей пользователя с комнатами
        /// POST odata/UserFriends/Func.GetFriends
        /// </summary>
        /// <returns></returns>

        [HttpPost]
        public IHttpActionResult GetFriends()
        {
           
            // получаем список друзей пользователя с комнатами
            var query = (from uf in db.UserFriends
                         join u1 in db.Users on uf.UserId equals u1.Id
                         join u2 in db.Users on uf.UserIdFriend equals u2.Id
                         join ur in db.UsersRooms on u2.Id equals ur.UserId
                         where u1.Id == AppUser.Id 
                         && uf.IsDeleted == false 
                         && ur.IsDeleted == false 
                         && u1.IsDeleted == false 
                         && u2.IsDeleted == false 
                         && (from ur1 in db.UsersRooms where ur1.UserId == AppUser.Id select ur1.RoomId).Contains(ur.RoomId)
                         select new  { u2, ur.RoomId }

                         ).ToList();

            var friends = new List<FriendRequest>();
            query.ForEach(e =>
            {
                friends.Add(new FriendRequest(e.u2, e.RoomId));
            });

            return Json(new { Friends = friends });
        }


        public IHttpActionResult Delete(int key)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //найти друга среди друзей пользователя
            var uf = db.UserFriends.Where(u => u.Id == key).FirstOrDefault();

            if (uf == null)
            {
                return BadRequest();
            }

            uf.IsDeleted = true;
            var uc = db.Contacts.Where(x => x.Phone == uf.FriendUser.PhoneNumber && x.UserId == uf.UserId).FirstOrDefault();
            if(uc != null)
            {
                uc.IsDeleted = true;
            }

            db.SaveChanges();
            return Ok();
        }

       
    }
}