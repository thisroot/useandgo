﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using ug.Models;
using ug.Models.Simple;

namespace ug.Controllers
{
    public class UserSubscribtionsController : BaseODataController
    {
        [EnableQuery]
        [Authorize]
        public IQueryable<UserSubscribtions> Get()
        {

            var UserId = AppUser.Id;
            return db.UsersSubscribtions.Where(a => a.IsDeleted == false)
                                               .AsQueryable();
        }

        [Authorize]
        public IHttpActionResult Post(UserSubscribtions subscribtion)
        {
            var UserId = AppUser.Id;

            if (ModelState.IsValid)
            {

                //нельзя подписываться на уже подписанные специализации
                //специализация должна быть в стандартном списке специализаций
                if (db.UsersSpecialisations.Where(u => u.Id == subscribtion.SpecialisationId && u.IsDeleted == false).Any() &&
                    db.UsersSubscribtions.Where(z=>z.SpecialisationId == subscribtion.SpecialisationId && z.UserId == UserId && z.IsDeleted == false).Any() != true)
                {
                    var specialisation = db.UsersSpecialisations.Where(u => u.Id == subscribtion.SpecialisationId && u.IsDeleted == false).FirstOrDefault();

                    if(UserId == specialisation.UserId)
                    {
                        return BadRequest("you cant subscribe to youserlf");
                    }

                    var room = db.Rooms.Add(new Room { TypeRoom = Room.RoomType.Specialisation, RoomName = specialisation.Profession });
                    db.UsersRooms.AddRange( new List<UserRoom> {
                        new UserRoom { Room = room, UserId = UserId, Role = UserRoom.MemberRole.Free },
                        new UserRoom { Room = room, UserId = specialisation.UserId, Role = UserRoom.MemberRole.Admin }
                    });

                    var subscr = new UserSubscribtions
                    {
                        UserId = UserId,
                        OnSubscribeId = specialisation.UserId,
                        IsDeleted = false,
                        SpecialisationId = specialisation.Id,
                    };

                    db.UsersSubscribtions.Add(subscr);
                    db.SaveChanges();
                    return Ok(subscr);
                }
            }

            return BadRequest("Model is invalid");
        }

        [Authorize]
        public IHttpActionResult Delete(int key)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //только сам пользователь может добавлять себе специализации
            var us = db.UsersSubscribtions.Where(u => u.Id == key
                                                    && u.UserId == AppUser.Id
                                                    && u.IsDeleted == false)
                                                    .FirstOrDefault();
            if (us == null)
            {
                return BadRequest();
            }

            us.IsDeleted = true;
            us.Room.IsDeleted = true;
            db.SaveChanges();
            return Ok();
        }
    }
}