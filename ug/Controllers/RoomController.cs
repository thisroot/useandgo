﻿using MoreLinq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Routing;
using ug.Models;
using ug.Models.Simple;

namespace ug.Controllers
{
    public class RoomController : BaseODataController
    {
        [Authorize]
        [EnableQuery]
        public IQueryable<Room> Get()
        {
            var rooms = db.Rooms.Where(u => u.UserRooms.Select(z => z.UserId).Contains(AppUser.Id) && u.IsDeleted == false).ToList();

            //rooms.ForEach(x =>
            //{
            //    x.UserRooms = x.UserRooms.Where(z => z.UserId != AppUser.Id).ToList();
            //});

            return rooms.AsQueryable();
        }

        [Authorize]
        [EnableQuery]
        public IQueryable<Room> Get(int key)
        {
            var rooms =  db.Rooms.Where(u => u.UserRooms.Select(z => z.UserId).Contains(AppUser.Id) && u.Id == key && u.IsDeleted == false).ToList();
            //rooms.ForEach(x =>
            //{
            //    x.UserRooms = x.UserRooms.Where(z => z.UserId != AppUser.Id).ToList();
            //});
            return rooms.AsQueryable();
        }

        public IHttpActionResult Patch(int key, Room room)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(db.Rooms.Where(d=> d.Id == key && d.UserRooms.Select(w =>w.UserId).Contains(AppUser.Id)).Any()) {

                var uroom = db.Rooms.FirstOrDefault(d => d.Id == key && d.UserRooms.Select(w => w.UserId).Contains(AppUser.Id));
                uroom.RoomName = room.RoomName;
                uroom.DateUpdate = DateTime.Now;
                db.SaveChanges();
                return Ok(uroom);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }
    }
}