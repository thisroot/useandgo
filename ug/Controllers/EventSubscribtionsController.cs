﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using ug.Models;
using ug.Models.Simple;
using System.Net;
using AutoMapper;
using MoreLinq;
using Newtonsoft.Json;
using ug.Models.Events;

namespace ug.Controllers
{
    public class EventSubscribtionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Authorize]
        [HttpGet]
        // GET: EventSubscribtions
        public ActionResult Index()
        {
            var uid = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var urooms = db.UsersRooms.Where(x => x.UserId == uid.Id && !x.Room.IsDeleted &&
                                                  x.Room.IsEvent && !x.IsDeleted);
            var events = db.EEvents.Where(y => urooms.Select(a => a.RoomId).ToList().Contains(y.RoomId) && !y.IsDeleted).ToList();

            //.Select(s => new
            // {
            //    Id = s.Id,
            //    IdEvent = s.IdEvent,
            //    UserId = s.UserId,
            //    EventType = s.Event,
            //    Name = s.Name,
            //    Description = s.Description,
            //    FileId = s.FileId,
            //    RoomId = s.RoomId,
            //    CostPerEvent = s.CostPerEvent,
            //    OnlyVIP = s.OnlyVIP,
            //    LimitVIPTickets = s.LimitVIPTickets,
            //    CostPerMinute = s.CostPerMinute,
            //    LimitTickets = s.LimitTickets,
            //    CostPerMinuteVIP = s.CostPerMinuteVIP,
            //    UserRooms = s.Room.UserRooms.Where(ws => !ws.IsDeleted)
            //        .Select(a => new {
            //            UserId = a.UserId,
            //            Role = a.Role,
            //            RoomId = a.RoomId,
            //            MPermission = a.MPermission,
            //            FileTransfer = a.FileTransfer,
            //            ChatWriter = a.ChatWriter,
            //            Id = a.Id }),
            //    SpecialisationId = s.SpecialisationId,
            //    DateStart = s.DateStart,
            //    DateEnd = s.DateEnd
            //}).ToList();

            var respond = JsonConvert.SerializeObject(events);
            return Content(respond, "application/json");
        }

        [Authorize]
        [HttpPost]
        public ActionResult Index(EventSubscribeRequest esr)
        {
            var uid = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
           
            //выполнить проверки на возможность подписки пользователя на события
            var ev = db.EEvents.Where(xw => xw.Id == esr.EventId && xw.UserId != uid.Id).FirstOrDefault();
            if(ev == null) return Json(new { Status = "error", Message = "Event can't found" }, JsonRequestBehavior.AllowGet);

            //проверка на наличие пользователя в комнате
            if (ev.Room.UserRooms.Where(s => uid.Id.Contains(s.UserId) && !s.IsDeleted).Any())
            {
                return Json(new { Status = "error", Message = "Member allready registered" }, JsonRequestBehavior.AllowGet);
            }
            //проверка на доступ для не Вип пользователя
            if (ev.OnlyVIP && !esr.IsVIP) return Json(new { Status = "error", Message = "Only Vips can connect to event " }, JsonRequestBehavior.AllowGet);
            //проверка на лимит Vip пользователей
            if (ev.LimitVIPTickets != null && ev.Room.UserRooms
                      .Where(x => x.Role == UserRoom.MemberRole.Vip && !x.IsBlocked && !x.IsDeleted).Count() >= ev.LimitVIPTickets)
            {
                return Json(new { Status = "error", Message = "Exceeded number of Vips" }, JsonRequestBehavior.AllowGet);
            }
            //проверка на лимит пользователей
            if (ev.LimitTickets != null && ev.Room.UserRooms
                      .Where(x => x.Role == UserRoom.MemberRole.Vip && !x.IsBlocked && !x.IsDeleted).Count() >= ev.LimitTickets)
            {
                return Json(new { Status = "error", Message = "Exceeded number of Vips" }, JsonRequestBehavior.AllowGet);
            }

            //проверка на наличие средств у пользователя
            if (esr.IsVIP)
            {
                if (ev.CostPerEventVIP != 0)
                {
                    if (ev.CostPerEventVIP >= uid.Balance)
                    {
                        return Json(new { Status = "error", Message = "Not enougth money" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        uid.MoneyTransaction(-ev.CostPerEventVIP);
                    }
                }
            }
            else
            {
                if (ev.CostPerEvent != 0)
                {
                    if (ev.CostPerEvent >= uid.Balance)
                    {
                        return Json(new { Status = "error", Message = "Not enougth money" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        uid.MoneyTransaction(-ev.CostPerEvent);
                    }
                }
            }

            var eevent = ev.GetObject(db);
            var ur = ((IEEvents)eevent).Subscribe(uid,esr);
            db.UsersRooms.Add(ur);
            db.SaveChanges();
            return Content(JsonConvert.SerializeObject(ur), "application/json");

        }

        [Authorize]
        [HttpDelete]
        public ActionResult Delete(EventSubscribeRequest esr)
        {
            var uid = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();

            var ev = db.EEvents.Where(xw => xw.Id == esr.EventId && !xw.IsDeleted && xw.UserId != uid.Id)
                                   .Select(a => a.Room.UserRooms.Where(e => e.UserId == uid.Id && !e.IsDeleted).FirstOrDefault()).FirstOrDefault();

            ev.IsDeleted = true;
            ev.Room.IsDeleted = true;
            db.SaveChanges();
            return Content(JsonConvert.SerializeObject(ev), "application/json");
        }
    }
}