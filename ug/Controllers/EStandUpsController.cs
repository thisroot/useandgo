﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using ug.Models;
using ug.Models.Events;

namespace ug.Controllers
{
    [Authorize]
    public class EStandUpsController : BaseODataController
    {
        [EnableQuery]
        public IQueryable<EStandUp> Get()
        {
            var UserId = AppUser.Id;
            return db.EStandUps.Where(x => x.IsDeleted == false).AsQueryable();
        }

        public async Task<IHttpActionResult> Post(EStandUp standup)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //только сам пользователь может добавлять себе специализации

            if (standup.DateEnd == null)
            {
                standup.DateEnd = standup.DateStart.AddHours(1).AddMinutes(30);
            };
            standup.UserId = AppUser.Id;
            var room = db.Rooms.Add(new Room() { IsEvent = true });
            standup.Room = room;
            db.EStandUps.Add(standup);
            await db.SaveChangesAsync();
            return Created(standup);
        }

        public IHttpActionResult Patch(int key, EStandUp standup)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var userId = AppUser.Id;
            if (db.EStandUps.Where(z => z.UserId == AppUser.Id && z.IsDeleted == false && z.Id == key).Any())
            {
                if (standup.DateEnd == null)
                {
                    standup.DateEnd = standup.DateStart.AddHours(1).AddMinutes(30);
                };
                // запрос к базе для извлечения референсной модели
                var eevent = db.EStandUps.Where(z => z.UserId == AppUser.Id && z.IsDeleted == false && z.Id == key).FirstOrDefault();
                eevent.CostPerMinute = standup.CostPerMinute;
                eevent.DateUpdate = DateTime.Now;
                eevent.Description = standup.Description;
                eevent.FileId = standup.FileId;
                eevent.LimitVIPTickets = standup.LimitVIPTickets;
                eevent.LimitTickets = standup.LimitTickets;
                eevent.CostPerMinuteVIP = standup.CostPerMinuteVIP;
                eevent.CostPerMessage = standup.CostPerMessage;
                eevent.Name = standup.Name;
                eevent.OnlyVIP = standup.OnlyVIP;
                eevent.CostPerEvent = standup.CostPerEvent;
                eevent.CostPerEventVIP = standup.CostPerEventVIP;
                eevent.DateStart = standup.DateStart;
                eevent.DateEnd = standup.DateEnd > standup.DateStart ?
                                 standup.DateEnd :
                                 standup.DateStart.AddHours(1).AddMinutes(30);

                db.SaveChanges();
                return Ok(eevent);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        public IHttpActionResult Delete(int key)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //только сам пользователь может добавлять себе специализации
            var us = db.EStandUps.Where(u => u.Id == key
                                                    && u.UserId == AppUser.Id
                                                    && u.IsDeleted == false)
                                                    .FirstOrDefault();
            if (us == null)
            {
                return BadRequest();
            }

            us.IsDeleted = true;
            us.Room.IsDeleted = true;
            db.SaveChanges();
            return Ok();
        }
    }
}