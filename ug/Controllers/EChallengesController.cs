﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using ug.Models;
using ug.Models.Events;
namespace ug.Controllers
{
    [Authorize]
    public class EChallengesController : BaseODataController
    {
        [EnableQuery]
        public IQueryable<EChallenge> Get()
        {
            var UserId = AppUser.Id;
            return db.EChallenges.Where(x => x.IsDeleted == false).AsQueryable();
        }

        public async Task<IHttpActionResult> Post(EChallenge challenge)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (challenge.DateEnd == null)
            {
                challenge.DateEnd = challenge.DateStart.AddHours(1).AddMinutes(30);
            };

            //только сам пользователь может добавлять себе специализации
            challenge.UserId = AppUser.Id;
            var room = db.Rooms.Add(new Room() { IsEvent = true });
            challenge.Room = room;
            db.EChallenges.Add(challenge);
            await db.SaveChangesAsync();
            return Created(challenge);
        }

        public IHttpActionResult Patch(int key, EChallenge challenge)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var userId = AppUser.Id;
            if (db.EChallenges.Where(z => z.UserId == AppUser.Id && z.IsDeleted == false && z.Id == key).Any())
            {
                if (challenge.DateEnd == null)
                {
                    challenge.DateEnd = challenge.DateStart.AddHours(1).AddMinutes(30);
                };
                // запрос к базе для извлечения референсной модели
                var eevent = db.EChallenges.Where(z => z.UserId == AppUser.Id && z.IsDeleted == false && z.Id == key).FirstOrDefault();
                eevent.CostPerMinute = challenge.CostPerMinute;
                eevent.DateUpdate = DateTime.Now;
                eevent.Description = challenge.Description;
                eevent.FileId = challenge.FileId;
                eevent.LimitVIPTickets = challenge.LimitVIPTickets;
                eevent.LimitTickets = challenge.LimitTickets;
                eevent.CostPerMinuteVIP = challenge.CostPerMinuteVIP;
                eevent.CostPerMessage = challenge.CostPerMessage;
                eevent.Name = challenge.Name;
                eevent.OnlyVIP = challenge.OnlyVIP;
                eevent.CostPerEvent = challenge.CostPerEvent;
                eevent.CostPerEventVIP = challenge.CostPerEventVIP;
                eevent.DateStart = challenge.DateStart;
                eevent.DateEnd = challenge.DateEnd > challenge.DateStart ?
                                 challenge.DateEnd :
                                 challenge.DateStart.AddHours(1).AddMinutes(30);

                db.SaveChanges();

                return Ok(eevent);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        public IHttpActionResult Delete(int key)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //только сам пользователь может добавлять себе специализации
            var us = db.EChallenges.Where(u => u.Id == key
                                                    && u.UserId == AppUser.Id
                                                    && u.IsDeleted == false)
                                                    .FirstOrDefault();
            if (us == null)
            {
                return BadRequest();
            }

            us.IsDeleted = true;
            us.Room.IsDeleted = true;
            db.SaveChanges();
            return Ok();
        }
    }
}