﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ug.Models;
using ug.Models.Simple;

namespace ug.Controllers
{
    [Obsolete("Удалить потом он будет работать с ангуляром 4тым что бы все радовать")]
    public class HomeController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();

        private const string TEST_TOKEN = "cV5oIVDzHdw:APA91bEaKKd0_l17LRb_gK5D0Nzm4XhIOOMA0Bna_PdTNYcVFGLxhachZuQ_aHge28YCl3UBDNDeOfe6SeuE6dLyLhpzXsSntp0KoJCIv0yyYNfcYLgWg_rwmUBFBZZyhexBi2YzUrad";

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        #region Тестовые методы
        //home/TestAndroid
        public ActionResult TestAndroid()
        {
            AndroidChanel chan = new AndroidChanel()
            {
                DateAdd = DateTime.Now,
                Ids_chanel = TEST_TOKEN,
                IsDelete = false,
                UserId = db.Users.FirstOrDefault().Id,
                User = db.Users.FirstOrDefault()
            };
            return Json(new { state = chan.Send("Привет мир! Ваш заказ будет готов через 15ть минут", 21) });
        }

        public ActionResult TestAndroidCall()
        {
            AndroidChanel chan = new AndroidChanel()
            {
                DateAdd = DateTime.Now,
                Ids_chanel = TEST_TOKEN,
                IsDelete = false,
                UserId = db.Users.FirstOrDefault().Id,
                User = db.Users.FirstOrDefault()
            };
            return Json(new
            {
                state = chan.Send(new PushMessage()
                {
                    PushEvent = PushMessage.TypePushEvent.Call,
                    UserId = "dfd",
                    RoomId = 1,
                    UserName = "test name"
                })
            });
        }


        public ActionResult TestAndroidAcceptCall()
        {
            AndroidChanel chan = new AndroidChanel()
            {
                DateAdd = DateTime.Now,
                Ids_chanel = TEST_TOKEN,
                IsDelete = false,
                UserId = db.Users.FirstOrDefault().Id,
                User = db.Users.FirstOrDefault()
            };
            return Json(new
            {
                state = chan.Send(new PushMessage()
                {
                    PushEvent = PushMessage.TypePushEvent.AcceptCall,
                    UserId = "dfd",
                    RoomId = 1,
                    UserName = "test name"
                })
            });
        }

        public ActionResult TestAndroidRejectCall()
        {
            AndroidChanel chan = new AndroidChanel()
            {
                DateAdd = DateTime.Now,
                Ids_chanel = TEST_TOKEN,
                IsDelete = false,
                UserId = db.Users.FirstOrDefault().Id,
                User = db.Users.FirstOrDefault()
            };
            return Json(new
            {
                state = chan.Send(new PushMessage()
                {
                    PushEvent = PushMessage.TypePushEvent.Reject,
                    UserId = "dfd",
                    RoomId = 1,
                    UserName = "test name"
                })
            });
        }


        #endregion

    }
}