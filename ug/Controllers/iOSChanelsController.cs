﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using ug.Models;

namespace ug.Controllers
{
#warning Задокументировать в вики
    /// <summary>
    /// Каналы уведомлений для пользователей андроид
    /// </summary>
    [Authorize]
    public class iOSChanelsController : BaseODataController
    {
        [EnableQuery]
        public IQueryable<iOSChanel> Get()
        {
            return db.iOSChanels.Where(x => x.IsDelete == false && x.UserId == AppUser.Id).AsQueryable();
        }
        /// <summary>
        /// Добавить Канал iOS
        /// </summary>
        /// <param name="item">Объект команды</param>
        /// <returns></returns>
        public IHttpActionResult Post(iOSChanel item)
        {
            if (ModelState.IsValid)
            {
                var items = db.iOSChanels.Where(x => x.UserId == AppUser.Id && !x.IsDelete).ToList();

                foreach (var chan in items)
                {
                    if((item.IsVoIP && chan.IsVoIP) || (!item.IsVoIP && !chan.IsVoIP))
                    {
                        chan.IsDelete = true;
                        db.SaveChanges();
                    } 
                }
                item.IsDelete = false;
                item.DateAdd = DateTime.Now;
                item.UserId = AppUser.Id;
                db.iOSChanels.Add(item);
                db.SaveChanges();
                return Ok(item);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}