﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using ug.Models;
using ug.Models.Money;
using ug.Models.Simple;

namespace ug.Controllers
{
    public class UserTransactionsController : BaseODataController
    {
        [EnableQuery]
        [Authorize]
        public IQueryable<UserTransaction> Get()
        {

            var UserId = AppUser.Id;
            return db.UsersTransactions.Where(a => a.UserId == UserId)
                                               .AsQueryable();
        }

        [Authorize]
        public IHttpActionResult Post(UserTransaction transaction)
        {
            var UserId = AppUser.Id;

           

            if (ModelState.IsValid)
            {
                var res = db.UsersTransactions.Add(new UserTransaction()
                {
                    UserId = UserId,
                    Value = transaction.Value,
                });

                db.SaveChanges();
                return Ok(res);
                
            }

            return BadRequest("Model is invalid");
        }
    }
}