﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using ug.Models;
using ug.Models.Events;

namespace ug.Controllers
{
    [Authorize]
    public class EStreamsController : BaseODataController
    {
        [EnableQuery]
        public IQueryable<EStream> Get()
        {
            var UserId = AppUser.Id;
            return db.EStreams.Where(x => x.IsDeleted == false).AsQueryable();
        }

        public async Task<IHttpActionResult> Post(EStream stream)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //только сам пользователь может добавлять себе специализации
            stream.UserId = AppUser.Id;
            var room = db.Rooms.Add(new Room() { IsEvent = true });
            stream.Room = room;
            db.EStreams.Add(stream);
            await db.SaveChangesAsync();
            return Created(stream);
        }

        public IHttpActionResult Patch(int key, EStream stream)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var userId = AppUser.Id;
            if (db.EStreams.Where(z => z.UserId == AppUser.Id && z.IsDeleted == false && z.Id == key).Any())
            {
                if (stream.DateEnd == null)
                {
                    stream.DateEnd = stream.DateStart.AddHours(1).AddMinutes(30);
                };
                // запрос к базе для извлечения референсной модели
                var eevent = db.EStreams.Where(z => z.UserId == AppUser.Id && z.IsDeleted == false && z.Id == key).FirstOrDefault();
                eevent.CostPerMinute = stream.CostPerMinute;
                eevent.DateUpdate = DateTime.Now;
                eevent.Description = stream.Description;
                eevent.FileId = stream.FileId;
                eevent.LimitVIPTickets = stream.LimitVIPTickets;
                eevent.LimitTickets = stream.LimitTickets;
                eevent.CostPerMinuteVIP = stream.CostPerMinuteVIP;
                eevent.CostPerMessage = stream.CostPerMessage;
                eevent.Name = stream.Name;
                eevent.OnlyVIP = stream.OnlyVIP;
                eevent.CostPerEvent = stream.CostPerEvent;
                eevent.CostPerEventVIP = stream.CostPerEventVIP;
                eevent.DateStart = stream.DateStart;
                eevent.DateEnd = stream.DateEnd > stream.DateStart ?
                                 stream.DateEnd :
                                 stream.DateStart.AddHours(1).AddMinutes(30);

                db.SaveChanges();
                return Ok(eevent);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        public IHttpActionResult Delete(int key)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //только сам пользователь может добавлять себе специализации
            var us = db.EStreams.Where(u => u.Id == key
                                                    && u.UserId == AppUser.Id
                                                    && u.IsDeleted == false)
                                                    .FirstOrDefault();
            if (us == null)
            {
                return BadRequest();
            }

            us.IsDeleted = true;
            us.Room.IsDeleted = true;
            db.SaveChanges();
            return Ok();
        }
    }
}