﻿using MoreLinq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Routing;
using ug.Models;
using ug.Models.Simple;
using ug.Models.Specialisations;

namespace ug.Controllers
{
    public class SpecialisationCategoryController : BaseODataController
    {
        /// <summary>
        /// "GET /odata/SpecialisationCategory/
        /// </summary>
        /// <returns></returns>
        [EnableQuery]
        public IQueryable<SpecialisationCategory> Get()
        {
            return db.SpecialisationsCategories.Where(x=>x.IsSystem == true).AsQueryable();
        }
    }
}