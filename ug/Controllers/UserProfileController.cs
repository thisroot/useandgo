﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Web.Mvc;
using ug.Models;
using ug.Models.Simple;

namespace ug.Controllers
{
   
    public class UserProfileController : Controller
    {
        static private ApplicationDbContext db = new ApplicationDbContext();

        [Authorize]
        [HttpGet]
        // GET: UserProfile
        public ActionResult Index(string Id)
        {
            
            var uid = User.Identity.Name;
            var user = db.Users.Where(u => u.UserName == uid).FirstOrDefault();

            UserProfile up = new UserProfile();

            if(user.Id == Id || Id == null)
            {
                 up = new UserProfile(user, true);
            } else if(db.Users.Where(u => u.Id == Id).Any())
            {
                var internalUser = db.Users.Where(u => u.Id == Id).FirstOrDefault();
                up = new UserProfile(internalUser, false);
                
            } else
            {
                return Json(new { status = "error", error = "user cant find" }, JsonRequestBehavior.AllowGet);
            }

            var respond = JsonConvert.SerializeObject(up);
            return Content(respond, "application/json");
            //return Json(new { UserProfile = up }, JsonRequestBehavior.AllowGet);
        }

     
        [Authorize]
        [HttpPatch]
        // PATCH: UserProfile
        public ActionResult Index(SimpleUserCard ucard)
        {
            var uid = User.Identity.Name;
            //для обновления данных должны быть предоставлены все поля (кроме id), 
            //иначе, те поля которые не указаны станут пустыми
            var user = db.Users.Where(u => u.PhoneNumber == uid).FirstOrDefault();
            if (String.IsNullOrEmpty(ucard.FirstName) == false)
            {
                user.FirstName = ucard.FirstName;
            }
            if (String.IsNullOrEmpty(ucard.SecondName) == false)
            {
                user.SecondName = ucard.SecondName;
            }
            if (String.IsNullOrEmpty(ucard.Patronymic) == false)
            {
                user.Patronymic = ucard.Patronymic;
            }
            if (ucard.DateBirth.ToString() != "01.01.0001 0:00:00")
            {  
                    user.DateBirth = ucard.DateBirth;
            }
            if (String.IsNullOrEmpty(ucard.AvatarId) == false)
            {
                user.AvatarId = ucard.AvatarId;
            }                      
            db.SaveChanges();
            var up = new UserProfile(user, true);
            var respond = JsonConvert.SerializeObject(up);
            return Content(respond, "application/json");
        }
    }
}