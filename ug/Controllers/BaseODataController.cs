﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.OData;
using ug.Models;

namespace ug.Controllers
{
    public class BaseODataController : ODataController
    {
        protected ApplicationDbContext db = new ApplicationDbContext();
        protected ApplicationUser AppUser
        {
            get
            {
                string userName = User.Identity.GetUserName();
                return db.Users.FirstOrDefault(x => x.UserName == userName);
            }
        }
        
    }
}