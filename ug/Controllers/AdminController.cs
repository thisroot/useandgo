﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ug.Models;

namespace ug.Controllers
{
    /// <summary>
    /// Контроллер администрирования
    /// Системы
    /// </summary>
    public class AdminController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Logs()
        {
            return View(db.Logs.OrderByDescending(s=>s.DateAdd).Take(100).ToList());
        }

        public ActionResult StreamerEvents()
        {
            return View(db.StreamEvents.OrderByDescending(s => s.DateAdd).Take(100).ToList());
        }

        public ActionResult Events()
        {
            return View(db.EEvents.OrderByDescending(s => s.DateAdd).Take(100).ToList());
        }


    }
}