﻿using System.Linq;
using System.Web.Mvc;
using ug.Models;
using ug.Models.Simple;
using System.Collections.Generic;
using MoreLinq;
using System.Data.SqlClient;
using Newtonsoft.Json;

namespace ug.Controllers
{
    public class FunctionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        /// <summary>
        /// Получить друзей пользователя с комнатами
        /// POST odata/Contacts/Func.Friends
        /// </summary>
        /// <returns></returns>
        //[Authorize]
        //[HttpGet]
        //public ActionResult FriendsContacts()
        //{
        //    var name = User.Identity.Name;
        //    var uid = db.Users.Where(u => u.UserName == name).FirstOrDefault().Id;

        //    var contacts = Contact.getFriends(uid);
        //    return Json(new { Contacts = contacts }, JsonRequestBehavior.AllowGet);
        //}


        [Authorize]
        [HttpGet]
        public ActionResult Friends()
        {

            var uid = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault().Id;

            // получаем список друзей пользователя с комнатами
            var query = (from uf in db.UserFriends
                         join u1 in db.Users on uf.UserId equals u1.Id
                         join u2 in db.Users on uf.UserIdFriend equals u2.Id
                         join ur in db.UsersRooms on u2.Id equals ur.UserId
                         where u1.Id == uid && ur.IsDeleted == false &&
                         (from ur1 in db.UsersRooms where ur1.UserId == uid && ur1.Room.TypeRoom == Room.RoomType.Friends select ur1.RoomId).Contains(ur.RoomId)
                         && uf.IsDeleted == false
                         select new { u2, ur.RoomId }).ToList();

            var friends = new List<FriendRequest>();
            query.ForEach(e =>
            {
                friends.Add(new FriendRequest(e.u2, e.RoomId));
            });

            var respond = JsonConvert.SerializeObject(friends);
            return Content(respond, "application/json");
        }



        /// <summary>
        /// POST: odata/Contacts/Func.Set
        ///  - Добавляет контакты @mode = add
        ///  - Перезаписывает контакты @mode = rw
        /// Если контакт зарегестрирован, то добавляет в друзья, создает комнату и обновляет ид друга в контактах пользователя
        /// </summary>
        /// <param name="parameters">
        /// </param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public ActionResult SetContacts(List<Contact> contacts, string mode)
        {
            if (ModelState.IsValid && contacts != null)
            {

                var uid = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
                //Убираем дубликаты по номерам
                contacts = contacts.DistinctBy(w => w.Phone).ToList();
                //обновим значение пользователя контактов
                contacts = contacts.Select(sel => { sel.UserId = uid.Id; return sel; }).ToList();
                var phones = contacts.Select(ph => ph.Phone).ToList();

                //перезапись - удалим все прежние контакты
                if (mode == "rw")
                {
                    int countUpcontacts = db.Database.ExecuteSqlCommand("UPDATE dbo.Contacts SET IsDeleted = 'true' WHERE UserId = @UserId", new SqlParameter("@UserId", uid.Id));
                    db.SaveChanges();
                }
                else
                {
                    //удалим те из контактов, которые имеют номера
                    if (db.Contacts.Where(x => phones.Contains(x.Phone) && x.UserId == uid.Id).Any())
                    {
                        var co = db.Contacts.Where(x => phones.Contains(x.Phone) && x.UserId == uid.Id);

                        co.ForEach(a =>
                        {
                            a.IsDeleted = true;
                        });
                        db.SaveChanges();
                    }
                }

                //добавили контакты
                var newContacts = db.Contacts.AddRange(contacts);
                db.SaveChanges();

                if (db.Users.Where(x => phones.Contains(x.PhoneNumber) && x.PhoneNumber != uid.PhoneNumber).Any())
                {
                    var users = db.Users.Where(x => phones.Contains(x.PhoneNumber) && x.PhoneNumber != uid.PhoneNumber);
                    foreach (var s in users)
                    {
                        //если пользователь не является самим собой
                        if (s != uid)
                        {
                            //добавим пользователя в контакт
                            //var contact = newContacts.Where(nc => nc.Phone == s.PhoneNumber).FirstOrDefault();
                            //if (contact != null) contact.FriendId = s.Id;
                            //добавить в друзья
                            uid.AddFriend(s);
                        }
                    }
                }
                return Json(new { state = true });
            }
            else
            {
                return Json(new { state = false });
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult InvitationAction(InvitationRequest request)
        {

            var name = User.Identity.Name;
            var uid = db.Users.Where(u => u.UserName == name).FirstOrDefault().Id;

            if (uid != null)
            {
                switch (request.InvitationAction)
                {
                    case (InvitationRequest.InvAction.Add):
                        //проверяем на наличие инвайта
                        if (db.UserInvitations.Where(a => a.UserIdFriend == request.UserId && a.UserId == uid && a.IsDeleted == false).Any())
                        {
                            var inv = db.UserInvitations.Where(a => a.UserIdFriend == request.UserId && a.UserId == uid && a.IsDeleted == false).FirstOrDefault();
                            inv.User = null;
                            inv.FriendUser = null;
                            return Json(new { inv });
                        }

                        var d = db.UserInvitations.Add(new UserInvitation()
                        {
                            UserIdFriend = request.UserId,
                            UserId = uid,
                        });

                        db.SaveChanges();
                        d.User = null;
                        d.FriendUser = null;
                        return Json(new { d });

                    case (InvitationRequest.InvAction.Accept):
                        var r = db.UserInvitations.Where(x => x.Id == request.Id && x.UserIdFriend == uid && x.IsDeleted == false).FirstOrDefault();
                        if (r != null)
                        {
                            UserFriend.Add(r);
                            r.IsDeleted = true;
                            db.SaveChanges();
                            return Json(new { Status = "Acepted" });
                        }
                        else
                        {
                            return Json(new { Status = "Error" });
                        }
                    case (InvitationRequest.InvAction.Reject):
                        var w = db.UserInvitations.Where(x => x.Id == request.Id && x.UserIdFriend == uid && x.IsDeleted == false).FirstOrDefault();
                        if (w != null)
                        {
                            w.IsDeleted = true;
                            db.SaveChanges();
                            return Json(new { Status = "Rejected" });
                        }
                        else
                        {
                            return Json(new { Status = "Error" });
                        }
                }
            }

            return Json(new { Status = "Error" });
        }

    }
}