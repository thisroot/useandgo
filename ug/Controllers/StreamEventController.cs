﻿using System.Linq;
using ug.Models;
using ug.Models.Simple;
using System.Collections.Generic;
using MoreLinq;
using System.Data.SqlClient;
using ug.Models.StreamManager;
using System.Web.Mvc;
using System;
using System.Web;
using AutoMapper;
using Newtonsoft.Json.Linq;
using ug.Models.Events;
using Microsoft.AspNet.SignalR.Client;
using ug.Hubs;
using System.Data.Entity;

namespace ug.Controllers
{
    public class StreamEventController : Controller
    {
        //private HubConnection connection;
        //private IHubProxy hub;

        //public StreamEventController()
        //{
        //    try
        //    {
        //        connection = new HubConnection("http://localhost:61124/");
        //        hub = connection.CreateHubProxy("RoomHub");
        //        connection.Start();
        //    }
        //    catch (Exception e)
        //    {
        //        db.Logs.Add(new Log
        //        {
        //            Level = Log.Levels.Error,
        //            Info = "Error create RooomHub client in StreamEventController" + e
        //        });
        //    }
        //}

        private double TimeBorder { get; set; } = 5;

        private ApplicationDbContext db = new ApplicationDbContext();
        /// <summary>
        /// со стримингового сервера передается колбек, с целью валидации пользователя.
        /// параметры полей POST запроса
        /// app должно быть типом события
        /// streamname - id события
        /// В теле сообщения в параметре "tcUrl" выделяются GET переменные.
        /// key токен юзера
        /// </summary>
        /// <param name="ser"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public ActionResult OnConnect(StreamEventRequest ser)
        {

            //RoomHub.SendAllUsersMessages();
            if (!ModelState.IsValid) { return Content("Not Found"); }

            //если запрос идет с локального сервера
            if (ser.ip == "127.0.0.1") return Content("0");

            //проверка на существование пользователя
            var url = new Uri(ser.tcUrl);
            var token = HttpUtility.ParseQueryString(url.Query).Get("key");
            if (db.UsersTokens.Where(x => x.Token == token && token != null).Any() == false) { return Content("Not Found"); }

            var utoken = db.UsersTokens.Where(x => x.Token == token).FirstOrDefault();

            //Id стрима при коннекте не передается
            // просто сохраняем пользователя как неопределенного и его URL

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<StreamEventRequest, StreamEvent>();
            });

            var stream = Mapper.Map<StreamEvent>(ser);
            stream.EventStream = TypeStreamEvent.Connect;
            stream.UserId = utoken.UserId;
            stream.ClientId = ser.client_id;
            db.StreamEvents.Add(stream);
            db.SaveChanges();
            return Content("0");
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult OnClose(StreamEventRequest ser)
        {
            //смысла проверку на закрытие соединения нет
            return Content("0");

            //if(!ModelState.IsValid)
            //{
            //    return Content("Not Found");
            //}

            ////если запрос идет с локального сервера
            //if (ser.ip == "127.0.0.1") return Content("0");


            //var streamer = db.StreamEvents.Where(x => x.ClientId == ser.client_id &&
            //                                //либо публикующий
            //                               (x.EventStream == TypeStreamEvent.Publish) || 
            //                               (x.EventStream == TypeStreamEvent.UnPublish) ||
            //                               //либо просматривающий
            //                               (x.EventStream == TypeStreamEvent.Play) ||
            //                               (x.EventStream == TypeStreamEvent.Stop))
            //                               .OrderByDescending(s => s.DateAdd).FirstOrDefault();

            //if (streamer == null) return Content("0");

            ////обновить статус события
            ////проверка на существание зарегистрированного события.
            //int eventId;
            //if (!Int32.TryParse(streamer.Stream, out eventId)) return Content("Not Found");
            //if (eventId == 0) return Content("Not Found");
            //var d = db.EEvents.Where(z => z.IdEvent == eventId && z.Event == streamer.App &&
            //                              (z.EventStatus != StatusEvent.Blocked ||
            //                               z.EventStatus != StatusEvent.Finished))
            //                         .OrderByDescending(s => s.DateAdd).FirstOrDefault();

            //if (d == null) return Content("Not Found");
            //var eevent = d.GetObject(db);
            //((IEEvents)eevent).ChangeStatus(StatusEvent.Pending);


            //Mapper.Initialize(cfg =>
            //{
            //    cfg.CreateMap<StreamEventRequest, StreamEvent>();
            //});

            //var stream = Mapper.Map<StreamEvent>(ser);
            //stream.EventStream = TypeStreamEvent.Close;
            //stream.Stream = streamer.Stream;
            //stream.UserId = streamer.UserId;
            //stream.ClientId = streamer.ClientId;
            //stream.RecvBytes = ser.recv_bytes;
            //stream.SendBytes = ser.send_bytes;

            //db.StreamEvents.Add(stream);
            //db.SaveChanges();

            //return Content("0");
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult OnPublish(StreamEventRequest ser)
        {

            if (!ModelState.IsValid)
            {
                return Content("Not Found");
            }

            //если запрос идет с локального сервера
            if (ser.ip == "127.0.0.1") return Content("0");

            //проверка на существование пользователя
            var url = new Uri(ser.tcUrl);
            var token = HttpUtility.ParseQueryString(url.Query).Get("key");
            if (db.UsersTokens.Where(x => x.Token == token).Any())
            {
                var utoken = db.UsersTokens.Where(x => x.Token == token).FirstOrDefault();
                var Downtime = DateTime.Now.AddHours(-TimeBorder);
                var UpTime = DateTime.Now.AddHours(TimeBorder);

                int eventId;
                if (!Int32.TryParse(ser.stream,out eventId)) return Content("Not Found");
                if (eventId == 0) return Content("Not Found");
                //проверка на существание зарегистрированного события.
                var d = db.EEvents.Where(z => z.Event == ser.app && z.Id == eventId &&
                                              z.UserId == utoken.UserId &&
                                              (z.EventStatus != StatusEvent.Finished ||
                                               z.EventStatus != StatusEvent.Blocked))
                                         .OrderByDescending(s => s.DateAdd).FirstOrDefault();

                if (d == null) return Content("Not found");
                var eevent = d.GetObject(db);
                ((IEEvents)eevent).ChangeStatus(StatusEvent.Started);


                var streamer = db.StreamEvents.Where(x => x.ClientId == ser.client_id &&
                                            x.EventStream == TypeStreamEvent.Connect)
                                            .OrderByDescending(z => z.DateAdd).FirstOrDefault();

                if (streamer == null) return Content("Not found");

                //отправим уведомление о начале трансляции
                RoomHub.StreamCallBack(streamer.User, streamer, d);

                if (!Int32.TryParse(d.IdEvent.ToString(), out eventId)) return Content("Not Found");

                streamer.StreamerType = TypeStreamer.Publisher;
                streamer.Stream = ser.stream;
                db.SaveChanges();
                return Content("0");
            }

            return Content("Not found");
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult OnUnpublish(StreamEventRequest ser)
        {

            return Content("0");

            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(200);
            }

            //если запрос идет с локального сервера
            if (ser.ip == "127.0.0.1") return Content("0");

            //var Downtime = DateTime.Now.AddHours(-TimeBorder);
            //var UpTime = DateTime.Now.AddHours(TimeBorder);

            var streamer = db.StreamEvents.Where(x => x.ClientId == ser.client_id &&
                                        x.EventStream == TypeStreamEvent.Publish)
                                        .OrderByDescending(s => s.DateAdd).FirstOrDefault();

            if (streamer == null) return Content("Not Found");

            //обновить статус события
            //проверка на существание зарегистрированного события.
            int eventId;
            if (!Int32.TryParse(streamer.Stream, out eventId)) return Content("Not Found");


          
            if (eventId == 0) return Content("Not Found");
            var d = db.EEvents.Where(z => z.Id == eventId &&
                                          z.Event == ser.app &&
                                          z.UserId == streamer.UserId &&
                                          (z.EventStatus != StatusEvent.Blocked ||
                                           z.EventStatus != StatusEvent.Finished))
                                     .OrderByDescending(s => s.DateAdd).FirstOrDefault();

            if (d == null) return Content("Not Found");


            var eevent = d.GetObject(db);
            ((IEEvents)eevent).ChangeStatus(StatusEvent.Pending);

            streamer.EventStream = TypeStreamEvent.UnPublish;
            db.SaveChanges();
            return Content("0");
        }

        [Authorize]
        [HttpDelete]
        public ActionResult StopStream(StreamEventRequest ser)
        {

            //если запрос идет с локального сервера
            if (ser.ip == "127.0.0.1") return Content("0");

            var uid = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();

            try
            {
                var streamId = int.Parse(ser.stream);
                var d = db.EEvents.FirstOrDefault(u => u.UserId == uid.Id && u.Id == streamId && u.Event == ser.app && (u.EventStatus == StatusEvent.Pending ||
                                                                  u.EventStatus == StatusEvent.Started &&
                                                                  u.EventStatus != StatusEvent.Blocked));

                if (d == null) return Content("Not Found");
                var eevent = d.GetObject(db);
                ((IEEvents)eevent).ChangeStatus(StatusEvent.Finished);

                db.SaveChanges();
                return Content("0");
            }
            catch
            {
                return Content("Not Found");
            }

        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult OnPlay(StreamEventRequest ser)
        {

            if (!ModelState.IsValid) { return Content("Not Found"); }
            //если запрос идет с локального сервера
            if (ser.ip == "127.0.0.1") return Content("0");

            //найдем пользователя по статусу его коннекта
            var viewer = db.StreamEvents.Where(s => s.ClientId == ser.client_id && 
            (s.EventStream == TypeStreamEvent.Connect) ||
            (s.EventStream == TypeStreamEvent.Play) ||
            (s.EventStream == TypeStreamEvent.Stop))
                .OrderByDescending(s => s.DateAdd).FirstOrDefault();

            if (viewer == null) { return Content("Not Found"); }

            int eventId;
            if (!Int32.TryParse(ser.stream, out eventId)) return Content("Not Found");
            if (eventId == 0) return Content("Not Found");
            //получаем стрим если он активен
            var d = db.EEvents.Where(z => z.Event == ser.app && z.Id == eventId &&
                                             (z.EventStatus == StatusEvent.Started))
                                        .OrderByDescending(s => s.DateAdd).FirstOrDefault();


            if (d == null) return Content("Not found");

            //является ли пользователь подписчиком на событие
            var urole = d.Room.UserRooms.FirstOrDefault(s => s.UserId == viewer.UserId && s.Role != UserRoom.MemberRole.Admin);
            if (urole == null) { return Content("Not Found"); }

          
            //проверка на наличие средств у пользователя и их списание
            if (d.CostPerEvent != 0 || d.CostPerEventVIP != 0)
            {

                if (d.CostPerEvent != 0 && urole.Role == UserRoom.MemberRole.Free && urole.User.Balance >= d.CostPerEvent)
                {
                    urole.User.MoneyTransfer(d.CostPerEvent, d.UserId);
                }
                else if (d.CostPerEventVIP != 0 && urole.Role == UserRoom.MemberRole.Vip && urole.User.Balance >= d.CostPerEventVIP)
                {
                    urole.User.MoneyTransfer(d.CostPerEventVIP, d.UserId);
                }
                else
                {
                    return Content("Not Found");
                }
            }

            viewer.StreamerType = TypeStreamer.Viewer;
            viewer.Stream = ser.stream;
            viewer.EventStream = TypeStreamEvent.Play;
            db.SaveChanges();
            return Content("0");
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult OnStop(StreamEventRequest ser)
        {

            if (!ModelState.IsValid) { return Content("Not Found"); }
            //если запрос идет с локального сервера
            if (ser.ip == "127.0.0.1") return Content("0");

            //найдем пользователя по статусу его коннекта
            var viewer = db.StreamEvents.Where(s => s.ClientId == ser.client_id &&
            s.StreamerType == TypeStreamer.Viewer &&
            (s.EventStream == TypeStreamEvent.Play) ||
            (s.EventStream == TypeStreamEvent.Stop) 
            )
                .OrderByDescending(s => s.DateAdd).FirstOrDefault();

            if (viewer == null) { return Content("Not Found"); }

            int eventId;
            if (!Int32.TryParse(ser.stream, out eventId)) return Content("Not Found");
            if (eventId == 0) return Content("Not Found");

            viewer.EventStream = TypeStreamEvent.Stop;
            db.SaveChanges();
            return Content("0");
        }
    }
}