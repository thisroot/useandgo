﻿using System.Linq;
using System.Web.Mvc;
using ug.Models;
using ug.Models.Simple;
using System.Collections.Generic;
using MoreLinq;
using System.Data.SqlClient;
using Newtonsoft.Json;

namespace ug.Controllers
{
    [Authorize]
    public class WidgetsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Widgets
        // Список виджетов
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TopBloggers()
        {

         
            //SELECT us.OnSubscribeId, COUNT(us.OnSubscribeId) AS Qty_model
            //    FROM UserSubscribtions us
            //    GROUP BY us.OnSubscribeId
            //        ORDER BY Qty_model

            var us = db.UsersSubscribtions.GroupBy(s => s.OnSubscribeUser)
                .Select(d=> new { User = d.Key, Count = d.Count() })
                .OrderByDescending(r=>r.Count)
                .Take(100).ToList();

            var uc = new SimpleUserCard();

            List<SimpleUserCard> group = new List<SimpleUserCard>();
            us.ForEach(d =>
            {
                var blogger = new SimpleUserCard
                {
                    Id = d.User.Id,
                    FirstName = d.User.FirstName,
                    SecondName = d.User.SecondName,
                    AvatarId = d.User.AvatarId,
                    CountSubscribers = d.Count,
                    DateBirth = d.User.DateBirth,
                    Patronymic = d.User.Patronymic,
                };

                group.Add(blogger);
            });

            return Content(JsonConvert.SerializeObject(group), "application/json");
        }
    }
}