﻿using MoreLinq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Routing;
using ug.Models;
using ug.Models.Simple;
using ug.Models.Specialisations;

namespace ug.Controllers
{
    [Authorize]
    public class UserSpecialisationsController : BaseODataController
    {
        /// <summary>
        /// "GET /odata/UserSpecialisations/
        /// </summary>
        /// <returns></returns>
        [EnableQuery]
        public IQueryable<UserSpecialisations> Get()
        {
            return db.UsersSpecialisations.Where(u => u.IsDeleted == false).AsQueryable();
        }


        public async Task<IHttpActionResult> Post(UserSpecialisations specialisation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //только сам пользователь может добавлять себе специализации
            specialisation.UserId = AppUser.Id;
            //проверка на добавление исключительно системных базовый тем специализаций
            var spec = db.Specialisations.Where(x => x.Id == specialisation.SpecialisationId).FirstOrDefault();
            if (spec != null)
            {
                db.UsersSpecialisations.Add(specialisation);
                await db.SaveChangesAsync();
                return Created(specialisation);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        public IHttpActionResult Patch(int key, UserSpecialisations specialisation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var userId = AppUser.Id;
            if (db.UsersSpecialisations.Where(z => z.UserId == AppUser.Id && z.Specialisation.IsDeleted == false && z.Id == key).Any())
            {
                var item = db.UsersSpecialisations.FirstOrDefault(x => x.Id == key);
                item.SpecialisationId = specialisation.SpecialisationId;
                item.Profession = specialisation.Profession;
                item.Description = specialisation.Description;
                item.CostPerMinuteCall = specialisation.CostPerMinuteCall;
                item.CostPerMessage = specialisation.CostPerMessage;
                item.Experience = specialisation.Experience;
                item.DateUpdate = DateTime.Now;
                db.SaveChanges();
                return Ok(item);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        public IHttpActionResult Delete(int key)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //только сам пользователь может добавлять себе специализации
            var us = db.UsersSpecialisations.Where(u => u.Id == key
                                                    && u.UserId == AppUser.Id
                                                    && u.IsDeleted == false)
                                                    .FirstOrDefault();
            if (us == null)
            {
                return BadRequest();
            }

            us.IsDeleted = true;
            us.Subscribtions.ForEach(e => { e.IsDeleted = true; e.Room.IsDeleted = true; });         
            db.SaveChanges();
            return Ok();
        }

    }
}