﻿using MoreLinq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Routing;
using ug.Models;
using ug.Models.Simple;

namespace ug.Controllers
{
    /// <summary>
    /// Контроллер контактов
    /// </summary>

    [Authorize]
    //odata/Contacts
    public class ContactsController : BaseODataController
    {
        [EnableQuery(MaxExpansionDepth = 2)]
        //GET: odata/Contacts
        public IQueryable<Models.Contact> Get()
        {
            var contacts = db.Contacts.Where(u => u.UserId == AppUser.Id && u.IsDeleted == false).DistinctBy(r => r.LocalId).AsQueryable();
            return contacts;
        }

        public IHttpActionResult Post(Contact contact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var u = AppUser.Id;
            contact.UserId = u;

            var oc = db.Contacts.Where(x => x.UserId == u && x.Phone == contact.Phone).FirstOrDefault();
            //удаляем если контакт с таким номером уже есть у пользователя
            if (oc != null) oc.IsDeleted = true;
            db.Contacts.Add(contact);
            db.SaveChanges();
            return Ok(contact);
        }
    }
}