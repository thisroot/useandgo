﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using ug.Models;
using ug.Models.Events;

namespace ug.Controllers
{
    [Authorize]
    public class EEventsController : BaseODataController
    {
        [EnableQuery]
        public IQueryable<EEvent> Get()
        {
            var UserId = AppUser.Id;
            return db.EEvents.Where(x => x.IsDeleted == false).AsQueryable();
        }

        //public async Task<IHttpActionResult> Post(EEvent item)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var room = new Room();
        //    item.RoomId = room.Id;

        //    item.UserId = AppUser.Id;

        //    Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<EEvent, EWebinar>();
        //        cfg.CreateMap<EEvent, EStandUp>();
        //        cfg.CreateMap<EEvent, EChallange>();
        //        cfg.CreateMap<EEvent, EBattle>();
        //    });

        //    dynamic result;

        //    switch (item.Event)
        //    {
        //        case ("webinar"):
        //            EWebinar webinar = Mapper.Map<EWebinar>(item);
        //            result = db.EWebinars.Add(webinar);
        //            break;
        //        case ("standup"):
        //            EStandUp standup = Mapper.Map<EStandUp>(item);
        //            result = db.EStandUps.Add(standup);
        //            break;
        //        case ("challange"):
        //            EChallange challange = Mapper.Map<EChallange>(item);
        //            result = db.EChallanges.Add(challange);
        //            break;
        //        case ("battle"):
        //            EBattle battle = Mapper.Map<EBattle>(item);
        //            result = db.EBattles.Add(battle);
        //            break;
        //        default:
        //            return BadRequest("Incorrect event Type");
        //    }

        //    await db.SaveChangesAsync();
        //    return Created(result);
        //}

        //public IHttpActionResult Patch(int key, EEvent item)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    item.UserId = AppUser.Id;
        //    item.IdEvent = key;



        //    Mapper.Initialize(cfg =>
        //    {
        //        cfg.CreateMap<EEvent, EWebinar>();
        //        cfg.CreateMap<EEvent, EStandUp>();
        //        cfg.CreateMap<EEvent, EChallange>();
        //        cfg.CreateMap<EEvent, EBattle>();
        //    });

        //    dynamic entry;

        //    switch (item.Event)
        //    {
        //        case ("webinar"):
        //            EWebinar webinar = Mapper.Map<EWebinar>(item);
        //            db.EWebinars.Attach(webinar);
        //            entry = db.Entry(webinar);
        //            break;
        //        case ("standup"):
        //            EStandUp standup = Mapper.Map<EStandUp>(item);
        //            db.EStandUps.Attach(standup);
        //            entry = db.Entry(standup);
        //            break;
        //        case ("challange"):
        //            EChallange challange = Mapper.Map<EChallange>(item);
        //            db.EChallanges.Attach(challange);
        //            entry = db.Entry(challange);
        //            break;
        //        case ("battle"):
        //            EBattle battle = Mapper.Map<EBattle>(item);
        //            db.EBattles.Add(battle);
        //            entry = db.Entry(battle);
        //            break;
        //        default:
        //            GC.Collect();
        //            return BadRequest("Incorrect event Type");
        //    }

        //    entry.State = EntityState.Modified;
        //    //entry.Property(e => e.DateAdd).IsModified = false;
        //    //entry.Property(e => e.IsDeleted).IsModified = false;

        //    db.SaveChanges();
        //    return Ok(entry);
        //}

        //public IHttpActionResult Delete(int key)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    //только сам пользователь может добавлять себе специализации
        //    var us = db.EChallanges.Where(u => u.Id == key
        //                                            && u.UserId == AppUser.Id
        //                                            && u.IsDeleted == false)
        //                                            .FirstOrDefault();
        //    if (us == null)
        //    {
        //        return BadRequest();
        //    }

        //    us.IsDeleted = true;
        //    db.SaveChanges();
        //    return Ok();
        //}
    }
}