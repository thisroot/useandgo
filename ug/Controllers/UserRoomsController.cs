﻿using MoreLinq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Routing;
using ug.Models;
using ug.Models.Simple;

namespace ug.Controllers
{
    public class UserRoomsController : BaseODataController
    {
        [Authorize]
        [EnableQuery]
        public IQueryable<UserRoom> Get()
        {
            return db.UsersRooms.Where(u => u.UserId == AppUser.Id && u.IsDeleted == false).AsQueryable();
        }
        [Authorize]
        [EnableQuery]
        public IQueryable<UserRoom> Get(int key)
        {
            return db.UsersRooms.Where(u => u.UserId == AppUser.Id && u.RoomId == key).AsQueryable();
        }
    }
}
