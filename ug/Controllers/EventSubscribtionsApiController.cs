﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ug.Models;
using ug.Models.Events;
using ug.Models.Simple;
using ug.Models.Specialisations;

namespace ug.Controllers
{
    [Authorize]
    public class EventSubscribtionsApiController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/EventSubscribtionsApi/5
        public IHttpActionResult Get()
        {
            var uid = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var urooms = db.UsersRooms.Where(x => x.UserId == uid.Id && !x.Room.IsDeleted &&
                                                  x.Room.IsEvent && !x.IsDeleted);
            var events = db.EEvents.Where(y => urooms.Select(a => a.RoomId).ToList().Contains(y.RoomId) && !y.IsDeleted).Select(s => new
            {
                Id = s.Id,
                IdEvent = s.IdEvent,
                UserId = s.UserId,
                EventType = s.Event,
                Name = s.Name,
                Description = s.Description,
                FileId = s.FileId,
                RoomId = s.RoomId,
                CostPerEvent = s.CostPerEvent,
                OnlyVIP = s.OnlyVIP,
                LimitVIPTickets = s.LimitVIPTickets,
                CostPerMinute = s.CostPerMinute,
                CostPerMessage = s.CostPerMessage,
                LimitTickets = s.LimitTickets,
                CostPerMinuteVIP = s.CostPerMinuteVIP,
                UserRooms = s.Room.UserRooms.Where(ws => !ws.IsDeleted)
                        .Select(a => new
                        {
                            UserId = a.UserId,
                            Role = a.Role,
                            RoomId = a.RoomId,
                            MPermission = a.MPermission,
                            FileTransfer = a.FileTransfer,
                            ChatWriter = a.ChatWriter,
                            Id = a.Id
                        }),

                SpecialisationId = s.SpecialisationId,
                DateStart = s.DateStart,
                DateEnd = s.DateEnd
            }).ToList();

            return Json(events);
        }

        // GET: api/EventSubscribtionsApi/5
        public IHttpActionResult Get(int id)
        {

            var uid = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var urooms = db.UsersRooms.Where(x => x.UserId == uid.Id && !x.Room.IsDeleted &&
                                                  x.Room.IsEvent && !x.IsDeleted);

            var events = db.EEvents.Where(y => y.Id == id && urooms.Select(a => a.RoomId).ToList().Contains(y.RoomId) && !y.IsDeleted).Select(s => new
            {
                Id = s.Id,
                IdEvent = s.IdEvent,
                UserId = s.UserId,
                EventType = s.Event,
                Name = s.Name,
                Description = s.Description,
                FileId = s.FileId,
                RoomId = s.RoomId,
                CostPerEvent = s.CostPerEvent,
                CostPerMessage = s.CostPerMessage,
                OnlyVIP = s.OnlyVIP,
                LimitVIPTickets = s.LimitVIPTickets,
                CostPerMinute = s.CostPerMinute,
                LimitTickets = s.LimitTickets,
                CostPerMinuteVIP = s.CostPerMinuteVIP,
                UserRooms = s.Room.UserRooms.Where(ws => !ws.IsDeleted)
                   .Select(a => new
                   {
                       UserId = a.UserId,
                       Role = a.Role,
                       RoomId = a.RoomId,
                       MPermission = a.MPermission,
                       FileTransfer = a.FileTransfer,
                       ChatWriter = a.ChatWriter,
                       Id = a.Id
                   }),

                SpecialisationId = s.SpecialisationId,
                DateStart = s.DateStart,
                DateEnd = s.DateEnd
            }).ToList();


            if (events == null)
            {
                BadRequest("Events can't find");
            }

            return Json(events);
        }

        // POST: api/EventSubscribtionsApi
        public IHttpActionResult Post([FromBody]EventSubscribeRequest esr)
        {
            var uid = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            UserRoom Ur;

            switch (esr.EventType)
            {
                case (EventSubscribeRequest.TypeEvent.Webinar):
                    var webinar = db.EWebinars.Where(xw => xw.Id == esr.EventId && !xw.IsDeleted && xw.UserId != uid.Id).FirstOrDefault();
                    if (webinar == null) return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Event can't found" }));
                    //проверка на лимит Vip пользователей
                    if (webinar.OnlyVIP && !esr.IsVIP) return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Only Vips can connect to event " }));
                    if (webinar.LimitVIPTickets != null && webinar.Room.UserRooms
                       .Where(x => x.Role == UserRoom.MemberRole.Vip && !x.IsBlocked && !x.IsDeleted).Count() >= webinar.LimitVIPTickets)
                    {
                        return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Exceeded number of Vips" }));
                    }

                    if (webinar.Room.UserRooms.Where(s => uid.Id.Contains(s.UserId) && !s.IsDeleted).Any())
                    {
                        return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Member allready registered" }));
                    }

                    Ur = new UserRoom
                    {
                        RoomId = webinar.RoomId,
                        UserId = uid.Id,
                        MPermission = UserRoom.MediaPermission.Viewer,
                    };

                    db.UsersRooms.Add(Ur);
                    db.SaveChanges();
                    return Json(new { Status = "sucess", Id = Ur.Id });


                case (EventSubscribeRequest.TypeEvent.Battle):
                    var battle = db.EBattles.Where(xw => xw.Id == esr.EventId && !xw.IsDeleted && xw.UserId != uid.Id).FirstOrDefault();
                    if (battle == null) return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Event can't found" }));
                    //проверка на лимит Vip пользователей
                    if (battle.OnlyVIP && !esr.IsVIP) return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Only Vips can connect to event " }));
                    if (battle.LimitVIPTickets != null && battle.Room.UserRooms
                       .Where(x => x.Role == UserRoom.MemberRole.Vip && !x.IsBlocked && !x.IsDeleted).Count() >= battle.LimitVIPTickets)
                    {
                        return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Exceeded number of Vips" }));
                    }

                    if (battle.Room.UserRooms.Where(s => uid.Id.Contains(s.UserId) && !s.IsDeleted).Any())
                    {
                        return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Member allready registered" }));
                    }

                    Ur = new UserRoom
                    {
                        RoomId = battle.RoomId,
                        UserId = uid.Id,
                        MPermission = UserRoom.MediaPermission.Viewer,
                    };

                    db.UsersRooms.Add(Ur);
                    db.SaveChanges();
                    return Json(new { Status = "sucess", Id = Ur.Id });

                case (EventSubscribeRequest.TypeEvent.Challange):
                    var challange = db.EChallenges.Where(xw => xw.Id == esr.EventId && !xw.IsDeleted && xw.UserId != uid.Id).FirstOrDefault();
                    if (challange == null) return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Event can't found" }));
                    //проверка на лимит Vip пользователей
                    if (challange.OnlyVIP && !esr.IsVIP) return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Only Vips can connect to event " }));
                    if (challange.LimitVIPTickets != null && challange.Room.UserRooms
                       .Where(x => x.Role == UserRoom.MemberRole.Vip && !x.IsBlocked && !x.IsDeleted).Count() >= challange.LimitVIPTickets)
                    {
                        return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Exceeded number of Vips" }));
                    }

                    if (challange.Room.UserRooms.Where(s => uid.Id.Contains(s.UserId) && !s.IsDeleted).Any())
                    {
                        return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Member allready registered" }));
                    }

                    Ur = new UserRoom
                    {
                        RoomId = challange.RoomId,
                        UserId = uid.Id,
                        MPermission = UserRoom.MediaPermission.Viewer,
                    };

                    db.UsersRooms.Add(Ur);
                    db.SaveChanges();
                    return Json(new { Status = "sucess", Id = Ur.Id });

                case (EventSubscribeRequest.TypeEvent.StandUp):
                    var standup = db.EStandUps.Where(xw => xw.Id == esr.EventId && !xw.IsDeleted && xw.UserId != uid.Id).FirstOrDefault();
                    if (standup == null) return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Event can't found" }));
                    //проверка на лимит Vip пользователей
                    if (standup.OnlyVIP && !esr.IsVIP) return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Only Vips can connect to event " }));
                    if (standup.LimitVIPTickets != null && standup.Room.UserRooms
                       .Where(x => x.Role == UserRoom.MemberRole.Vip && !x.IsBlocked && !x.IsDeleted).Count() >= standup.LimitVIPTickets)
                    {
                        return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Exceeded number of Vips" }));
                    }

                    if (standup.Room.UserRooms.Where(s => uid.Id.Contains(s.UserId) && !s.IsDeleted).Any())
                    {
                        return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Member allready registered" }));
                    }

                    Ur = new UserRoom
                    {
                        RoomId = standup.RoomId,
                        UserId = uid.Id,
                        MPermission = UserRoom.MediaPermission.Viewer,
                    };

                    db.UsersRooms.Add(Ur);
                    db.SaveChanges();
                    return Json(new { Status = "sucess", Id = Ur.Id });
                default:
                    return Json(new { Status = "error", Message = "Event type can't found" });
            }
        }

        // PUT: api/EventSubscribtionsApi
        public IHttpActionResult Put([FromBody]string request)
        {
            return Ok();
        }

        // DELETE: api/EventSubscribtionsApi/5/0
        public IHttpActionResult Delete(int EventId, EventSubscribeRequest.TypeEvent typeEvent)
        {
            var uid = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();

            switch (typeEvent)
            {
                case (EventSubscribeRequest.TypeEvent.Webinar):
                    var uwroom = db.EWebinars.Where(xw => xw.Id == EventId && !xw.IsDeleted && xw.UserId != uid.Id)
                                    .Select(a => a.Room.UserRooms.Where(e => e.UserId == uid.Id && !e.IsDeleted).FirstOrDefault()).FirstOrDefault();
                    if (uwroom == null) return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Event can't found" }));
                    uwroom.IsDeleted = true;
                    db.SaveChanges();
                    return Json(new { Status = "sucess", Id = uwroom.Id });
                case (EventSubscribeRequest.TypeEvent.Battle):
                    var ubroom = db.EBattles.Where(xw => xw.Id == EventId && !xw.IsDeleted && xw.UserId != uid.Id)
                                     .Select(a => a.Room.UserRooms.Where(e => e.UserId == uid.Id && !e.IsDeleted).FirstOrDefault()).FirstOrDefault();
                    if (ubroom == null) return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Event can't found" }));
                    ubroom.IsDeleted = true;
                    db.SaveChanges();
                    return Json(new { Status = "sucess", Id = ubroom.Id });

                case (EventSubscribeRequest.TypeEvent.Challange):
                    var ucroom = db.EChallenges.Where(xw => xw.Id == EventId && !xw.IsDeleted && xw.UserId != uid.Id)
                                    .Select(a => a.Room.UserRooms.Where(e => e.UserId == uid.Id && !e.IsDeleted).FirstOrDefault()).FirstOrDefault();
                    if (ucroom == null) return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Event can't found" }));
                    ucroom.IsDeleted = true;
                    db.SaveChanges();
                    return Json(new { Status = "sucess", Id = ucroom.Id });
                case (EventSubscribeRequest.TypeEvent.StandUp):
                    var usroom = db.EStandUps.Where(xw => xw.Id == EventId && !xw.IsDeleted && xw.UserId != uid.Id)
                                     .Select(a => a.Room.UserRooms.Where(e => e.UserId == uid.Id && !e.IsDeleted).FirstOrDefault()).FirstOrDefault();
                    if (usroom == null) return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Event can't found" }));
                    usroom.IsDeleted = true;
                    db.SaveChanges();
                    return Json(new { Status = "sucess", Id = usroom.Id });
                default:
                    return BadRequest(JsonConvert.SerializeObject(new { Status = "error", Message = "Event type can't found" }));
            }
        }
    }
}
