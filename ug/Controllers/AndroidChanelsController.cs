﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using ug.Models;

namespace ug.Controllers
{
#warning Задокументировать в вики
    /// <summary>
    /// Каналы уведомлений для пользователей андроид
    /// </summary>
    [Authorize]
    public class AndroidChanelsController : BaseODataController
    {
        [EnableQuery]
        [Authorize]
        public IQueryable<AndroidChanel> Get()
        {
            return db.AndroidChanels.Where(x => x.IsDelete == false && x.UserId == AppUser.Id).AsQueryable();
        }
        /// <summary>
        /// Добавить Канал андройда
        /// </summary>
        /// <param name="item">Объект команды</param>
        /// <returns></returns>
        [Authorize]
        public IHttpActionResult Post(AndroidChanel item)
        {
            if (ModelState.IsValid)
            {
                var items = db.AndroidChanels.Where(x => x.UserId == AppUser.Id).ToList();
                foreach (var chan in items)
                {
                    chan.IsDelete = true;
                    db.SaveChanges();
                }

                item.IsDelete = false;
                item.DateAdd = DateTime.Now;
                item.UserId = AppUser.Id;
                db.AndroidChanels.Add(item);
                db.SaveChanges();
                return Ok(item);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}