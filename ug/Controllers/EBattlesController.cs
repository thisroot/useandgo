﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using ug.Models;
using ug.Models.Events;

namespace ug.Controllers
{
    [Authorize]
    public class EBattlesController : BaseODataController
    {
        [EnableQuery]
        public IQueryable<EBattle> Get()
        {
            var UserId = AppUser.Id;
            return db.EBattles.Where(x => x.IsDeleted == false).AsQueryable();
        }

        public async Task<IHttpActionResult> Post(EBattle battle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (battle.DateEnd == null)
            {
                battle.DateEnd = battle.DateStart.AddHours(1).AddMinutes(30);
            };

            //только сам пользователь может добавлять себе специализации
            battle.UserId = AppUser.Id;
            var room = db.Rooms.Add(new Room() { IsEvent = true });
            battle.Room = room;
            db.EBattles.Add(battle);
            await db.SaveChangesAsync();
            return Created(battle);
        }

        public IHttpActionResult Patch(int key, EBattle battle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var userId = AppUser.Id;
            if (db.EBattles.Where(z => z.UserId == AppUser.Id && z.IsDeleted == false && z.Id == key).Any())
            {
                if (battle.DateEnd == null)
                {
                    battle.DateEnd = battle.DateStart.AddHours(1).AddMinutes(30);
                };

                // запрос к базе для извлечения референсной модели
                var eevent = db.EBattles.Where(z => z.UserId == AppUser.Id && z.IsDeleted == false && z.Id == key).FirstOrDefault();
                eevent.CostPerMinute = battle.CostPerMinute;
                eevent.DateUpdate = DateTime.Now;
                eevent.Description = battle.Description;
                eevent.FileId = battle.FileId;
                eevent.LimitVIPTickets = battle.LimitVIPTickets;
                eevent.LimitTickets = battle.LimitTickets;
                eevent.CostPerMessage = battle.CostPerMessage;
                eevent.CostPerMinuteVIP = battle.CostPerMinuteVIP;
                eevent.Name = battle.Name;
                eevent.OnlyVIP = battle.OnlyVIP;
                eevent.CostPerEvent = battle.CostPerEvent;
                eevent.CostPerEventVIP = battle.CostPerEventVIP;
                eevent.DateStart = battle.DateStart;
                eevent.DateEnd = battle.DateEnd > battle.DateStart ?
                                 battle.DateEnd :
                                 battle.DateStart.AddHours(1).AddMinutes(30);

                db.SaveChanges();
                return Ok(eevent);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        public IHttpActionResult Delete(int key)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //только сам пользователь может добавлять себе специализации
            var us = db.EBattles.Where(u => u.Id == key
                                                    && u.UserId == AppUser.Id
                                                    && u.IsDeleted == false)
                                                    .FirstOrDefault();
            if (us == null)
            {
                return BadRequest();
            }

            us.IsDeleted = true;
            us.Room.IsDeleted = true;
            db.SaveChanges();
            return Ok();
        }
    }
}