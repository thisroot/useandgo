﻿using MoreLinq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Routing;
using ug.Models;
using ug.Models.Simple;

namespace ug.Controllers
{
    /// <summary>
    /// Список пользователей вернуть
    /// </summary>
    [Authorize]
    public class UsersController : BaseODataController
    {
        // GET: Users
        /// <summary>
        /// Получить всех тренеров по пользователю
        /// </summary>
        /// <returns></returns>
        ///  
       
        [EnableQuery]
        public IQueryable<ApplicationUser> Get()
        {
          return db.Users.Where(x=>x.Id != AppUser.Id && x.IsDeleted == false).AsQueryable();
        }

    }
}