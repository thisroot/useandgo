﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using ug.Models;
using ug.Models.Events;

namespace ug.Controllers
{
    [Authorize]
    public class EWebinarsController : BaseODataController
    {
        [EnableQuery]
        public IQueryable<EWebinar> Get()
        {
            var UserId = AppUser.Id;
            return db.EWebinars.Where(x => x.IsDeleted == false).AsQueryable();
        }

        public async Task<IHttpActionResult> Post(EWebinar webinar)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (webinar.DateEnd == null)
            {
                webinar.DateEnd = webinar.DateStart.AddHours(1).AddMinutes(30);
            };

            //только сам пользователь может добавлять себе специализации
            webinar.UserId = AppUser.Id;
            var room = db.Rooms.Add(new Room() { IsEvent = true });
            webinar.Room = room;
            db.EWebinars.Add(webinar);
            await db.SaveChangesAsync();
            return Created(webinar);
        }

        public IHttpActionResult Patch(int key, EWebinar webinar)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var userId = AppUser.Id;
            if (db.EWebinars.Where(z => z.UserId == AppUser.Id && z.IsDeleted == false && z.Id == key).Any())
            {
                if (webinar.DateEnd == null)
                {
                    webinar.DateEnd = webinar.DateStart.AddHours(1).AddMinutes(30);
                };
                // запрос к базе для извлечения референсной модели
                var eevent = db.EWebinars.Where(z => z.UserId == AppUser.Id && z.IsDeleted == false && z.Id == key).FirstOrDefault();
                eevent.CostPerMinute = webinar.CostPerMinute;
                eevent.DateUpdate = DateTime.Now;
                eevent.Description = webinar.Description;
                eevent.FileId = webinar.FileId;
                eevent.LimitVIPTickets = webinar.LimitVIPTickets;
                eevent.LimitTickets = webinar.LimitTickets;
                eevent.CostPerMinuteVIP = webinar.CostPerMinuteVIP;
                eevent.Name = webinar.Name;
                eevent.OnlyVIP = webinar.OnlyVIP;
                eevent.CostPerEvent = webinar.CostPerEvent;
                eevent.CostPerEventVIP = webinar.CostPerEventVIP;
                eevent.DateStart = webinar.DateStart;
                eevent.DateEnd = webinar.DateEnd > webinar.DateStart ?
                                 webinar.DateEnd :
                                 webinar.DateStart.AddHours(1).AddMinutes(30);

                db.SaveChanges();
                return Ok(eevent);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        public IHttpActionResult Delete(int key)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //только сам пользователь может добавлять себе специализации
            var us = db.EWebinars.Where(u => u.Id == key
                                                    && u.UserId == AppUser.Id
                                                    && u.IsDeleted == false)
                                                    .FirstOrDefault();
            if (us == null)
            {
                return BadRequest();
            }

            us.IsDeleted = true;
            us.Room.IsDeleted = true;
            db.SaveChanges();
            return Ok();
        }
    }
}