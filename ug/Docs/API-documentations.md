﻿# Спецификация UseAndGo

- ### REST API
    - Регистрация / аутентификация
    - Пользователи
    - Контакты
    - Комнаты
    
- ### Сигнальные сообщения

<hr>


### REST API

#### Регистрация / Аутентификация

- Регистрация
    - запрос
    ```json
    {
        "url": "POST: Account/RegisterUser",
        "headers": {
                 "Content-Type": "application/x-www-form-urlencoded" },
        "body": {
                "phone": "(int) 79213245432",
                "password": "(string) myPassword123" }
    }
    ```
    - ответ (200)
    ```json
      { "state": true }
    ```
    
- Подтверждение кода
    - запрос
    ```json
    {
        "url": "POST: Account/ConfirmationCode ",
        "headers": {
               "Content-Type": "application/x-www-form-urlencoded" },
        "body": {
              "phone": "(int) 79213245432",
              "code": "(int) 123456" }
    }
    ```
  - ответ (200)
    ```json
      { "state": true }
    ```
- Аутентификация
    - запрос
    ```json
    {
        "url": "POST: oauth/login ",
        "headers": {
                 "Content-Type": "application/x-www-form-urlencoded" },
        "body": {
                "username": "(int) 79213245432",
                "password": "(string) myPassword123",
                "grant_type": "(const string) password"}
    }
    ```
    - ответ (200)
    ```json
    {
        "access_token": "vT_3bjfci8OYHJ92JcZE6qPd...",
        "token_type": "bearer",
        "expires_in": 2591999
    }
    ```
    
- Валидация токена
    - запрос
        ```json
        {
            "url": "POST: Account/ValidateToken ",
            "headers": {
                     "Content-Type": "application/x-www-form-urlencoded",
                     "Authorization": "bearer {{yourToken}}"},
            "body": {
                    "token": "(string) vT_3bjfci8OYHJ92JcZE6qPd...",
                    "password": "(string) myPassword123",
                    "grant_type": "(const string) password"}
        }
        ```
        - ответ (200)
        ```json
        { "state": "true" }
        ```
  
#### Пользователи (odata)
- Получить всех пользователей
    - запрос
     ```json
            {
                "url": "GET: odata/Users",
                "headers": {
                         "Content-Type": "application/x-www-form-urlencoded",
                         "Authorization": "bearer {{yourToken}}"}
            }
     ```
    - ответ (200)
    ```json
    {
    "@odata.context": "http://localhost:61124/odata/$metadata#Users",
    "value": [
        {
            "Nummer": 2,
            "Surname": null,
            "Name": null,
            "DateBirth": "2017-08-24T10:10:30.37+03:00",
            "LastLogin": "2017-08-24T10:10:30.37+03:00",
            "Email": "79202550067@useandgo.online",
            "PhoneNumber": "79202550067",
            "Id": "540f9af4-b804-48cb-9c47-47921ae8bf5c",
            "UserName": "79202550067",
            "Roles": []
        },
        {
            "Nummer": 3,
            "Surname": null,
            "Name": null,
            "DateBirth": "2017-08-24T14:58:00.773+03:00",
            "LastLogin": "2017-08-24T14:58:00.773+03:00",
            "Email": "79601962733@useandgo.online",
            "PhoneNumber": "79601962733",
            "Id": "5a3078ad-8636-4695-9190-3275e5062d63",
            "UserName": "79601962733",
            "Roles": []
        }
    ]
}
    ```
    
#### Получить всех друзей пользователя
   - запрос
     ```json
            {
                "url": "GET: /odata/UserFriends?$expand=FriendUser",
                "headers": {
                         "Content-Type": "application/x-www-form-urlencoded",
                         "Authorization": "bearer {{yourToken}}"}
            }
     ```
  - ответ (200)
    
    ```json
       {
        "@odata.context": "http://localhost:61124/odata/$metadata#UserFriends",
        "value": [
            {
                "id": 3,
                "UserId": "3751615e-4a1c-42f0-b4da-d605fae5d70d",
                "UserIdFriend": "540f9af4-b804-48cb-9c47-47921ae8bf5c",
                "DateAdd": "2017-08-24T11:36:56.64+03:00",
                "FriendUser": {
                    "Nummer": 2,
                    "Surname": null,
                    "Name": null,
                    "DateBirth": "2017-08-24T10:10:30.37+03:00",
                    "LastLogin": "2017-08-24T10:10:30.37+03:00",
                    "Email": "79202550067@useandgo.online",
                    "PhoneNumber": "79202550067",
                    "Id": "540f9af4-b804-48cb-9c47-47921ae8bf5c",
                    "UserName": "79202550067",
                    "Roles": []
                }
            }
        ]
        }
    ```

#### Получить все приглашения в друзья
  - запрос
         
    ```json
        {
            "url": "GET: odata/UserInvitations",
            "headers": {
                        "Content-Type": "application/x-www-form-urlencoded",
                        "Authorization": "bearer {{yourToken}}"}
        }
    ```

- ответ (200)
        
    ```json
        {
            "@odata.context": "http://localhost:61124/odata/$metadata#UserInvitations",
            "value": []
        }
    ```

## Контакты

#### Получить все контакты пользователя
 Если в ответе  в обьекте Phones в поле ContactUserId есть значение, то этот человек у Вас в друзьях
  - запрос
    
    ```json
        {
            "url": "GET: /odata/Contacts?$expand=Phones",
            "headers": {
                     "Content-Type": "application/json",
                     "Authorization": "bearer {{yourToken}}"}
        }
    ```
    
  - ответ (200)
    
    ```json
       {
        "@odata.context": GET: "odata/$metadata#Contacts",
        "value": [
            {
                "id": 5970,
                "LocalId": "16692C48-27B9-4927-9F2C-60BA854C482F",
                "FirstName": "Женя",
                "SecondName": "Абашин",
                "Company": "",
                "UserId": "3751615e-4a1c-42f0-b4da-d605fae5d70d",
                "DateAdd": "2017-08-28T14:42:21.647+03:00",
                "isDelete": false,
                "Phones": [
                    {
                        "id": 6331,
                        "Tel": "79047892904",
                        "ContactId": 5970,
                        "ContactUserId": null,
                        "DateAdd": "2017-08-28T14:42:21.647+03:00"
                    }
                ]
            },
            {
                "id": 5971,
                "LocalId": "9CA44382-475B-4CB0-8883-811B3D3AAE9E",
                "FirstName": "Саша",
                "SecondName": "Ава",
                "Company": "",
                "UserId": "3751615e-4a1c-42f0-b4da-d605fae5d70d",
                "DateAdd": "2017-08-28T14:42:21.76+03:00",
                "isDelete": false,
                "Phones": [
                    {
                        "id": 6332,
                        "Tel": "79040654374",
                        "ContactId": 5971,
                        "ContactUserId": null,
                        "DateAdd": "2017-08-28T14:42:21.76+03:00"
                    }
                ]
            }]
        }
    ```
#### Получить всеx друзей в контактах (через функцию): 
данный запрос  удобен тем, что возвращает все идентификаторы друга, а так же комнату с
которой он ассоциирован с пользователем
  - запрос
  
    ```json
    {
        "url": "POST: odata/Contacts/Func.Friends",
        "headers": {
                    "Content-Type": "application/json",
                    "Authorization": "bearer {{yourToken}}"}
    }
    ```

  - ответ (200)
    
    ```json
        {
        "Contacts": [
                {
                    "Phones": [
                        {
                            "Phone": "79202550067",
                            "RoomId": 2,
                            "ContactUserId": "540f9af4-b804-48cb-9c47-47921ae8bf5c"
                        }
                    ],
                    "LocalId": "CCA2F0B8-155D-4682-BFC2-F23E93B43823",
                    "FirstName": "Сергей",
                    "SecondName": "Балашов",
                    "Company": ""
                }
            ]
        }
    ```

#### Получить всеx друзей в контактах (через роут)
  - запрос
    
    ```json
        {
            "url": "GET: odata/UserFriends?$expand=FriendUser",
            "headers": {
                     "Content-Type": "application/json",
                     "Authorization": "bearer {{yourToken}}"}
        }
    ```

  - ответ (200)
    
    ```json
    {
    "@odata.context": "odata/$metadata#UserFriends",
    "value": [
            {
                "id": 3,
                "UserId": "3751615e-4a1c-42f0-b4da-d605fae5d70d",
                "UserIdFriend": "540f9af4-b804-48cb-9c47-47921ae8bf5c",
                "DateAdd": "2017-08-24T11:36:56.64+03:00",
                "FriendUser": {
                    "Nummer": 2,
                    "Surname": null,
                    "Name": null,
                    "DateBirth": "2017-08-24T10:10:30.37+03:00",
                    "LastLogin": "2017-08-24T10:10:30.37+03:00",
                    "Email": "79202550067@useandgo.online",
                    "PhoneNumber": "79202550067",
                    "Id": "540f9af4-b804-48cb-9c47-47921ae8bf5c",
                    "UserName": "79202550067",
                    "Roles": []
                }
            }
        ]
    }
    ```
    
#### Добавить / перезаписать контакты - через функцию 

- запрос (mode: add - просто добавить, rw - перезаписать)
    
    ```json
        {
            "url": "POST: /odata/Contacts/Func.Set",
            "headers": {
                        "Content-Type": "application/json",
                        "Authorization": "bearer {{yourToken}}"},
            "body": {
                    "Mode": "add",
                    "Contacts": [
                        {
                            "LocalId": "16692C48-27B9-4927-9F2C-60BA854C482F",
                            "FirstName": "Женя",
                            "Company": "",
                            "SecondName": "Абашин",
                            "Phones": [
                                {"Phone": "79047892904"}
                            ]
                        }
                    ]
            }
        }
    ```
    - ответ (200)
    ```json
    { "state": true }
    ```

### Комнаты

#### Получить комнаты пользователя с друзьями

- Запрос

    ```json
        {
            "url": "GET: odata/Room?$expand=UserRooms($expand=User)",
            "headers": {
                     "Content-Type": "application/json",
                     "Authorization": "bearer {{yourToken}}"}
        }
    ```

- Ответ (200)
    
    ```json
        {
    "@odata.context": "http://localhost:61124/odata/$metadata#Room",
    "value": [
        {
            "id": 2,
            "DateAdd": "2017-08-24T11:36:56.647+03:00",
            "UserRooms": [
                {
                    "id": 4,
                    "UserId": "540f9af4-b804-48cb-9c47-47921ae8bf5c",
                    "RoomId": 2,
                    "DateAdd": "2017-08-24T11:36:56.66+03:00",
                    "User": {
                        "Nummer": 2,
                        "Surname": null,
                        "Name": null,
                        "DateBirth": "2017-08-24T10:10:30.37+03:00",
                        "LastLogin": "2017-08-24T10:10:30.37+03:00",
                        "Email": "79202550067@useandgo.online",
                        "PhoneNumber": "79202550067",
                        "Id": "540f9af4-b804-48cb-9c47-47921ae8bf5c",
                        "UserName": "79202550067",
                        "Roles": []
                    }
                }
            ]
        }
    ]
    }
    ```
    - ответ (200)
    ```json
    { "state": true }
    ```
        
### Сигнальные сообщения

#### Исходящие сообытия:
   
   
- Войти в систему сообщений ```Login(string Token)```
- Получить онлайн друзей ```getOnlineFriends()```
- Отправить запрос в друзья ```AddUserInvations(string UserID)``` 
- Исходящий вызов ```OutgoingCall(string UserId)```
- Отменить вызов ```RejectInitiator(string UserId)```
- Принять вызов ```AcceptСall(string UserId)```
- Сигнальное сообщение ```SignalingMessage(SignalingMessage message)```
- Принять приглашение ```AcceptInvations(int id)```
- Отменить приглашение ```RejectInvations(int id)```
- Получить список приглашений в друзья ```GetUserInvations()```

#### Входящие события

- Удачный вход ```SuccessfulLogin(DateTime.Now)```
- Ошибка входа ```ErrorLogin(string "Error Token")```
- Получить ICE сервера ```ICE(ICE ice)```
- Изменился статус друга (онлайн / оффлайн) 
```js
ChangeFriendState({ 
        status = "online", 
        id = "540f9af4-b804-48cb-9c47-47921ae8bf5c" 
    })
```
- Друзья онлайн ```onlineFriends(List<string> phones)```
- Запрос на добавление ```AddUserInvations(string id)```
- Запрос на добавление отправлен ```SuccessAddUserInvations("Success Add User")```
- Запрос на добаленине принят ```AcceptInvations(bool true)```
- Запрос на добавление отклонен ```RejectInvations(bool true)```
- Список приглашений в друзья ```UserInvations(List<UserInvations> invations)```
- Входящий звонок ```NewCall(string UserId)```
- Отменили звонок ```RejectInitiator(string UserId)```
- Приняли приглашение на звонок ```AcceptСall(int IdRoom)```
- Сигнальное сообщение ```SignalingMessage(SignalingMessage message)```


#### Приложение: интерфейсы сообщений

- **SignalingMessage**

```charp
class SignalingMessage
    {
        public string to;
        public string type;
        public int roomId;
        public string roomType;
        public dynamic body;
    };

```
