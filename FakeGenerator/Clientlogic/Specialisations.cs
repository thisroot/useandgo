﻿using Bogus;
using FakeGenerator.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ug.Models;
using ug.Models.Specialisations;

namespace FakeGenerator.Clientlogic
{

    public class Specialisations
    {


        public static ug.Models.ApplicationDbContext ugdb = new ug.Models.ApplicationDbContext("Data Source = (local)\\SQLExpress; Initial Catalog = ug; Integrated Security = True; MultipleActiveResultSets = True;");
        public static TestDBContext locdb = new TestDBContext();

        public static List<UserSpecialisations> CreateSpecialisations(FakeUser user, string path, string host)
        {
            List<UserSpecialisations> res = new List<UserSpecialisations>();
            var random = new Bogus.Randomizer();
            var count = random.Number(0, 20);
            if (count == 0) return res;

            var specialisations = ugdb.Specialisations.ToList();

            var testSpecialisation = new Faker<UserSpecialisations>("ru")
                  //.CustomInstantiator(f => new User(userIds++, f.Random.Replace("###-##-####")))

                  .RuleFor(u => u.Profession, f => f.Name.JobTitle())
                  .RuleFor(u => u.Description, f => f.Name.JobDescriptor())
                  .RuleFor(u => u.Experience, f => f.Date.Past(10))
                  .RuleFor(u => u.CostPerMessage, f => f.Finance.Amount(0, 2))
                  .RuleFor(u => u.CostPerMinuteCall, f => f.Finance.Amount(0, 200))
                  .RuleFor(u => u.UserId, user.ExternalId.ToString());


            var client = new RestClient();
            client.BaseUrl = new Uri(host);

            for (int i = 0; i < count; i++)
            {

                var spec = testSpecialisation.Generate();
#warning выходит за пределы
                spec.SpecialisationId = specialisations[random.Number(0, specialisations.Count()-1)].Id;


                var request = new RestRequest(path, Method.POST);
                request.Parameters.Clear();
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", "bearer " + user.Token);
                request.AddJsonBody(new {
                    Profession = spec.Profession,
                    Description = spec.Description,
                    Experience = spec.Experience,
                    CostPerMessage = spec.CostPerMessage,
                    CostPerMinuteCall = spec.CostPerMinuteCall,
                    SpecialisationId = spec.SpecialisationId
                });
                request.RequestFormat = DataFormat.Json;

                var response = client.Execute(request).Content;

                try
                {
                    res.Add(JsonConvert.DeserializeObject<UserSpecialisations>(response));

                }

                catch
                {
                    Console.WriteLine(response);
                }

            }

            return res;

        }

        public static List<UserSubscribtions> Subscribe(FakeUser user, string path, string host)
        {

            List<UserSubscribtions> res = new List<UserSubscribtions>();
            var random = new Bogus.Randomizer();

            var countSubscr = random.Number(0, 40);
            if (countSubscr == 0) return res;
            var uspecs = ugdb.UsersSpecialisations.ToList();

            var client = new RestClient();
            client.BaseUrl = new Uri(host);

            for (int i = 0; i < countSubscr; i++)
            {
                var randspec = uspecs[random.Number(0, uspecs.Count()-1)];
                var request = new RestRequest(path, Method.POST);
                request.Parameters.Clear();
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", "bearer " + user.Token);
                request.AddJsonBody(new
                {
                    SpecialisationId = randspec.Id

                });
                request.RequestFormat = DataFormat.Json;
                var response = client.Execute(request).Content;

                try
                {
                    res.Add(JsonConvert.DeserializeObject<UserSubscribtions>(response));
                }

                catch
                {
                    Console.WriteLine(response);
                }

            }

            return res;
        }
    }
}
