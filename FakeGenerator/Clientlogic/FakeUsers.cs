﻿using Bogus;
using FakeGenerator.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ug.Models;
using ug.Models.Simple;
using Z.EntityFramework.Plus;

namespace FakeGenerator.Clientlogic
{
    class FakeUsers
    {

        public static ug.Models.ApplicationDbContext ugdb = new ug.Models.ApplicationDbContext("Data Source = (local)\\SQLExpress; Initial Catalog = ug; Integrated Security = True; MultipleActiveResultSets = True;");
        public static TestDBContext locdb = new TestDBContext();
        

        public static List<FakeUser> RegisterFakeUser(int count, string path, string host)
        {

            Random rnd = new Random();


            var testUser = new Faker<FakeUser>("ru")
                   //.CustomInstantiator(f => new User(userIds++, f.Random.Replace("###-##-####")))
                   .RuleFor(u => u.FirstName, f => f.Name.FirstName())
                   .RuleFor(u => u.SecondName, f => f.Name.LastName())
                   .RuleFor(u => u.Password, "12345678")
                   .RuleFor(u => u.PhoneNumber, "79" + rnd.Next(100000000, 999999999).ToString())
                   .RuleFor(u => u.DateBirth, f => f.Person.DateOfBirth);


            var oldusers = locdb.Users.ToList();
            var regUsers = new List<FakeUser>();

            var client = new RestClient();
            client.BaseUrl = new Uri(host);

            //генерация пользователей
            for (int i = 0; i < count; i++)
            {
                
                var user = testUser.Generate();
                Thread.Sleep(1);
                user.PhoneNumber = "79" + rnd.Next(100000000, 999999999).ToString();


                //проверка на существование такого номера в базе данных 
                //- если надо исключить стресс тест на наличие в системе зареганных юзеров
                //if (oldusers.Select(s => s.PhoneNumber).Contains(user.PhoneNumber)) continue;

                var request = new RestRequest(path, Method.POST);
                //.AddHeader("Content-Type", "application/x-www-form-urlencoded")
                //.RequestFormat(DataFormat.Xml)
                request.Parameters.Clear();
                request.AddParameter("phone", user.PhoneNumber);
                request.AddParameter("password", user.Password);
                request.RequestFormat = DataFormat.Xml;


                var response = client.Execute(request).Content;

                dynamic res = JsonConvert.DeserializeObject(response);
                if (res.state == true)
                {
                    var code = res.code;
                    user.SmsCode = code;
                    locdb.Users.Add(user);
                    regUsers.Add(user);
                }
            }
            locdb.SaveChanges();
            return regUsers;
        }

        public static List<FakeUser> ValidateSms(List<FakeUser> users, string path, string host)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri(host);
            

            users.ForEach(x =>
            {
                var request = new RestRequest(path, Method.POST);
                request.AddParameter("phone", x.PhoneNumber);
                request.AddParameter("code", x.SmsCode);
                request.RequestFormat = DataFormat.Xml;

                var response = client.Execute(request).Content;

                dynamic res = JsonConvert.DeserializeObject(response);

                if (res.state == false)
                {
                    var rem = new List<FakeUser>();
                    rem.Add(x);
                    Remove(rem);
                }
            });

            locdb.SaveChanges();
            
            return locdb.Users.ToList();
        }

        public static List<FakeUser> Auth(List<FakeUser> users, string path, string host)
        {
            var client = new RestClient();
            client.BaseUrl = new Uri(host);

            var ids = users.Select(o => o.id);
            var locusers = locdb.Users.Where(e => ids.Contains(e.id));

            foreach(var locuser in locusers)
            {
                var request = new RestRequest(path, Method.POST);
                request.Parameters.Clear();
                request.AddParameter("username", locuser.PhoneNumber);
                request.AddParameter("password", locuser.Password);
                request.AddParameter("grant_type", "password");
                request.RequestFormat = DataFormat.Xml;

                var response = client.Execute(request).Content;
                dynamic res = JsonConvert.DeserializeObject(response);
                try
                {
                    locuser.Token = res.access_token;
                }
                catch
                {
                    var rem = new List<FakeUser>();
                    rem.Add(locuser);
                    Remove(rem);
                }
            }

            locdb.SaveChanges();
            return locdb.Users.Where(x=>x.Token != null).ToList();
        }

        public static List<FakeUser> ValidateToken(List<FakeUser> users, string path, string host)
        {
            ug.Models.ApplicationDbContext ugdb = new ug.Models.ApplicationDbContext("Data Source = (local)\\SQLExpress; Initial Catalog = ug; Integrated Security = True; MultipleActiveResultSets = True;");
            var client = new RestClient();
            client.BaseUrl = new Uri(host);


            var ids = users.Select(o => o.id);
            var locusers = locdb.Users.Where(e => ids.Contains(e.id));
            foreach(var locuser in locusers)
            {
                var request = new RestRequest(path, Method.POST);
                request.Parameters.Clear();
                request.AddHeader("Authorization", "bearer " + locuser.Token);
                request.AddParameter("token", locuser.Token);
                request.AddParameter("password", locuser.Password);
                request.AddParameter("grant_type", "password");
                request.RequestFormat = DataFormat.Xml;

                var response = client.Execute(request).Content;

                try
                {
                    dynamic res = JsonConvert.DeserializeObject(response);
                    if (res.state == false)
                    {
                        var rem = new List<FakeUser>();
                        rem.Add(locuser);
                        Remove(rem);
                    }
                }

                catch
                {
                    Console.WriteLine(response);
                }
            }


            var userPhones = locusers.Select(e => e.PhoneNumber).ToList();
            var dbusers = ugdb.Users.Where(s => userPhones.Contains(s.PhoneNumber)).ToList();
                foreach(var dbuser in dbusers)
            {
                locdb.Users.FirstOrDefault(g => g.PhoneNumber == dbuser.PhoneNumber).ExternalId = Guid.Parse(dbuser.Id);
            }
            
            
            locdb.SaveChanges();
            return locdb.Users.Where(s=>s.Token != null).ToList();
        }

        public static List<FakeUser> UpdateProfile(List<FakeUser> users, string path, string host)
        {

            ApplicationDbContext ugdb = new ApplicationDbContext("Data Source = (local)\\SQLExpress; Initial Catalog = ug; Integrated Security = True; MultipleActiveResultSets = True;");
            var client = new RestClient();
            client.BaseUrl = new Uri(host);

            var ids = users.Select(o => o.id);
            var locusers = locdb.Users.Where(e => ids.Contains(e.id));
            foreach (var user in locusers)
            {
                var request = new RestRequest(path, Method.PATCH);
                request.Parameters.Clear();
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", "bearer " + user.Token);
                request.AddJsonBody(new SimpleUserCard
                {
                     Id = user.ExternalId.ToString(),
                     AvatarId = user.AvatarId,
                     DateBirth = user.DateBirth,
                     FirstName = user.FirstName,
                     SecondName = user.SecondName,
                });
                var response = client.Execute(request).Content;

                try
                {
                    dynamic res = JsonConvert.DeserializeObject<UserProfile>(response);
                }

                catch
                {
                    var rem = new List<FakeUser>();
                    rem.Add(user);
                    Remove(rem);
                    Console.WriteLine(response);
                }
            }

            return locdb.Users.ToList();
        }

        public static List<FakeUser> Remove(List<FakeUser> users)
        {
            ug.Models.ApplicationDbContext ugdb = new ug.Models.ApplicationDbContext("Data Source = (local)\\SQLExpress; Initial Catalog = ug; Integrated Security = True; MultipleActiveResultSets = True;");
            TestDBContext locdb = new TestDBContext();

            var userIds = users.Select(d => d.ExternalId.ToString()).ToList();

            ugdb.UsersCodes.Where(s => userIds.Contains(s.UserId)).Delete();
            ugdb.UsersTokens.Where(s => userIds.Contains(s.UserId)).Delete();
            ugdb.UsersSpecialisations.Where(s => userIds.Contains(s.UserId)).Delete();
            ugdb.SaveChanges();

            ugdb.Users.Where(s => userIds.Contains(s.Id)).ToList()
                .ForEach(d=> { ugdb.Users.Remove(d);});
            ugdb.SaveChanges();

            locdb.Users.Where(s => userIds.Contains(s.ExternalId.ToString())).Delete();
            locdb.SaveChanges();
            return users;
        }
    }
}
