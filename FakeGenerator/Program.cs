﻿using Bogus;
using RestSharp;
using FakeGenerator.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FakeGenerator.Clientlogic;
using ug.Models.Specialisations;
using ug.Models;
//using ug.Models;

namespace FakeGenerator
{
    class Program
    {

        public static ug.Models.ApplicationDbContext ugdb = new ug.Models.ApplicationDbContext("Data Source = (local)\\SQLExpress; Initial Catalog = ug; Integrated Security = True; MultipleActiveResultSets = True;");
        public static TestDBContext locdb = new TestDBContext();


        public static string host = "http://localhost:61124";

        static void Main(string[] args)
        {
            //контекст приложения

            //конекст тестовых данных
            TestDBContext locdb = new TestDBContext();

            // получаем зареганных юзеров
            var regUsers = FakeUsers.RegisterFakeUser(300, "Account/RegisterUser", host);
            // валидируем по смс коду
            var valUsers = FakeUsers.ValidateSms(regUsers, "Account/ConfirmationCode", host);
            //Авторизуем пользователей

            var authUsers = FakeUsers.Auth(locdb.Users.ToList(), "oauth/login", host);
            var validateUsers = FakeUsers.ValidateToken(locdb.Users.ToList(), "Account/ValidateToken", host);

            //Обновим данные пользователя
            var upUsers = FakeUsers.UpdateProfile(locdb.Users.ToList(), "UserProfile", host);

            //создадим контакты среди пользователей в случайном порядке добавив каждого каждому

            //создать специализации в случайном порядке
            Dictionary<FakeUser, List<UserSpecialisations>> uspecs = new Dictionary<FakeUser, List<UserSpecialisations>>();
            locdb.Users.AsNoTracking().ToList().ForEach(s =>
            {
                var specs = Specialisations.CreateSpecialisations(s, "odata/UserSpecialisations", host);
                uspecs.Add(s, specs);
            });
            Console.WriteLine("Count specs:" + uspecs.Sum(d => d.Value.Count()));


            //случайно подписаться на специализации
            Dictionary<FakeUser, List<UserSubscribtions>> ussubscr = new Dictionary<FakeUser, List<UserSubscribtions>>();
            locdb.Users.AsNoTracking().ToList().ForEach(d =>
            {
                var specSubscr = Specialisations.Subscribe(d, "odata/UserSubscribtions", host);
                ussubscr.Add(d, specSubscr);
            });


            //удалим созданных пользователей из всех баз
            //FakeUsers.Remove(locdb.Users.ToList());

            //выведем список зареганных пользователей.
            var json = JsonConvert.SerializeObject(validateUsers, Formatting.Indented);
            Console.WriteLine(json);


            Console.WriteLine("Press ESC or Enter to stop");
            do {while (!Console.KeyAvailable) {}}
            while (Console.ReadKey(true).Key != ConsoleKey.Escape);

        }

        
    }
}
