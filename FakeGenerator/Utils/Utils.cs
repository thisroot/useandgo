﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakeGenerator.Utils
{
    public class Utils
    {
        public static bool IsPropertyExist(dynamic settings, string name)
        {
            bool resp;
            if (settings is ExpandoObject)
            {
                resp = ((IDictionary<string, object>)settings).ContainsKey(name);
                return resp;
            }

            resp =  settings.GetType().GetProperty(name) != null;
            return resp;
        }
    }
}
