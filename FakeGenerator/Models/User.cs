﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakeGenerator.Models
{
    public class FakeUser
    {
        [Key]
        public int id {get;set;}
        public Guid ExternalId { get; set; }
        public string SecondName { get; set; }
        public string FirstName { get; set; }
        public string Patronymic { get; set; }
        public DateTime DateBirth { get; set; } = DateTime.Now;
        public string AvatarId { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public string SmsCode { get; set; }
    }
}
