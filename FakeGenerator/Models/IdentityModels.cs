﻿using FakeGenerator.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakeGenerator.Models
{
    public class TestDBContext : DbContext
    {

        public DbSet<FakeUser> Users { get; set; }

        public TestDBContext()
            : base("DefaultConnection")
        {

        }

        public TestDBContext(string DefaultConnection)
            : base(DefaultConnection)
        {

        }

        

    }

}